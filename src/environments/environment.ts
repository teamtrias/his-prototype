// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseDatabaseUrl: 'https://his-prototype-default-rtdb.firebaseio.com/',
    webApiUrl: 'https://localhost:44309/api',
    isEncriptionStorage: true,
    secretKey: {
        localStorageKey: 'pison_ts',
        sessionKey: 'pison_ts_session',
        jwtKey: 'pison_ts_jwt',
        httpRequestKey: 'pison_ts_http_request',
        routeKey: 'pison_ts_route'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
