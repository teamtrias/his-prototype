import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';
import { ComboBoxModule, DropDownListModule, AutoCompleteModule } from '@syncfusion/ej2-angular-dropdowns';
import { NumericTextBoxModule, TextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { ContextMenuModule, SidebarModule, TabModule } from '@syncfusion/ej2-angular-navigations';

import { MenuService } from './service/page/menu.service';
import { CommandColumnService, EditService, GridModule, PageService, PdfExportService, ResizeService, SearchService, ToolbarService } from '@syncfusion/ej2-angular-grids';
import { CookieService } from 'ngx-cookie-service';

import { JwtInterceptor } from './helper/interceptor/jwt.interceptor';
import { XsrfTokenInterceptor } from './helper/interceptor/xsrf-token.interceptor';

import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { SharedLookupComponent } from './components/shared-lookup/shared-lookup.component';
import { InputFieldComponent } from './components/shared-lookup/search-field/input-field.component';
import { FiltersComponent } from './components/shared-lookup/filters/filters.component';
import { SharedGridComponent } from './components/shared-grid/shared-grid.component';

import { AuthComponent } from './page/auth/auth.component';
import { DashboardComponent } from './page/dashboard/dashboard.component';
import { HomeComponent } from './page/dashboard/home/home.component';
import { SetupTarifComponent } from './page/setup-data/setup-tarif/setup-tarif.component';
import { SetupHargaTarifComponent } from './page/setup-data/setup-harga-tarif/setup-harga-tarif.component';
import { DataPasienComponent } from './page/IRNA/data-pasien/data-pasien.component';
import { SetupPasienComponent } from './page/IRNA/setup-pasien/setup-pasien.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AdmisiPasienComponent } from './page/IRJA/admisi-pasien/admisi-pasien.component';
import { AntrianPasienComponent } from './page/IRJA/antrian-pasien/antrian-pasien.component';
import { LaboratoriumComponent } from './page/Order/laboratorium/laboratorium.component';
import { RadiologiComponent } from './page/Order/radiologi/radiologi.component';
import { ResepDigitalComponent } from './page/Order/resep-digital/resep-digital.component';
import { PenjualanObatComponent } from './page/Farmasi/penjualan-obat/penjualan-obat.component';
import { BrowsePenjualanObatComponent } from './page/Farmasi/browse-penjualan-obat/browse-penjualan-obat.component';
import { BillingComponent } from './page/billing/billing.component';


@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        AuthComponent,
        LoadingSpinnerComponent,
        SetupTarifComponent,
        SetupHargaTarifComponent,
        SharedLookupComponent,
        InputFieldComponent,
        FiltersComponent,
        SharedGridComponent,
        HomeComponent,
        DataPasienComponent,
        SetupPasienComponent,
        PageNotFoundComponent,
        AdmisiPasienComponent,
        AntrianPasienComponent,
        LaboratoriumComponent,
        RadiologiComponent,
        ResepDigitalComponent,
        PenjualanObatComponent,
        BrowsePenjualanObatComponent,
        BillingComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        TabsModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        GridModule,
        DatePickerModule,
        DropDownListModule,
        ComboBoxModule,
        NumericTextBoxModule,
        CheckBoxModule,
        TextBoxModule,
        ModalModule.forRoot(),
        SidebarModule,
        ContextMenuModule,
        TabModule,
        AutoCompleteModule
    ],
    providers: [
        MenuService,
        ToolbarService,
        EditService,
        Title,
        PdfExportService,
        ResizeService,
        BsModalService,
        SearchService,
        PageService,
        CommandColumnService,
        { provide: CookieService, useClass: CookieService },
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: XsrfTokenInterceptor, multi: true },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
