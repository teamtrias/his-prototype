import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SharedGridComponent } from 'src/app/components/shared-grid/shared-grid.component';
import { BillingService } from 'src/app/service/page/billing.service';

@Component({
    selector: 'app-billing',
    templateUrl: './billing.component.html',
    styleUrls: ['./billing.component.css']
})
export class BillingComponent implements OnInit, AfterViewInit {

    formBilling: FormGroup;

    // ** Lookup Properties
    buttonLookup: string = "Cari";
    columnsLookup: any = [];
    filterLookup: any = [];
    urlLookup: string = "";

    // ** Grid Slip Tagihan Properties
    gridSlipTagihan: SharedGridComponent = null;
    gridSlipTagihanColumns: any = [];
    gridSlipTagihanDatasource: any = [];


    constructor(
        private formBuilder: FormBuilder,
        private billingService: BillingService
    ) {
        this.onSetFormBillingAttribute();
    }

    ngOnInit(): void {
    }

    ngAfterViewInit(): void {
        this.onSetGridProperties();
    }

    // ** Function untuk set Form Attribute ...
    onSetFormBillingAttribute() {
        this.formBilling = this.formBuilder.group({
            "ControlName": [0, []],
            "FirstName": ["", []],
            "NoRegister": ["", []],
            "MrNo": ["", []],
            "ScvCode": ["", []],
            "ScvDescr": ["", []],
            "TglMasukRawat": [null, []],
            "PartyDebitur": [0, []],
            "NamaDebitur": ["", []],
            "Alamat": ["", []],
            "TotalBiaya": [0, []],
            "Asuransi": [0, []],
            "TotalTagihan": [0, []],
            "DiskonDokter": [0, []],
            "SaldoKlaim": [0, []],
            "Bayar": [0, []],
            "SisaTagihan": [0, []],
            "Status": ["", []]
        });
    }

    // ** Function untuk set Attribute Lookup (Column, Filter dan Url) ...
    onOpenLookup(id: string) {
        this.columnsLookup = [];
        this.filterLookup = [];
        this.urlLookup = "";

        if (id == "modalAdmisiPasien") {
            this.urlLookup = "/TransaksiRawatJalan/GetDataAdmisiByParams";

            this.billingService.onGetLookupColumns("Farmasi/PenjualanObat/Lookup/LookupAdmisi/columns.json")
                .subscribe((_result) => {
                    this.columnsLookup = _result;
                });

            this.billingService.onGetLookupFilters("Farmasi/PenjualanObat/Lookup/LookupAdmisi/filters.json")
                .subscribe((_result) => {
                    this.filterLookup = _result;
                });
        }
    }

    // ** Function untuk GET SELECTED DATA FROM LOOKUP ADMISI ...
    onGetSelectedDataLookupAdmisi(args: any) {
        this.NoRegister.setValue(args.NoRegister);
        this.MrNo.setValue(args.MrNo);
        this.FirstName.setValue(args.NamaPasien);
        this.ScvCode.setValue(args.ScvCode);
        this.ScvDescr.setValue(args.ScvDescr);
        this.TglMasukRawat.setValue(args.TglMasukRawat);
        this.Alamat.setValue(args.Alamat);
        this.PartyDebitur.setValue(args.PartyDebitur);
        this.NamaDebitur.setValue(args.NamaDebitur);

        this.onFetchGridSlipTagihanDatasource(args.NoRegister, args.MrNo);
    }

    // ** Initialize Shared Grid Component
    onInitializedGridSlipTagihan(component: SharedGridComponent) {
        this.gridSlipTagihan = component;
    }

    // ** Function untuk set Grid Properties ...
    onSetGridProperties() {
        this.billingService.onFetchGridColumn("Billing/GridHeader/GridSlipGaji.json")
            .subscribe((_result) => {
                this.gridSlipTagihanColumns = _result;
            });
    }

    // ** Function untuk fetch Grid Slip Tagihan Datasource ...
    onFetchGridSlipTagihanDatasource(NoRegister: string, MrNo: string) {
        this.billingService.onFetchGridSlipTagihanDatasource(NoRegister, MrNo)
            .subscribe((_result) => {
                this.gridSlipTagihanDatasource = _result;
            })
    }

    onSelectingRowGridSlipTagihan(args: any) {
        console.log(args);
    }

    get FirstName() { return this.formBilling.get("FirstName"); }
    get NoRegister() { return this.formBilling.get("NoRegister"); }
    get MrNo() { return this.formBilling.get("MrNo"); }
    get ScvCode() { return this.formBilling.get("ScvCode"); }
    get ScvDescr() { return this.formBilling.get("ScvDescr"); }
    get TglMasukRawat() { return this.formBilling.get("TglMasukRawat"); }
    get PartyDebitur() { return this.formBilling.get("PartyDebitur"); }
    get NamaDebitur() { return this.formBilling.get("NamaDebitur"); }
    get Alamat() { return this.formBilling.get("Alamat"); }
    get TotalBiaya() { return this.formBilling.get("TotalBiaya"); }
    get Asuransi() { return this.formBilling.get("Asuransi"); }
    get TotalTagihan() { return this.formBilling.get("TotalTagihan"); }
    get DiskonDokter() { return this.formBilling.get("DiskonDokter"); }
    get ControlName() { return this.formBilling.get("ControlName"); }
    get SaldoKlaim() { return this.formBilling.get("SaldoKlaim"); }
    get Bayar() { return this.formBilling.get("Bayar"); }
    get SisaTagihan() { return this.formBilling.get("SisaTagihan"); }
    get Status() { return this.formBilling.get("Status"); }
}
