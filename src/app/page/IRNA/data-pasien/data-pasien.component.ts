import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { GridComponent } from '@syncfusion/ej2-angular-grids';
import { SharedGridComponent } from 'src/app/components/shared-grid/shared-grid.component';
import { DataPasienService } from 'src/app/service/page/data-pasien.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-data-pasien',
    templateUrl: './data-pasien.component.html',
    styleUrls: ['./data-pasien.component.css']
})
export class DataPasienComponent implements OnInit {

    // ... Combobox Kriteria Properties
    public dataKriteria: Object[];
    fieldsKriteria: Object;
    kriteriaAktif: string;
    kriteriaFilter: string;

    // ... Grid Data Pasien Properties
    @ViewChild("gridDataPasien", { read: true, static: false })
    public gridDataPasien: SharedGridComponent;

    gridId: string = "gridDataPasien";
    gridDataSource: Object[];
    gridWidth: any = 'auto';
    gridLines: string = "Both";
    isAllowPaging: boolean = true;
    gridPageSettings: any = [];
    gridToolbar: any = [];
    gridEditSettings: any = [];
    gridColumns: any;

    // ... Selected Data
    selectedData: any;

    constructor(private dataPasienService: DataPasienService, private router: Router) { }

    ngOnInit(): void {
        this.onFetchDataKriteriaPasien();
        this.onSetPropertiesGridDataPasien();
    }

    onFetchDataKriteriaPasien() {
        this.dataPasienService.getKriteriaPasien()
            .subscribe((_kriteria) => {
                this.dataKriteria = _kriteria;

                this.fieldsKriteria = { text: 'text', value: 'value' };
            })
    }

    onChangeKriteria(args: any) {
        let value = args.value;
        this.kriteriaAktif = value;

        let filter = args.filter;
        this.kriteriaFilter = filter;
    }

    onSearchKriteria(value: any, grid: any) {
        this.dataPasienService.getDataPasien(this.kriteriaAktif, this.kriteriaFilter, value)
            .subscribe((_result) => {
                grid.gridDataSource = _result;
            })
    }

    onFetchHeaderGrid() {
        this.dataPasienService.getGridColumnHeaders()
            .subscribe((headers) => {
                this.gridColumns = headers.columns;
            });
    }

    onSetPropertiesGridDataPasien() {
        this.onFetchHeaderGrid();

        this.gridToolbar = ['Add', 'Edit', 'Search'];
        this.gridEditSettings = { allowAdding: true, allowEditing: true };
    }

    onRowSelecting(args: any) {
        this.selectedData = args.data;
    }

    onToolbarClick(args: any, grid: any) {
        args.cancel = true;

        let action = args.item.text;

        if (action == "Add") {
            this.router.navigateByUrl("dashboard/irna/pasien");
        }
        else if (action == "Edit") {
            if (this.selectedData != undefined) {
                this.router.navigateByUrl("dashboard/irna/pasien", { state: { Pasien: this.selectedData } });
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Belum Ada Data Yang Dipilih',
                });
            }
        }
        else {
            //... do nothing
        }

    }
}
