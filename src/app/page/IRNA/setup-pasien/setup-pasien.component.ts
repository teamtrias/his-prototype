import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SetupPasienService } from 'src/app/service/page/setup-pasien.service';
import { UtilityService } from 'src/app/service/utility/utility.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-setup-pasien',
    templateUrl: './setup-pasien.component.html',
    styleUrls: ['./setup-pasien.component.css']
})
export class SetupPasienComponent implements OnInit {

    formSetupPasien: FormGroup;
    dataEditingPasien: any;

    // ...Tab Header
    tabHeader: any = [
        { "iconCss": "", "text": "" },
        { "iconCss": "", "text": "" },
        { "iconCss": "", "text": "" },
    ];

    // ... Sex Combobox
    public dataSex: Object[];
    fieldsSex: Object;

    // ... Gol. Darah Combobox
    public dataGolDarah: Object[];
    fieldsGolDarah: Object;

    // ... Keterbatasan Combobox
    public dataKeterbatasan: Object[];
    fieldsKeterbatasan: Object;

    // ... Bahasa Combobox
    public dataBahasa: Object[];
    fieldsBahasa: Object;

    modalButtonLookup: string = "Cari";
    modalColumnsItemLookup: any = [];
    modalFiltersItemLookup: any = [];
    modalStaticFilterLookup: any = [];

    // ... Shared Lookup Agama
    modalUrlLookupAgama: string = "/SetupPasien/GetAllAgama";

    // ... Shared Lookup Marital Status
    modalUrlLookupMarital: string = "/SetupPasien/GetAllMaritalStatus";

    // ... Shared Lookup Pendidikan
    modalUrlLookupPendidikan: string = "/SetupPasien/GetAllPendidikan";

    // ... Shared Lookup Pekerjaan
    modalUrlLookupPekerjaan: string = "/SetupPasien/GetAllTipePekerjaan";

    // ... Shared Lookup Provinsi
    modalUrlLookupProvinsi: string = "/Daerah/GetAllProvinsi";

    // ... Shared Lookup Kota
    modalUrlLookupKota: string = "/Daerah/GetKotaByProvinsi";

    // ... Shared Lookup Kecamatan
    modalUrlLookupKecamatan: string = "/Daerah/GetKecamatanByKota";

    // ... Shared Lookup Kelurahan
    modalUrlLookupKelurahan: string = "/Daerah/GetKelurahanByKecamatan";

    // ... Shared Lookup Jenis Id Card
    modalUrlLookupJenisKartu: string = "/SetupPasien/GetAllKartuIdentitas";

    // ... Shared Lookup Suku
    modalUrlLookupSuku: string = "/SetupPasien/GetAllSuku";

    // ... Shared Lookup Kebangsaan
    modalUrlLookupKebangsaan: string = "/SetupPasien/GetAllBangsa";

    // ... Untuk menghitung umur pasien
    @ViewChild('TahunPasien') TahunPasien: ElementRef;
    @ViewChild('BulanPasien') BulanPasien: ElementRef;
    @ViewChild('HariPasien') HariPasien: ElementRef;

    // ... Untuk menghitung umur ayah
    @ViewChild('TahunAyah') TahunAyah: ElementRef;
    @ViewChild('BulanAyah') BulanAyah: ElementRef;
    @ViewChild('HariAyah') HariAyah: ElementRef;

    // ... Untuk menghitung umur ibu
    @ViewChild('TahunIbu') TahunIbu: ElementRef;
    @ViewChild('BulanIbu') BulanIbu: ElementRef;
    @ViewChild('HariIbu') HariIbu: ElementRef;

    //... Untuk menampilkan data ke input form
    @ViewChild('AgamaText') AgamaText: ElementRef;
    @ViewChild('MaritalStatusText') MaritalStatusText: ElementRef;
    @ViewChild('KodePendidikanText') KodePendidikanText: ElementRef;
    @ViewChild('KodePekerjaanText') KodePekerjaanText: ElementRef;
    @ViewChild('ProvinsiText') ProvinsiText: ElementRef;
    @ViewChild('KotaText') KotaText: ElementRef;
    @ViewChild('KecamatanText') KecamatanText: ElementRef;
    @ViewChild('KelurahanText') KelurahanText: ElementRef;
    @ViewChild('KodeJenisKartuText') KodeJenisKartuText: ElementRef;
    @ViewChild('EthnicCodeText') EthnicCodeText: ElementRef;
    @ViewChild('NatCodeText') NatCodeText: ElementRef;

    @ViewChild('KotaOrtuAyahText') KotaOrtuAyahText: ElementRef;
    @ViewChild('KecamatanOrtuAyahText') KecamatanOrtuAyahText: ElementRef;
    @ViewChild('KodePekerjaanOrtuAyahText') KodePekerjaanOrtuAyahText: ElementRef;

    @ViewChild('KotaOrtuIbuText') KotaOrtuIbuText: ElementRef;
    @ViewChild('KecamatanOrtuIbuText') KecamatanOrtuIbuText: ElementRef;
    @ViewChild('KodePekerjaanOrtuIbuText') KodePekerjaanOrtuIbuText: ElementRef;

    constructor(private formBuilder: FormBuilder,
        private utilityService: UtilityService,
        private setupPasienService: SetupPasienService,
        private router: Router) {

        this.dataEditingPasien = this.router.getCurrentNavigation();
    }

    ngOnInit(): void {
        this.onFetchTabHeader();
        this.onSetFormGroupAttribute();
        this.onSetComboboxAttribute();

        setTimeout(() => {
            this.onCekCurrentNavigationExtras();
        }, 100);
    }

    onCekCurrentNavigationExtras() {
        let data = this.dataEditingPasien;

        if (data) {
            let partyId = data.extras.state.Pasien.PartyId;

            // ... Get informasi Umum
            this.setAttributeForInformasiUmum(partyId.toString());

            // ... Get informasi Pasien
            this.setAttributeForInformasiPasien(partyId.toString());

            // ... Get informasi Alamat Pasien
            this.setAttributeForInformasiAddressPasien(partyId.toString());

            // ... Get informasi Kartu Identitas Pasien
            this.setAttributeForKartuIdentitasPasien(partyId.toString());

            // ... Get informasi Keluarga Pasien
            this.setAttributeForInformasiKeluargaPasien(partyId.toString());

            // ... Get informasi Orang Tua Pasien
            this.setAttributeForInformasiOrtuPasien(partyId.toString());
        }
        else {
            // ... do nothing
        }
    }

    // ... Set attribute for Informasi Umum => prsn.PartyId
    setAttributeForInformasiUmum(partyId: string) {
        let url = '/SetupPasien/GetAllPersonsByParams';

        let params = {
            columnName: "prsn.PartyId",
            filter: "equal",
            searchText: partyId
        };

        let informasiUmum = [
            { formControlName: "FirstName", value: "FirstName", isInt: false },
            { formControlName: "Sex", value: "Sex", isInt: false },
            { formControlName: "Agama", value: "Agama", isInt: false },
            { formControlName: "TemplatLahir", value: "TemplatLahir", isInt: false },
            { formControlName: "TanggalLahir", value: "TanggalLahir", isInt: false },
            { formControlName: "MaritalStatus", value: "MaritalStatus", isInt: true },
            { formControlName: "Pendidikan.PartyId", value: "PartyId", isInt: true },
            { formControlName: "Pendidikan.KodePendidikan", value: "KodePendidikan", isInt: true },
            { formControlName: "Pekerjaan.PartyId", value: "PartyId", isInt: true },
            { formControlName: "Pekerjaan.KodePekerjaan", value: "KodePekerjaan", isInt: true },
            { formControlName: "GolDarah", value: "GolDarah", isInt: true },
            { formControlName: "Keterbatasan.PartyId", value: "PartyId", isInt: true },
            { formControlName: "Keterbatasan.IdKeterbatasanFisik", value: "IdKeterbatasanFisik", isInt: true },
            { formControlName: "EthnicCode", value: "EthnicCode", isInt: false },
            { formControlName: "NatCode", value: "NatCode", isInt: false },
            { formControlName: "IdBahasa", value: "IdBahasa", isInt: true },
            { formControlName: "Bahasa", value: "Bahasa", isInt: false },
            { formControlName: "Telp", value: "Telp", isInt: false },
            { formControlName: "Hp", value: "Hp", isInt: false },
        ];

        this.setupPasienService.onGetDataPasien(url, params, this.formSetupPasien, informasiUmum)
            .subscribe((_result) => {
                this.AgamaText.nativeElement.value = _result[0]["AgamaText"];
                this.MaritalStatusText.nativeElement.value = _result[0]["MaritalStatusText"];
                this.KodePendidikanText.nativeElement.value = _result[0]["KodePendidikanText"];
                this.KodePekerjaanText.nativeElement.value = _result[0]["KodePekerjaanText"];
                this.EthnicCodeText.nativeElement.value = _result[0]["EthnicCodeText"];
                this.NatCodeText.nativeElement.value = _result[0]["NatCodeText"];

                setTimeout(() => {
                    let age = this.utilityService.getAgeUsingMoment(this.TanggalLahir.value);

                    this.TahunPasien.nativeElement.value = age.years;
                    this.BulanPasien.nativeElement.value = age.months;
                    this.HariPasien.nativeElement.value = age.days;
                }, 200);
            })
    }

    // ... Set attribute for Informasi Pasien => psn.PartyId
    setAttributeForInformasiPasien(partyId: string) {
        let url = '/SetupPasien/GetAllPasienByParams';

        let params = {
            columnName: "psn.PartyId",
            filter: "equal",
            searchText: partyId
        };

        let informasiPasien = [
            { formControlName: "PartyId", value: "PartyId", isInt: true },
            { formControlName: "Pasien.MrNo", value: "MrNo", isInt: false }
        ];

        this.setupPasienService.onGetDataPasien(url, params, this.formSetupPasien, informasiPasien)
            .subscribe((_result) => {
            })
    }

    // ... Set attribute for Informasi Address Pasien
    setAttributeForInformasiAddressPasien(partyId: string) {
        let url = '/SetupPasien/GetAllPasienAddressByParams';

        let params = {
            columnName: "PartyId",
            filter: "equal",
            searchText: partyId
        };

        let informasiAddressPasien = [
            { formControlName: "PasienAddress.IdAlamat", value: "IdAlamat", isInt: true },
            { formControlName: "PasienAddress.Alamat", value: "Alamat", isInt: false },
            { formControlName: "PasienAddress.Rt", value: "Rt", isInt: false },
            { formControlName: "PasienAddress.Rw", value: "Rw", isInt: false },
            { formControlName: "PasienAddress.Provinsi", value: "Provinsi", isInt: false },
            { formControlName: "PasienAddress.Kota", value: "Kota", isInt: false },
            { formControlName: "PasienAddress.Kecamatan", value: "Kecamatan", isInt: false },
            { formControlName: "PasienAddress.Kelurahan", value: "Kelurahan", isInt: false },
            { formControlName: "PasienAddress.KodePos", value: "KodePos", isInt: false },
        ];

        this.setupPasienService.onGetDataPasien(url, params, this.formSetupPasien, informasiAddressPasien)
            .subscribe((_result) => {
                this.ProvinsiText.nativeElement.value = _result[0]["ProvinsiText"];
                this.KotaText.nativeElement.value = _result[0]["KotaText"];
                this.KecamatanText.nativeElement.value = _result[0]["KecamatanText"];
                this.KelurahanText.nativeElement.value = _result[0]["KelurahanText"];
            })
    }

    // ... Set attribute for Informasi Kartu Identitas
    setAttributeForKartuIdentitasPasien(partyId: string) {
        let url = '/SetupPasien/GetAllKartuIdentitasByParams';

        let params = {
            columnName: "PartyId",
            filter: "equal",
            searchText: partyId
        };

        let informasiAddressPasien = [
            { formControlName: "KartuIdentitas.PartyId", value: "PartyId", isInt: true },
            { formControlName: "KartuIdentitas.KodeJenisKartu", value: "KodeJenisKartu", isInt: false },
            { formControlName: "KartuIdentitas.NomorKartu", value: "NomorKartu", isInt: false },
            { formControlName: "KartuIdentitas.FromDate", value: "FromDate", isInt: false },
            { formControlName: "KartuIdentitas.ThruDate", value: "ThruDate", isInt: false }
        ];

        this.setupPasienService.onGetDataPasien(url, params, this.formSetupPasien, informasiAddressPasien)
            .subscribe((_result) => {
                this.KodeJenisKartuText.nativeElement.value = _result[0]["KodeJenisKartuText"];
            })
    }

    // ... Set attribute for Informasi Keluarga Pasien
    setAttributeForInformasiKeluargaPasien(partyId: string) {
        let url = '/SetupPasien/GetAllPasienKeluargaByParams';

        let params = {
            columnName: "PartyId",
            filter: "equal",
            searchText: partyId
        };

        let informasiAddressPasien = [
            { formControlName: "Keluarga.NamaSuamiIstri", value: "NamaSuamiIstri", isInt: false },
            { formControlName: "Keluarga.TelpKeluarga", value: "TelpKeluarga", isInt: false },
            { formControlName: "Keluarga.AnakKe", value: "AnakKe", isInt: false },
            { formControlName: "Keluarga.Dari", value: "Dari", isInt: false },
            { formControlName: "Keluarga.AkteLahir", value: "AkteLahir", isInt: false }
        ];

        this.setupPasienService.onGetDataKeluargaPasien(url, params, this.formSetupPasien, informasiAddressPasien)
            .subscribe((_result) => { })
    }

    // ... Set attribute for Informasi Ortu Pasien
    setAttributeForInformasiOrtuPasien(partyId: string) {
        let url = '/SetupPasien/GetAllPasienParentsByParams';

        let params = {
            columnName: "PartyId",
            filter: "equal",
            searchText: partyId
        };

        let informasiAddressPasien = [
            { formControlName: "Ayah.ParentIdOrtuAyah", value: "ParentIdOrtuAyah", isInt: true },
            { formControlName: "Ayah.NamaOrtuAyah", value: "NamaOrtuAyah", isInt: false },
            { formControlName: "Ayah.TglLahirOrtuAyah", value: "TglLahirOrtuAyah", isInt: false },
            { formControlName: "Ayah.AlamatOrtuAyah", value: "AlamatOrtuAyah", isInt: false },
            { formControlName: "Ayah.KotaOrtuAyah", value: "KotaOrtuAyah", isInt: false },
            { formControlName: "Ayah.KecamatanOrtuAyah", value: "KecamatanOrtuAyah", isInt: false },
            { formControlName: "Ayah.KodePekerjaanOrtuAyah", value: "KodePekerjaanOrtuAyah", isInt: false },

            { formControlName: "Ibu.ParentIdOrtuIbu", value: "ParentIdOrtuIbu", isInt: true },
            { formControlName: "Ibu.NamaOrtuIbu", value: "NamaOrtuIbu", isInt: false },
            { formControlName: "Ibu.TglLahirOrtuIbu", value: "TglLahirOrtuIbu", isInt: false },
            { formControlName: "Ibu.AlamatOrtuIbu", value: "AlamatOrtuIbu", isInt: false },
            { formControlName: "Ibu.KotaOrtuIbu", value: "KotaOrtuIbu", isInt: false },
            { formControlName: "Ibu.KecamatanOrtuIbu", value: "KecamatanOrtuIbu", isInt: false },
            { formControlName: "Ibu.KodePekerjaanOrtuIbu", value: "KodePekerjaanOrtuIbu", isInt: false },
        ];

        this.setupPasienService.onGetDataKeluargaPasien(url, params, this.formSetupPasien, informasiAddressPasien)
            .subscribe((_result) => {
                this.KotaOrtuAyahText.nativeElement.value = _result["KotaOrtuAyahText"];
                this.KecamatanOrtuAyahText.nativeElement.value = _result["KecamatanOrtuAyahText"];
                this.KodePekerjaanOrtuAyahText.nativeElement.value = _result["KodePekerjaanOrtuAyahText"];

                this.KotaOrtuIbuText.nativeElement.value = _result["KotaOrtuIbuText"];
                this.KecamatanOrtuIbuText.nativeElement.value = _result["KecamatanOrtuIbuText"];
                this.KodePekerjaanOrtuIbuText.nativeElement.value = _result["KodePekerjaanOrtuIbuText"];

                setTimeout(() => {
                    let ageAyah = this.utilityService.getAgeUsingMoment(this.TglLahirOrtuAyah.value);

                    this.TahunAyah.nativeElement.value = ageAyah.years;
                    this.BulanAyah.nativeElement.value = ageAyah.months;
                    this.HariAyah.nativeElement.value = ageAyah.days;

                    let ageIbu = this.utilityService.getAgeUsingMoment(this.TglLahirOrtuIbu.value);

                    this.TahunIbu.nativeElement.value = ageIbu.years;
                    this.BulanIbu.nativeElement.value = ageIbu.months;
                    this.HariIbu.nativeElement.value = ageIbu.days;
                }, 200);
            })
    }

    // ... Initiate Tab Header
    onFetchTabHeader() {
        this.setupPasienService.getTabHeader()
            .subscribe((_result) => {
                this.tabHeader = _result;
            })
    }

    // ... Combobox Pasien Attribute
    onSetComboboxAttribute() {
        this.setupPasienService.getDataFromSetupPasien("/SetupPasien/GetAllSex")
            .subscribe((_sex) => {
                this.dataSex = _sex;

                this.fieldsSex = { text: 'Text', value: 'Value' };
            });

        this.setupPasienService.getDataFromSetupPasien("/SetupPasien/GetAllGolDarah")
            .subscribe((_golDarah) => {
                this.dataGolDarah = _golDarah;

                this.fieldsGolDarah = { text: 'Text', value: 'Value' };
            });

        this.setupPasienService.getDataFromSetupPasien("/SetupPasien/GetAllKeterbatasanFisik")
            .subscribe((_keterbatasan) => {
                this.dataKeterbatasan = _keterbatasan;

                this.fieldsKeterbatasan = { text: 'Text', value: 'Value' };
            });

        this.setupPasienService.getDataFromSetupPasien("/SetupPasien/GetAllBahasa")
            .subscribe((_bahasa) => {
                this.dataBahasa = _bahasa;

                this.fieldsBahasa = { text: 'Text', value: 'Value' };
            });
    }

    // ... Combobox Jenis Kelamin
    onChangeSex(args: any) {
        // let itemData = args.itemData;
        // this.Sex.setValue(itemData.Value);
    }

    // ... Combobox Golongan Darah
    onChangeGolDarah(args: any) {
        console.log(args);
    }

    // ... Combobox Bahasa
    onChangeBahasa(args: any) {
        this.Bahasa.setValue(args.itemData.Text);
    }

    // ... Datepicker Tanggal Lahir Properties
    onChangeTanggalLahir(args: any) {
        let elementId = args.element.id;

        if (elementId == "TanggalLahir") {
            let age = this.utilityService.getAgeUsingMoment(args.value);

            this.TahunPasien.nativeElement.value = age.years;
            this.BulanPasien.nativeElement.value = age.months;
            this.HariPasien.nativeElement.value = age.days;
        }
        else if (elementId == "TglLahirOrtuAyah") {
            let age = this.utilityService.getAgeUsingMoment(args.value);

            this.TahunAyah.nativeElement.value = age.years;
            this.BulanAyah.nativeElement.value = age.months;
            this.HariAyah.nativeElement.value = age.days;
        }
        else if (elementId == "TglLahirOrtuIbu") {
            let age = this.utilityService.getAgeUsingMoment(args.value);

            this.TahunIbu.nativeElement.value = age.years;
            this.BulanIbu.nativeElement.value = age.months;
            this.HariIbu.nativeElement.value = age.days;
        } else {
            //... do nothing
        }

    }

    // ... Lookup Get Selected Data Agama
    onSetLookupAgamaAttribute() {
        this.setupPasienService.getLookupAgamaColumns()
            .subscribe((_result) => {
                this.modalColumnsItemLookup = _result;
            });

        this.setupPasienService.getLookupAgamaFilters()
            .subscribe((_result) => {
                this.modalFiltersItemLookup = _result;
            });
    }

    onGetSelectedDataAgama(args: any) {
        this.AgamaText.nativeElement.value = args.Agama;
        this.Agama.setValue(args.Id);
    }

    // ... Lookup Get Selected Data Marital Status
    onSetLookupMaritalAttribute() {
        this.setupPasienService.getLookupMaritalColumns()
            .subscribe((_result) => {
                this.modalColumnsItemLookup = _result;
            });

        this.setupPasienService.getLookupMaritalFilters()
            .subscribe((_result) => {
                this.modalFiltersItemLookup = _result;
            });
    }

    onGetSelectedDataMarital(args: any) {
        this.MaritalStatus.setValue(args.Id);
        this.MaritalStatusText.nativeElement.value = args.MaritalStatus;
    }

    // ... Lookup Get Selected Data Pendidikan
    onSetLookupPendidikanAttribute() {
        this.setupPasienService.getLookupPendidikanColumns()
            .subscribe((_result) => {
                this.modalColumnsItemLookup = _result;
            });

        this.setupPasienService.getLookupPendidikanFilters()
            .subscribe((_result) => {
                console.log(_result);
                this.modalFiltersItemLookup = _result;
            });
    }

    onGetSelectedDataPendidikan(args: any) {
        this.KodePendidikanText.nativeElement.value = args.NamaPendidikan;
        this.KodePendidikan.setValue(args.KodePendidikan);
    }

    // ... Lookup Get Selected Data Pekerjaan
    onSetLookupPekerjaanAttribute() {
        this.setupPasienService.getLookupPekerjaanColumns()
            .subscribe((_result) => {
                this.modalColumnsItemLookup = _result;
            });

        this.setupPasienService.getLookupPekerjaanFilters()
            .subscribe((_result) => {
                console.log(_result);
                this.modalFiltersItemLookup = _result;
            });
    }

    onGetSelectedDataPekerjaan(args: any, elementId: string) {
        console.log(args);

        if (elementId == "modalPekerjaan") {
            this.KodePekerjaanText.nativeElement.value = args.NamaPekerjaan;
            this.KodePekerjaan.setValue(parseInt(args.KodePekerjaan));
        } else if (elementId == "modalPekerjaanAyah") {
            this.KodePekerjaanOrtuAyahText.nativeElement.value = args.NamaPekerjaan;
            this.KodePekerjaanOrtuAyah.setValue(args.KodePekerjaan);
        } else {
            this.KodePekerjaanOrtuIbuText.nativeElement.value = args.NamaPekerjaan;
            this.KodePekerjaanOrtuIbu.setValue(args.KodePekerjaan);
        }
    }

    // ... Lookup Get Selected Data Provinsi
    onSetLookupProvinsiAttribute() {
        this.setupPasienService.getLookupProvinsiColumns()
            .subscribe((_result) => {
                // console.log(_result);
                this.modalColumnsItemLookup = _result;
            });

        this.setupPasienService.getLookupProvinsiFilters()
            .subscribe((_result) => {
                // console.log(_result);
                this.modalFiltersItemLookup = _result;
            });
    }

    onGetSelectedDataProvinsi(args: any) {
        // console.log(args);

        this.ProvinsiText.nativeElement.value = args.Nama;
        this.Provinsi.setValue(args.Id);
    }

    // ... Lookup Get Selected Data Kota
    onSetLookupKotaAttribute() {
        this.setupPasienService.getLookupKotaColumns()
            .subscribe((_result) => {
                this.modalColumnsItemLookup = _result;
            });

        this.setupPasienService.getLookupKotaFilters()
            .subscribe((_result) => {
                this.modalFiltersItemLookup = _result;
            });
    }

    onGetSelectedDataKota(args: any, elementId: string) {
        if (elementId == "modalKota") {
            this.KotaText.nativeElement.value = args.Nama;
            this.Kota.setValue(args.Id);
        } else if (elementId == "modalKotaAyah") {
            this.KotaOrtuAyahText.nativeElement.value = args.Nama;
            this.KotaOrtuAyah.setValue(args.Id);
        } else {
            this.KotaOrtuIbuText.nativeElement.value = args.Nama;
            this.KotaOrtuIbu.setValue(args.Id);
        }
    }

    // ... Lookup Get Selected Data Kecamatan
    onSetLookupKecamatanAttribute() {
        this.setupPasienService.getLookupKecamatanColumns()
            .subscribe((_result) => {
                this.modalColumnsItemLookup = _result;
            });

        this.setupPasienService.getLookupKecamatanFilters()
            .subscribe((_result) => {
                console.log(_result);
                this.modalFiltersItemLookup = _result;
            });
    }

    onGetSelectedDataKecamatan(args: any, elementId: string) {
        if (elementId == "modalKecamatan") {
            this.KecamatanText.nativeElement.value = args.Nama;
            this.Kecamatan.setValue(args.Id);
        } else if (elementId == "modalKecamatanAyah") {
            this.KecamatanOrtuAyahText.nativeElement.value = args.Nama;
            this.KecamatanOrtuAyah.setValue(args.Id);
        } else {
            this.KecamatanOrtuIbuText.nativeElement.value = args.Nama;
            this.KecamatanOrtuIbu.setValue(args.Id);
        }
    }

    // ... Lookup Get Selected Data Kelurahan
    onSetLookupKelurahanAttribute() {
        this.setupPasienService.getLookupKelurahanColumns()
            .subscribe((_result) => {
                this.modalColumnsItemLookup = _result;
            });

        this.setupPasienService.getLookupKelurahanFilters()
            .subscribe((_result) => {
                console.log(_result);
                this.modalFiltersItemLookup = _result;
            });
    }

    onGetSelectedDataKelurahan(args: any) {
        this.KelurahanText.nativeElement.value = args.Nama;
        this.Kelurahan.setValue(args.Id);
    }

    // ... Lookup Get Selected Data Jenis Kartu
    onSetLookupJenisKartuAttribute() {
        this.setupPasienService.getLookupJenisKartuColumns()
            .subscribe((_result) => {
                this.modalColumnsItemLookup = _result;
            });

        this.setupPasienService.getLookupJenisKartuFilters()
            .subscribe((_result) => {
                // console.log(_result);
                this.modalFiltersItemLookup = _result;
            });
    }

    onGetSelectedDataJenisKartu(args: any) {
        this.KodeJenisKartuText.nativeElement.value = args.NamaJenisKartu;
        this.KodeJenisKartu.setValue(args.KodeJenisKartu);
    }

    // ... Lookup Get Selected Data Suku
    onSetLookupSukuAttribute() {
        this.setupPasienService.getLookupSukuColumns()
            .subscribe((_result) => {
                this.modalColumnsItemLookup = _result;
            });

        this.setupPasienService.getLookupSukuFilters()
            .subscribe((_result) => {
                this.modalFiltersItemLookup = _result;
            });
    }

    onGetSelectedDataSuku(args: any) {
        this.EthnicCodeText.nativeElement.value = args.EthName;
        this.EthnicCode.setValue(args.EthCode);
    }

    // ... Lookup Get Selected Data Kebangsaan
    onSetLookupKebangsaanAttribute() {
        this.setupPasienService.getLookupKebangsaanColumns()
            .subscribe((_result) => {
                this.modalColumnsItemLookup = _result;
            });

        this.setupPasienService.getLookupKebangsaanFilters()
            .subscribe((_result) => {
                this.modalFiltersItemLookup = _result;
            });
    }

    onGetSelectedDataKebangsaan(args: any) {
        // console.log(args);
        this.NatCodeText.nativeElement.value = args.NatName;
        this.NatCode.setValue(args.NatCode);
    }

    // ... Initiate Filters and Columns Lookup after modal open
    onOpenLookup(id: any) {
        console.log(id);

        this.modalColumnsItemLookup = [];
        this.modalFiltersItemLookup = [];

        if (id == "modalAgama") {
            this.onSetLookupAgamaAttribute();
        }
        else if (id == "modalMarital") {
            this.onSetLookupMaritalAttribute();
        }
        else if (id == "modalPendidikan") {
            this.onSetLookupPendidikanAttribute();
        }
        else if (id == "modalPekerjaan" || id == "modalPekerjaanAyah" || id == "modalPekerjaanIbu") {
            this.onSetLookupPekerjaanAttribute();
        }
        else if (id == "modalProvinsi") {
            this.onSetLookupProvinsiAttribute();
        }
        else if (id == "modalKota") {
            this.modalStaticFilterLookup = [
                {
                    "columnName": "IdProvinsi",
                    "filter": "equal",
                    "searchText": this.Provinsi.value,
                    "searchText2": ""
                }
            ];

            this.onSetLookupKotaAttribute();
        }
        else if (id == "modalKotaAyah" || id == "modalKotaIbu") {
            this.modalStaticFilterLookup = [
                {
                    "columnName": "IdProvinsi",
                    "filter": "like",
                    "searchText": "",
                    "searchText2": ""
                }
            ];

            this.onSetLookupKotaAttribute();
        }
        else if (id == "modalKecamatan") {
            this.modalStaticFilterLookup = [
                {
                    "columnName": "IdKota",
                    "filter": "equal",
                    "searchText": this.Kota.value,
                    "searchText2": ""
                }
            ];

            this.onSetLookupKecamatanAttribute();
        }
        else if (id == "modalKecamatanAyah") {
            this.modalStaticFilterLookup = [
                {
                    "columnName": "IdKota",
                    "filter": "equal",
                    "searchText": this.KotaOrtuAyah.value,
                    "searchText2": ""
                }
            ];

            this.onSetLookupKecamatanAttribute();
        }
        else if (id == "modalKecamatanIbu") {
            this.modalStaticFilterLookup = [
                {
                    "columnName": "IdKota",
                    "filter": "equal",
                    "searchText": this.KotaOrtuIbu.value,
                    "searchText2": ""
                }
            ];

            this.onSetLookupKecamatanAttribute();
        }
        else if (id == "modalKelurahan") {
            this.modalStaticFilterLookup = [
                {
                    "columnName": "IdKecamatan",
                    "filter": "equal",
                    "searchText": this.Kecamatan.value,
                    "searchText2": ""
                }
            ];

            this.onSetLookupKelurahanAttribute();
        }
        else if (id == "modalJenisKartu") {
            this.onSetLookupJenisKartuAttribute();
        }
        else if (id == "modalSuku") {
            this.onSetLookupSukuAttribute();
        }
        else if (id == "modalKebangsaan") {
            this.onSetLookupKebangsaanAttribute();
        }
        else {
            this.modalColumnsItemLookup = [];
            this.modalFiltersItemLookup = [];
        }
    }

    // ... Initiate Submit Form
    onSubmitForm(value: any) {
        console.log(value);

        this.setupPasienService.onSubmitForm(value)
            .subscribe(
                (_result) => {
                    Swal.fire({
                        title: 'Data Berhasil Disimpan',
                        icon: "success",
                    }).then(() => {
                        this.formSetupPasien.reset();
                        this.onClearForm();
                    });
                },
                (_error) => {
                    Swal.fire({
                        title: 'Oops...',
                        icon: "error",
                        html: _error
                    });
                })
    }

    // ... Clear form value after submit 
    onClearForm() {
        this.AgamaText.nativeElement.value = "";
        this.MaritalStatusText.nativeElement.value = "";
        this.KodePendidikanText.nativeElement.value = "";
        this.KodePekerjaanText.nativeElement.value = "";
        this.ProvinsiText.nativeElement.value = "";
        this.KotaText.nativeElement.value = "";
        this.KecamatanText.nativeElement.value = "";
        this.KelurahanText.nativeElement.value = "";
        this.KodeJenisKartuText.nativeElement.value = "";
        this.EthnicCodeText.nativeElement.value = "";
        this.NatCodeText.nativeElement.value = "";
        this.KotaOrtuAyahText.nativeElement.value = "";
        this.KecamatanOrtuAyahText.nativeElement.value = "";
        this.KodePekerjaanOrtuAyahText.nativeElement.value = "";
        this.KotaOrtuIbuText.nativeElement.value = "";
        this.KecamatanOrtuIbuText.nativeElement.value = "";
        this.KodePekerjaanOrtuIbuText.nativeElement.value = "";

        this.TahunPasien.nativeElement.value = "";
        this.BulanPasien.nativeElement.value = "";
        this.HariPasien.nativeElement.value = "";

        this.TahunAyah.nativeElement.value = "";
        this.BulanAyah.nativeElement.value = "";
        this.HariAyah.nativeElement.value = "";

        this.TahunIbu.nativeElement.value = "";
        this.BulanIbu.nativeElement.value = "";
        this.HariIbu.nativeElement.value = "";
    }

    // ... Initiate FormGroup Attribute
    onSetFormGroupAttribute() {
        this.formSetupPasien = this.formBuilder.group({
            "PartyId": [0, []],
            "FirstName": ["", Validators.required],
            "MiddleName": ["", []],
            "LastName": ["", []],
            "FrontTitle": ["", []],
            "EndTitle": ["", []],
            "Sex": [0, Validators.required],
            "Agama": [0, Validators.required],
            "TemplatLahir": ["", Validators.required],
            "TanggalLahir": ["", Validators.required],
            "Height": [0, []],
            "Weight": [0, []],
            "MaritalStatus": [0, Validators.required],
            "GolDarah": [0, Validators.required],
            "Telp": ["", Validators.required],
            "Hp": ["", Validators.required],
            "EthnicCode": ["", Validators.required],
            "NatCode": ["", Validators.required],
            "IdBahasa": [0, Validators.required],
            "Bahasa": ["", Validators.required],

            Pasien: this.formBuilder.group({
                "MrNo": ['0', []],
                "DebiturMemberNo": ["", []],
                "ExpDebiturMember": [null, []],
                "Visits": [0, []],
                "LastVisitDate": [null, []],
                "HistoryVisitDate": [null, []],
                "IsBridge": [false, []],
                "PhysicalLimitation": [0, []],
                "IrnaSirusVisits": [0, []],
                "LastNote": ["", []],
            }),
            KartuIdentitas: this.formBuilder.group({
                "PartyId": [0, []],
                "KodeJenisKartu": ["", Validators.required],
                "NomorKartu": ["", Validators.required],
                "FromDate": ["1900-01-01", []],
                "ThruDate": [null, []],
            }),
            PasienAddress: this.formBuilder.group({
                "IdAlamat": [null, []],
                "Alamat": ["", Validators.required],
                "Rt": ["", Validators.required],
                "Rw": ["", Validators.required],
                "Provinsi": ["", Validators.required],
                "Kota": ["", Validators.required],
                "Kecamatan": ["", Validators.required],
                "Kelurahan": ["", Validators.required],
                "KodePos": ["", Validators.required],
            }),
            Keluarga: this.formBuilder.group({
                "NamaSuamiIstri": ["", Validators.required],
                "TelpKeluarga": ["", Validators.required],
                "AnakKe": ["", Validators.required],
                "Dari": ["", Validators.required],
                "AkteLahir": ["", Validators.required],
            }),
            Ayah: this.formBuilder.group({
                "ParentIdOrtuAyah": [null, Validators.required],
                "NamaOrtuAyah": ["", Validators.required],
                "TglLahirOrtuAyah": ["", Validators.required],
                "AlamatOrtuAyah": ["", Validators.required],
                "KotaOrtuAyah": ["", Validators.required],
                "KecamatanOrtuAyah": ["", Validators.required],
                "KodePekerjaanOrtuAyah": ["", Validators.required],
            }),
            Ibu: this.formBuilder.group({
                "ParentIdOrtuIbu": [null, Validators.required],
                "NamaOrtuIbu": ["", Validators.required],
                "TglLahirOrtuIbu": ["", Validators.required],
                "AlamatOrtuIbu": ["", Validators.required],
                "KotaOrtuIbu": ["", Validators.required],
                "KecamatanOrtuIbu": ["", Validators.required],
                "KodePekerjaanOrtuIbu": ["", Validators.required],
            }),
            Pendidikan: this.formBuilder.group({
                "PartyId": [0, []],
                "KodePendidikan": [0, Validators.required],
            }),
            Pekerjaan: this.formBuilder.group({
                "PartyId": [0, []],
                "KodePekerjaan": [0, Validators.required],
            }),
            Keterbatasan: this.formBuilder.group({
                "PartyId": [0, []],
                "IdKeterbatasanFisik": [0, Validators.required],
            }),
        })
    }

    get Pasien() { return this.formSetupPasien.get("Pasien") }
    get KartuIdentitas() { return this.formSetupPasien.get("KartuIdentitas") }
    get Keluarga() { return this.formSetupPasien.get("Keluarga") }
    get Ayah() { return this.formSetupPasien.get("Ayah") }
    get Ibu() { return this.formSetupPasien.get("Ibu") }
    get Pendidikan() { return this.formSetupPasien.get("Pendidikan") }
    get Pekerjaan() { return this.formSetupPasien.get("Pekerjaan") }
    get Keterbatasan() { return this.formSetupPasien.get("Keterbatasan") }

    get MrNo() { return this.formSetupPasien.get("Pasien.MrNo") }
    get FirstName() { return this.formSetupPasien.get("FirstName") }
    get Sex() { return this.formSetupPasien.get("Sex") }
    get Agama() { return this.formSetupPasien.get("Agama") }
    get TemplatLahir() { return this.formSetupPasien.get("TemplatLahir") }
    get TanggalLahir() { return this.formSetupPasien.get("TanggalLahir") }
    get MaritalStatus() { return this.formSetupPasien.get("MaritalStatus") }
    get KodePendidikan() { return this.formSetupPasien.get("Pendidikan.KodePendidikan") }
    get KodePekerjaan() { return this.formSetupPasien.get("Pekerjaan.KodePekerjaan") }
    get GolDarah() { return this.formSetupPasien.get("GolDarah") }
    get IdKeterbatasanFisik() { return this.formSetupPasien.get("Keterbatasan.IdKeterbatasanFisik") }
    get Alamat() { return this.formSetupPasien.get("PasienAddress.Alamat") }
    get Rt() { return this.formSetupPasien.get("PasienAddress.Rt") }
    get Rw() { return this.formSetupPasien.get("PasienAddress.Rw") }
    get Provinsi() { return this.formSetupPasien.get("PasienAddress.Provinsi") }
    get Kota() { return this.formSetupPasien.get("PasienAddress.Kota") }
    get Kecamatan() { return this.formSetupPasien.get("PasienAddress.Kecamatan") }
    get Kelurahan() { return this.formSetupPasien.get("PasienAddress.Kelurahan") }
    get KodePos() { return this.formSetupPasien.get("PasienAddress.KodePos") }
    get Telp() { return this.formSetupPasien.get("Telp") }
    get Hp() { return this.formSetupPasien.get("Hp") }
    get KodeJenisKartu() { return this.formSetupPasien.get("KartuIdentitas.KodeJenisKartu") }
    get NomorKartu() { return this.formSetupPasien.get("KartuIdentitas.NomorKartu") }
    get EthnicCode() { return this.formSetupPasien.get("EthnicCode") }
    get NatCode() { return this.formSetupPasien.get("NatCode") }
    get IdBahasa() { return this.formSetupPasien.get("IdBahasa") }
    get Bahasa() { return this.formSetupPasien.get("Bahasa") }
    get NamaSuamiIstri() { return this.formSetupPasien.get("Keluarga.NamaSuamiIstri") }
    get TelpKeluarga() { return this.formSetupPasien.get("Keluarga.TelpKeluarga") }
    get AnakKe() { return this.formSetupPasien.get("Keluarga.AnakKe") }
    get Dari() { return this.formSetupPasien.get("Keluarga.Dari") }
    get AkteLahir() { return this.formSetupPasien.get("Keluarga.AkteLahir") }
    get NamaOrtuAyah() { return this.formSetupPasien.get("Ayah.NamaOrtuAyah") }
    get TglLahirOrtuAyah() { return this.formSetupPasien.get("Ayah.TglLahirOrtuAyah") }
    get AlamatOrtuAyah() { return this.formSetupPasien.get("Ayah.AlamatOrtuAyah") }
    get KotaOrtuAyah() { return this.formSetupPasien.get("Ayah.KotaOrtuAyah") }
    get KecamatanOrtuAyah() { return this.formSetupPasien.get("Ayah.KecamatanOrtuAyah") }
    get KodePekerjaanOrtuAyah() { return this.formSetupPasien.get("Ayah.KodePekerjaanOrtuAyah") }
    get NamaOrtuIbu() { return this.formSetupPasien.get("Ibu.NamaOrtuIbu") }
    get TglLahirOrtuIbu() { return this.formSetupPasien.get("Ibu.TglLahirOrtuIbu") }
    get AlamatOrtuIbu() { return this.formSetupPasien.get("Ibu.AlamatOrtuIbu") }
    get KotaOrtuIbu() { return this.formSetupPasien.get("Ibu.KotaOrtuIbu") }
    get KecamatanOrtuIbu() { return this.formSetupPasien.get("Ibu.KecamatanOrtuIbu") }
    get KodePekerjaanOrtuIbu() { return this.formSetupPasien.get("Ibu.KodePekerjaanOrtuIbu") }
}
