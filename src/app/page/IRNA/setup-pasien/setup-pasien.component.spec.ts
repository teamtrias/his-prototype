import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupPasienComponent } from './setup-pasien.component';

describe('SetupPasienComponent', () => {
  let component: SetupPasienComponent;
  let fixture: ComponentFixture<SetupPasienComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetupPasienComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupPasienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
