import { Component, OnInit } from '@angular/core';
import { EditSettingsModel } from '@syncfusion/ej2-angular-grids';
import { HttpRequestService } from 'src/app/service/http-request/http-request.service';
import { SetupTarifService } from 'src/app/service/page/setup-tarif.service';

@Component({
    selector: 'app-setup-tarif',
    templateUrl: './setup-tarif.component.html',
    styleUrls: ['./setup-tarif.component.css']
})
export class SetupTarifComponent implements OnInit {

    tabHeader: any = [];

    gridId: string = "GridGroup";
    gridWidth: any = 'auto';
    gridHeight: any = 250;
    gridDataSource: any = [];
    gridLines: string = 'Both';
    gridColumns: any;
    gridPageSettings: Object;
    gridToolbars: any;
    gridEditSettings: EditSettingsModel;

    rowSelectedInGridGroup: any = [];

    constructor(private httpRequestService: HttpRequestService, private setupTarifService: SetupTarifService) { }

    ngOnInit(): void {
        this.onGetTabHeader();
        this.onSetGridGrupProperty();

        this.onFetchGridGrupDatasource();
    }

    onGetTabHeader(url: string = "setupTarifTabHeader.json") {
        this.httpRequestService.defaultGetRequestWithoutMap(url)
            .subscribe(
                (_result) => {
                    this.tabHeader = _result;
                    console.clear();
                }
            )
    }

    onSetGridGrupProperty() {
        this.gridToolbars = ['Add', 'Edit', 'Delete', 'Search'];

        this.gridEditSettings = { allowDeleting: true, allowAdding: true, allowEditing: true, mode: 'Normal' };

        this.gridPageSettings = { pageSizes: true, pageCount: 4, pageSize: 8 };

        this.gridColumns = [
            { width: 80, field: 'KodeTarif', headerText: 'KODE', visibility: true, headerTextAlign: 'Center', },
            { width: 80, field: 'KeteranganTarif', headerText: 'KETERANGAN', visibility: true, headerTextAlign: 'Center', },
        ];
    }

    onFetchGridGrupDatasource() {
        this.httpRequestService.defaultGetRequest('setupTarif.json', [])
            .subscribe((_result) => {
                console.log(_result);

                this.gridDataSource = _result;
            })
    }

    rowSelectedGridGroup(args: any) {
        // console.log(args);
        this.rowSelectedInGridGroup = args.data;
    }

    actionBeginGroup(args: any) {
        let action = args.action;
        let requestType = args.requestType;
        let data = args.data;

        // add, cancel, save, beginEdit, delete
        console.log(args);

        if (action == "add" && requestType == "save") {
            this.setupTarifService.onSubmitSetupTarif(data, this.gridDataSource);
        }
        else if (action == "edit" && requestType == "save") {
            console.log(this.rowSelectedInGridGroup);

            this.setupTarifService.onEditSetupTarif(this.rowSelectedInGridGroup, data);
        }
        else if (requestType == "delete") {
            console.log(this.rowSelectedInGridGroup);
        }
        else {
            //... do nothing
        }
    }

}
