import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupHargaTarifComponent } from './setup-harga-tarif.component';

describe('SetupHargaTarifComponent', () => {
  let component: SetupHargaTarifComponent;
  let fixture: ComponentFixture<SetupHargaTarifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetupHargaTarifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupHargaTarifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
