import { formatDate } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { DatePickerComponent } from '@syncfusion/ej2-angular-calendars';
import { SharedGridComponent } from 'src/app/components/shared-grid/shared-grid.component';
import { BrowsePenjualanObatService } from 'src/app/service/page/browse-penjualan-obat.service';
import * as moment from 'moment';
import { UtilityService } from 'src/app/service/utility/utility.service';
import { dataBound } from '@syncfusion/ej2-grids';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-browse-penjualan-obat',
    templateUrl: './browse-penjualan-obat.component.html',
    styleUrls: ['./browse-penjualan-obat.component.css']
})
export class BrowsePenjualanObatComponent implements OnInit, AfterViewInit {

    @ViewChild("TglPreviewDatepicker") TglPreviewDatepicker: DatePickerComponent;

    @ViewChild("Pencarian") Pencarian: HTMLInputElement;

    // ... History Header
    gridHistoryHeader: SharedGridComponent = null;
    gridHistoryHeaderDatasource: any = [];
    gridHistoryHeaderColumns: any = [];
    gridHistoryHeaderSelectedData: any = [];

    // ... History Detail
    gridHistoryDetail: SharedGridComponent = null;
    gridHistoryDetailDatasource: any = [];
    gridHistoryDetailColumns: any = [];

    constructor(
        private utilityService: UtilityService,
        private browsePenjualanObatService: BrowsePenjualanObatService
    ) { }

    ngOnInit(): void {
    }

    ngAfterViewInit() {
        this.onSetGridProperties();
    }

    // ... Untuk Set Grid Properties => Columns ...
    onSetGridProperties() {
        this.browsePenjualanObatService.onFetchGridColumn("Farmasi/BrowsePenjualanObat/GridHistory/HeaderHistory.json")
            .subscribe((_result) => {
                this.gridHistoryHeaderColumns = _result;
            });

        this.browsePenjualanObatService.onFetchGridColumn("Farmasi/BrowsePenjualanObat/GridHistory/DetailHistory.json")
            .subscribe((_result) => {
                this.gridHistoryDetailColumns = _result;
            });
    }

    // ... On Initialize Grid ...
    onInitializedGridHistory(component: SharedGridComponent) {
        this.gridHistoryHeader = component;
        this.gridHistoryDetail = component;
    }

    // ... Untuk Get Datasource Grid History Header ...
    onFetchHistoryHeaderByBillDate(BillDate: any) {
        let date = moment(BillDate).format("YYYY-MM-DD");

        this.browsePenjualanObatService.onFetchHistoryHeaderByBillDate(date)
            .subscribe((_result) => {
                setTimeout(() => {
                    this.gridHistoryHeaderDatasource = _result;
                }, 2100);
            });
    }

    onSearchGridHeaderUsingKeyword(Pencarian: string) {
        this.gridHistoryDetail.grid.search(Pencarian);
    }

    // ... Function untuk mendapatkan data ketika memilih salah satu row di Grid History Header ...
    onSelectRowGridHistoryHeader(args: any) {
        this.gridHistoryHeaderSelectedData = args.data;
    }

    // ... Function untuk Get Datasource Grid History Detail ...
    onFetchHistoryDetailByPenjualanResepId(args: any) {
        let PenjualanResepId = args.PenjualanResepID;

        this.browsePenjualanObatService.onFetchHistoryDetailByPenjualanResepId(PenjualanResepId)
            .subscribe((_result) => {
                this.gridHistoryDetailDatasource = _result;
            })
    }

    // ... Function untuk mendapatkan data ketika memilih salah satu row di Grid History Detail ...
    onSelectRowGridHistoryDetail(args: any) {
        console.log(args);
    }

    // ... Function untuk mencetak report Rincian Biaya / Bukti Penjualan ...
    onCetakRincianBiaya() {
        let isGridHeaderEmpty: boolean = this.gridHistoryHeaderDatasource.length > 0 ? false : true;

        if (!isGridHeaderEmpty) {
            let parameterPrint = [{
                columnName: 'PenjualanResepID',
                filter: 'equal',
                searchText: this.gridHistoryHeaderSelectedData['PenjualanResepID'].toString(),
                searchText2: '',
            }];

            this.utilityService.onPrintReport("/TransaksiPenjualanObat/CetakBuktiPenjualan", parameterPrint);
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: "Bill Belum Dipilih",
            });
        }
    }

    onCetakTiketObat() {
        let isGridHeaderEmpty: boolean = this.gridHistoryHeaderDatasource.length > 0 ? false : true;

        if (!isGridHeaderEmpty) {
            let parameterPrint = [];

            let indexes = this.gridHistoryDetail.grid.getSelectedRowIndexes();

            if (indexes.length > 0) {
                for (let index in indexes) {
                    parameterPrint.push({
                        columnName: 'DetailPenjualanResepID',
                        filter: 'equal',
                        searchText: this.gridHistoryDetailDatasource[index]['DetailPenjualanResepID'].toString(),
                        searchText2: '',
                    });
                }

                this.utilityService.onPrintReport("/TransaksiPenjualanObat/CetakTiket", parameterPrint);
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: "Resep Tidak Boleh Kosong",
                });
            }
        }
    }
}
