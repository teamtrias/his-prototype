import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowsePenjualanObatComponent } from './browse-penjualan-obat.component';

describe('BrowsePenjualanObatComponent', () => {
  let component: BrowsePenjualanObatComponent;
  let fixture: ComponentFixture<BrowsePenjualanObatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrowsePenjualanObatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowsePenjualanObatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
