import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PenjualanObatComponent } from './penjualan-obat.component';

describe('PenjualanObatComponent', () => {
  let component: PenjualanObatComponent;
  let fixture: ComponentFixture<PenjualanObatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PenjualanObatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PenjualanObatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
