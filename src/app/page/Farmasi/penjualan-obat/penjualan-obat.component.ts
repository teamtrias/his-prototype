import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CommandColumnService } from '@syncfusion/ej2-angular-grids';
import { MenuItemModel } from '@syncfusion/ej2-angular-navigations';
import { SharedGridComponent } from 'src/app/components/shared-grid/shared-grid.component';
import { PenjualanObatService } from 'src/app/service/page/penjualan-obat.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-penjualan-obat',
    templateUrl: './penjualan-obat.component.html',
    styleUrls: ['./penjualan-obat.component.css'],
    providers: [CommandColumnService]
})
export class PenjualanObatComponent implements OnInit, AfterViewInit {

    formPenjualanObat: FormGroup;

    // ** Lookup Properties
    buttonLookup: string = "Cari";
    columnsLookup: any = [];
    filterLookup: any = [];
    urlLookup: string = "";

    // ** Tabs Obat Properties
    tabHeader: any = [
        {
            id: "diagnosaPasien",
            iconCss: "fas fa-table fa-sm",
            text: "Diagnosa"
        },
        {
            id: "pemeriksaanFisik",
            iconCss: "fas fa-user-md fa-sm",
            text: "Pemeriksaan Fisik"
        },
        {
            id: "alergiObat",
            iconCss: "fas fa-pills fa-sm",
            text: "Alergi Obat"
        },
        {
            id: "resepDigital",
            iconCss: "fas fa-file-medical fa-sm",
            text: "Resep Digital"
        },
    ];

    // ** Grid Diagnosa
    gridDiagnosa: SharedGridComponent = null;
    gridDiagnosaDatasource: any = [];
    gridDiagnosaColumns: any = [];

    // ** Context Menu Grid Resep Header
    contextMenuItemGridResepHeader: MenuItemModel[] = [
        {
            id: 'duplikat',
            text: 'Duplikat Resep',
            iconCss: 'fas fa-copy'
        }
    ];

    // ** Grid Resep Header
    gridResepHeader: SharedGridComponent = null;
    gridResepHeaderDatasource: any = [];
    gridResepHeaderColumns: any = [];
    gridResepHeaderSelectedData: any;

    // ** Grid Resep Header
    gridResepDetail: SharedGridComponent = null;
    gridResepDetailDatasource: any = [];
    gridResepDetailColumns: any = [];

    // ** Tab Detail -> Penjualan
    tabDetail: any = [
        {
            id: "inputPenjualanObat",
            iconCss: "fas fa-pills fa-sm",
            text: "Input Penjualan Obat"
        },
        {
            id: "browsePenjualanObat",
            iconCss: "fas fa-user-md fa-sm",
            text: "Browse Penjualan Obat"
        },
        {
            id: "daftarPending",
            iconCss: "fas fa-table fa-sm",
            text: "Daftar Pending"
        },
    ];

    // ** Grid Input Penjualan
    gridPenjualan: SharedGridComponent = null;
    gridPenjualanDatasource: any = [];
    gridPenjualanColumns: any = [];
    gridPenjualanEditSettings: any = { allowEditing: true, allowDeleting: true, allowEditOnDblClick: true, mode: 'Normal' };
    gridPenjualanSelectedRowIndex: any;

    // ! Command Button di Grid Penjualan
    public commandButtonObat: any;
    public commandButtonLabelObat: any;

    // ** Grid History Header
    gridHistoryHeader: SharedGridComponent = null;
    gridHistoryHeaderDatasource: any = [];
    gridHistoryHeaderColumns: any = [];

    // ** Grid History Detail
    gridHistoryDetail: SharedGridComponent = null;
    gridHistoryDetailDatasource: any = [];
    gridHistoryDetailColumns: any = [];

    // ** Static Filter Lookup Stok Gudang Lain
    staticFilterLookupStokGudangLain: any = [];

    constructor(
        private formBuilder: FormBuilder,
        private penjualanObatService: PenjualanObatService
    ) {
        // ** Panggil function set Form Penjualan Obat Attribute
        this.onSetFormPenjualanObatAttribute();
    }

    ngOnInit(): void {
        this.commandButtonObat = [
            {
                title: 'Obat',
                type: 'None',
                buttonOption: {
                    cssClass: 'e-flat',
                    iconCss: 'fas fa-pills',
                    id: 'CommandObat'
                }
            }
        ];

        this.commandButtonLabelObat = [
            {
                title: 'Label Obat',
                type: 'None',
                buttonOption: {
                    cssClass: 'e-flat',
                    iconCss: 'fas fa-file-medical',
                    id: 'CommandLabelObat'
                }
            }
        ];
    }

    ngAfterViewInit(): void {
        this.onSetGridProperties();
    }

    // ** Set Form Penjualan Obat Attribute ...
    onSetFormPenjualanObatAttribute() {
        this.formPenjualanObat = this.formBuilder.group({
            "PartyId": [0, []],
            "NoRegister": ["", []],
            "MrNo": ["", []],
            "TglMasukRawat": [null, []],
            "NamaPasien": ["", []],
            "Sex": ["", []],
            "TglLahir": [null, []],
            "KodeDokter": ["", []],
            "NamaDokter": ["", []],
            "PartyDebitur": [0, []],
            "KodeDebitur": ["", []],
            "NamaDebitur": ["", []],
            "ScvCode": ["", []],
            "ScvDescr": ["", []],
            "ScvPelayanan": ["", []],
            "NoSjp": ["", []],
            "Deposite": [0, []],
            "Balance": [0, []],
            "TotalCharge": [0, []],
            "PolyCode": ["", []],
            "PolyName": ["", []],
            "BuildingId": ["", []],
            "Alamat": ["", []],
            "GrandTotal": [0, []]
        })
    }

    // ** Function untuk GET GRID Properties ...
    onSetGridProperties() {
        // ! Grid Diagnosa Column
        this.penjualanObatService.onFetchGridColumn("Farmasi/PenjualanObat/GridHeader/Diagnosa.json")
            .subscribe((_result) => {
                this.gridDiagnosaColumns = _result;
            });

        // ! Grid Resep Header Column
        this.penjualanObatService.onFetchGridColumn("Farmasi/PenjualanObat/GridHeader/ResepHeader.json")
            .subscribe((_result) => {
                this.gridResepHeaderColumns = _result;
            });

        // ! Grid Resep Detail Column
        this.penjualanObatService.onFetchGridColumn("Farmasi/PenjualanObat/GridHeader/ResepDetail.json")
            .subscribe((_result) => {
                this.gridResepDetailColumns = _result;
            });

        // ! Grid Input Penjualan Column
        setTimeout(() => {
            this.onSetColumnGridInputPenjualan();
        }, 500);

        // ! Grid History Header Column
        this.penjualanObatService.onFetchGridColumn("Farmasi/PenjualanObat/GridHeader/HistoryHeader.json")
            .subscribe((_result) => {
                this.gridHistoryHeaderColumns = _result;
            });

        // ! Grid History Detail Column
        this.penjualanObatService.onFetchGridColumn("Farmasi/PenjualanObat/GridHeader/HistoryDetail.json")
            .subscribe((_result) => {
                this.gridHistoryDetailColumns = _result;
            });
    }

    // ** Initialize Shared Grid Component
    onInitializedGridDiagnosa(component: SharedGridComponent) {
        this.gridDiagnosa = component;
    }

    // ** Initialize Shared Grid Component
    onInitializedGridResepHeader(component: SharedGridComponent) {
        this.gridResepHeader = component;
    }

    // ** Initialize Shared Grid Component
    onInitializedGridResepDetail(component: SharedGridComponent) {
        this.gridResepDetail = component;
    }

    // ** Initialize Shared Grid Component
    onInitializedGridPenjualan(component: SharedGridComponent) {
        this.gridPenjualan = component;
    }

    // ** Initialize Shared Grid Component
    onInitializedGridHistoryHeader(component: SharedGridComponent) {
        this.gridHistoryHeader = component;
    }

    // ** Initialize Shared Grid Component
    onInitializedGridHistoryDetail(component: SharedGridComponent) {
        this.gridHistoryDetail = component;
    }

    // ** Function untuk set Attribute Lookup (Column, Filter dan Url) ...
    onOpenLookup(id: string) {
        this.columnsLookup = [];
        this.filterLookup = [];
        this.urlLookup = "";

        if (id == "modalAdmisiPasien") {
            this.urlLookup = "/TransaksiRawatJalan/GetDataAdmisiByParams";
            this.onSetAttributeLookup("modalAdmisiPasien");
        } else if (id == "modalObat") {
            this.urlLookup = "/TransaksiPenjualanObat/GetAllObatByParams";
            this.onSetAttributeLookup("modalObat");
        } else if (id == "modalLabelObat") {
            this.urlLookup = "/TransaksiPenjualanObat/GetAllLabelObatByParams";
            this.onSetAttributeLookup("modalLabelObat");
        } else {
            this.urlLookup = "/TransaksiPenjualanObat/GetAllStokObatGudangLain";
            this.onSetAttributeLookup("modalStokGudangLain");
        }
    }

    // ** Function untuk set Lookup Admisi Pasien Attribute
    onSetAttributeLookup(id: string) {
        let urlColumns: string = "";
        let urlFilters: string = "";

        if (id == "modalAdmisiPasien") {
            urlColumns = "Farmasi/PenjualanObat/Lookup/LookupAdmisi/columns.json";
            urlFilters = "Farmasi/PenjualanObat/Lookup/LookupAdmisi/filters.json";
        } else if (id == "modalObat") {
            urlColumns = "Farmasi/PenjualanObat/GridHeader/Penjualan/columns.json";
            urlFilters = "Farmasi/PenjualanObat/GridHeader/Penjualan/filters.json";
        } else if (id == "modalLabelObat") {
            urlColumns = "Farmasi/PenjualanObat/Lookup/LookupLabelObat/columns.json";
            urlFilters = "Farmasi/PenjualanObat/Lookup/LookupLabelObat/filters.json";
        } else {
            urlColumns = "Farmasi/PenjualanObat/Lookup/LookupStokObatGudangLain/columns.json";
            urlFilters = "Farmasi/PenjualanObat/Lookup/LookupStokObatGudangLain/filters.json";
        }

        this.penjualanObatService.onGetLookupColumns(urlColumns)
            .subscribe((_result) => {
                this.columnsLookup = _result;
            });

        this.penjualanObatService.onGetLookupFilters(urlFilters)
            .subscribe((_result) => {
                this.filterLookup = _result;
            });
    }

    // ** Function untuk GET SELECTED DATA FROM LOOKUP ADMISI ...
    onGetSelectedDataLookupAdmisi(args: any) {
        args.GrandTotal = 0;

        this.formPenjualanObat.setValue(args);

        // ** Panggil function untuk Get Diagnosa ...
        this.onGetDiagnosaPasienByNoRegister(args.NoRegister);

        // ** Panggil function untuk Get Resep Header ... '
        this.onGetResepHeaderByNoRegisterAndNoMr(args.MrNo, args.NoRegister);

        // ** Panggil function untuk Add New Row ke Grid Input Penjualan ...
        this.onAddNewRowGridPenjualan();

        // ** Panggil function untuk Get History Header ...
        this.onGetHistoryHeaderByNoRegister(args.NoRegister);
    }

    // ** Function untuk GET DIAGNOSA BY NO REGISTER ...
    onGetDiagnosaPasienByNoRegister(noRegister: any) {
        return this.penjualanObatService.onGetDiagnosaPasienByNoRegister(noRegister)
            .subscribe((_result) => {
                this.gridDiagnosaDatasource = _result;
            });
    }

    // ** Function untuk GET RESEP HEADER BY NO REGISTER and NO MR ...
    onGetResepHeaderByNoRegisterAndNoMr(MrNo: any, NoRegister: any) {
        return this.penjualanObatService.onGetResepHeaderByNoRegisterAndNoMr(MrNo, NoRegister)
            .subscribe((_result) => {
                this.gridResepHeaderDatasource = _result;
            })
    }

    // ** Function untuk mentrigger function lain ketika Context Menu di Grid Resep Header di click ...
    onSelectContextMenuGridResepHeader(args: any) {
        let id = args.item.id;

        if (id == "duplikat") {
            if (this.gridResepHeaderSelectedData != null) {

                this.penjualanObatService.onGetResepDetailByResepId(this.gridResepHeaderSelectedData.ResepId)
                    .subscribe((_result) => {
                        console.log(_result);

                        if (_result.length > 0) {
                            for (let index in _result) {

                                let data = {
                                    "NamaObat": _result[index]["NamaObat"],
                                    "LabelDesc": _result[index]["LabelDesc"],
                                    "Qty": _result[index]["Qty"],
                                    "ResepId": _result[index]["ResepId"],
                                    "DetailResepId": _result[index]["DetailResepId"]
                                };

                                this.onAddRowGridPenjualanByDuplicatingResepHeader(data);
                            }

                            this.gridPenjualan.grid.refresh();
                        }
                    });
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Resep Belum Dipilih',
                });
            }
        }
    }

    // ** Function untuk menambahkan Row di Grid Penjualan by Resep Header -> Duplikat ...
    onAddRowGridPenjualanByDuplicatingResepHeader(data: any) {
        this.gridPenjualan.gridDataSource.push({
            "ItemCode": "",
            "ItemCodeDesc": "",
            "DrugName": data.NamaObat,
            "AturanPakai": data.LabelDesc,
            "SatuanId": "",
            "OnHandPerGudang": 0,
            "Qty": data.Qty,
            "Harga": 0,
            "Total": 0,
            "LabelId": "FREE",
            "LabelDesc": "",
            "LabelKet": "",
            "SetupObatDetailId": 0,
            "HargaBeli": 0,
            "ResepId": data.ResepId,
            "DetailResepId": data.DetailResepId
        });
    }

    // ** Function untuk GET RESEP DETAIL BY RESEP ID ...
    onGetResepDetailByResepId(args: any) {
        let data = args.data;
        this.gridResepHeaderSelectedData = data;

        return this.penjualanObatService.onGetResepDetailByResepId(data.ResepId)
            .subscribe((_result) => {
                this.gridResepDetailDatasource = _result;
            })
    }

    // ** Function untuk Set Column pada Grid Input Penjualan 
    onSetColumnGridInputPenjualan() {
        this.gridPenjualanColumns = [
            {
                "width": 150,
                "allowSorting": true,
                "allowEditing": false,
                "type": "number",
                "format": "N2",
                "editType": "numericEdit",
                "field": "SetupObatDetailId",
                "headerText": "Obat Detail Id",
                "visible": false
            },

            {
                "width": 150,
                "allowSorting": true,
                "allowEditing": false,
                "type": "number",
                "format": "N2",
                "editType": "numericEdit",
                "field": "ResepId",
                "headerText": "Resep Id",
                "visible": false
            },

            {
                "width": 150,
                "allowSorting": true,
                "allowEditing": false,
                "type": "number",
                "format": "N2",
                "editType": "numericEdit",
                "field": "DetailResepId",
                "headerText": "Detail Resep Id",
                "visible": false
            },

            {
                "width": 150,
                "allowSorting": true,
                "allowEditing": false,
                "type": "string",
                "format": null,
                "editType": "defaultEdit",
                "field": "DrugName",
                "headerText": "Nama Resep",
                "visible": true
            },

            {
                "width": 150,
                "allowSorting": true,
                "allowEditing": false,
                "type": "string",
                "format": null,
                "editType": "defaultEdit",
                "field": "AturanPakai",
                "headerText": "Aturan Pakai",
                "visible": true
            },

            {
                "width": 100,
                "allowSorting": true,
                "allowEditing": false,
                "type": "string",
                "format": null,
                "editType": "defaultEdit",
                "field": "ItemCode",
                "headerText": "Kode Obat",
                "visible": true
            },
            {
                "width": 90,
                "headerText": "Pilih Obat",
                "textAlign": "center",
                "commands": this.commandButtonObat
            },

            {
                "width": 120,
                "allowSorting": true,
                "allowEditing": false,
                "type": "string",
                "format": null,
                "editType": "defaultEdit",
                "field": "ItemCodeDesc",
                "headerText": "Nama Obat",
                "visible": true
            },
            {
                "width": 90,
                "allowSorting": true,
                "allowEditing": false,
                "type": "string",
                "format": null,
                "editType": "defaultEdit",
                "field": "SatuanId",
                "headerText": "Satuan",
                "visible": true
            },

            {
                "width": 150,
                "allowSorting": true,
                "allowEditing": false,
                "type": "number",
                "format": "N2",
                "editType": "numericEdit",
                "field": "OnHandPerGudang",
                "headerText": "Sisa Stok",
                "visible": false
            },
            {
                "width": 120,
                "allowSorting": true,
                "allowEditing": false,
                "type": "number",
                "format": "N2",
                "editType": "numericEdit",
                "field": "Harga",
                "headerText": "Harga Satuan",
                "visible": true,
                "textAlign": "Right"
            },

            {
                "width": 150,
                "allowSorting": true,
                "allowEditing": false,
                "type": "number",
                "format": "N2",
                "editType": "numericEdit",
                "field": "HargaBeli",
                "headerText": "Hpp",
                "visible": false
            },
            {
                "width": 90,
                "allowSorting": true,
                "allowEditing": true,
                "type": "number",
                "format": "N",
                "editType": "numericEdit",
                "field": "Qty",
                "headerText": "Qty Jual",
                "visible": true,
                "textAlign": "Right"
            },

            {
                "width": 120,
                "allowSorting": true,
                "allowEditing": false,
                "type": "number",
                "format": "N2",
                "editType": "numericEdit",
                "field": "Total",
                "headerText": "Harga Total",
                "visible": true,
                "textAlign": "Right"
            },

            {
                "width": 250,
                "allowSorting": true,
                "allowEditing": false,
                "type": "string",
                "format": null,
                "editType": "defaultEdit",
                "field": "LabelId",
                "headerText": "Label ID",
                "visible": false
            },
            {
                "width": 250,
                "allowSorting": true,
                "allowEditing": true,
                "type": "string",
                "format": null,
                "editType": "defaultEdit",
                "field": "LabelDesc",
                "headerText": "Label Obat",
                "visible": true
            },

            {
                "width": 120,
                "headerText": "Pilih Label Obat",
                "textAlign": "center",
                "commands": this.commandButtonLabelObat
            },
            {
                "width": 150,
                "allowSorting": true,
                "allowEditing": true,
                "type": "string",
                "format": null,
                "editType": "defaultEdit",
                "field": "LabelKet",
                "headerText": "Keterangan",
                "visible": true
            }
        ]
    }

    // ** Function untuk Add New Row di Grid Input Penjualan
    onAddNewRowGridPenjualan() {
        this.gridPenjualan.gridDataSource.push({
            "ItemCode": "",
            "ItemCodeDesc": "",
            "DrugName": "",
            "AturanPakai": "",
            "SatuanId": "",
            "OnHandPerGudang": 0,
            "Qty": 0,
            "Harga": 0,
            "Total": 0,
            "LabelId": "FREE",
            "LabelDesc": "",
            "LabelKet": "",
            "SetupObatDetailId": 0,
            "HargaBeli": 0
        });

        this.gridPenjualan.grid.refresh();
    }

    // ** Function untuk Get Data pada Row yang Dipilih
    onSelectRowGridPenjualan(args: any) {
        this.gridPenjualanSelectedRowIndex = args.rowIndex;
    }

    onActionCompleteGridPenjualan(args: any) {
        let action = args.action;
        let requestType = args.requestType;
        let data = args.data;
        let index = args.rowIndex;

        if (action == "edit" && requestType == "save") {
            for (let item in data) {
                this.gridPenjualan.gridDataSource[index][item] = data[item];
                this.gridPenjualan.gridDataSource[index]["Total"] = data["Qty"] * data["Harga"];
            };

            this.gridPenjualan.grid.refresh();
        }
    }

    // ** Function untuk Mengeksekusi function lainnya apabila Command Button di Grid Penjualan di click
    onCommandClickGridPenjualan(args: any) {
        let commandId = args.target.ej2_instances[0].id;

        if (commandId == "CommandObat") {
            document.getElementById("LookupObat").click();
        }
        else if (commandId == "CommandLabelObat") {
            document.getElementById("LookupLabelObat").click();
        }
        else {
            // .. do nothing!
        }
    }

    // ** Function untuk Menginputkan Value kedalam Input Field berdasarkan Selected Data dari Lookup
    onGetSelectedDataLookupObat(args: any) {
        // (<HTMLInputElement>document.getElementById("ItemCodeDesc")).value = args.ItemCodeDesc;

        // .. Deklarasikan sebuah variable untuk menampung Grid Penjualan Datasource per Index;
        let gridPenjualanDataPerIndex = this.gridPenjualan.gridDataSource[this.gridPenjualanSelectedRowIndex];

        // .. Panggil function untuk me-match-kan Grid Penjualan Datasource per Index dengan args
        this.penjualanObatService.onMapBetweenGridDatasourceAndSomeData(gridPenjualanDataPerIndex, args);

        // .. Refresh Grid Penjualan
        this.gridPenjualan.grid.refresh();

        // .. Panggil function untuk mendapatkan Harga Obat Terakhir By Item Code
        this.penjualanObatService.onGetHargaObatTerakhirByItemCode(args.ItemCode)
            .subscribe((_result) => {

                gridPenjualanDataPerIndex["SetupObatDetailId"] = _result["Id"];
                gridPenjualanDataPerIndex["Harga"] = _result["SellingPrice"];

                let total = _result["SellingPrice"] * gridPenjualanDataPerIndex["Qty"];
                gridPenjualanDataPerIndex["Total"] = total;

                // .. Refresh Grid Penjualan
                this.gridPenjualan.grid.refresh();

            });

        // .. Set Static Filter untuk Lookup Stok Gudang Lain
        this.staticFilterLookupStokGudangLain = [{
            columnName: 'sig.ItemCode',
            filter: 'like',
            searchText: args.ItemCode,
            searchText2: ''
        }];

    }

    // ** Function untuk Menginputkan Value kedalam Input Field berdasarkan Selected Data dari Lookup
    onGetSelectedDataLookupLabelObat(args: any) {
        // (<HTMLInputElement>document.getElementById("LabelDesc")).value = args.LabelDesc;

        // .. Deklarasikan sebuah variable untuk menampung Grid Penjualan Datasource per Index;
        let gridPenjualanDataPerIndex = this.gridPenjualan.gridDataSource[this.gridPenjualanSelectedRowIndex];

        // .. Panggil function untuk me-match-kan Grid Penjualan Datasource per Index dengan args
        this.penjualanObatService.onMapBetweenGridDatasourceAndSomeData(gridPenjualanDataPerIndex, args);

        // .. Refresh Grid Penjualan
        this.gridPenjualan.grid.refresh();
    }

    // ** Function untuk Get History Header ...
    onGetHistoryHeaderByNoRegister(NoRegister: any) {
        this.penjualanObatService.onGetHistoryHeaderByNoRegister(NoRegister)
            .subscribe((_result) => {
                this.gridHistoryHeaderDatasource = _result;
            });
    }

    // ** Function untuk Get History Detail ...
    onGetHistoryDetailByPenjualanResepId(args: any) {
        let data = args.data;

        this.penjualanObatService.onGetHistoryDetailByPenjualanResepId(data.PenjualanResepID)
            .subscribe((_result) => {
                this.gridHistoryDetailDatasource = _result;
            });
    }

    onGetSelectedDataLookupStokGudangLain(args: any) {
        (<HTMLInputElement>document.getElementById("StokGudangLain")).value = args.Gudang;
    }

    onHitungSubtotalGridPenjualan() {
        let grid = this.gridPenjualanDatasource;
        let subtotal = 0;

        if (grid.length > 0) {
            for (let item in grid) {
                subtotal += grid[item]['Total'];
            }
        }

        this.GrandTotal.setValue(subtotal);

        this.onSubmitFormInputPenjualanObat(this.formPenjualanObat.value);
    }

    onSubmitFormInputPenjualanObat(formPenjualanObat: any) {

        // ... Deklarasikan variable => object
        let DataInputPenjualan = {};

        // ... Deklarasikan variable untuk memfilter Stok Barang
        let isStokKosong: boolean = false;
        let idObatKosong: number = 0;

        // ... Isikan variable DataInputPenjualan
        DataInputPenjualan["OutletId"] = "4080029";
        DataInputPenjualan["JenisPasien"] = "OUT";
        DataInputPenjualan["StatusResep"] = "DILAYANI";
        DataInputPenjualan["MrNo"] = formPenjualanObat.MrNo;
        DataInputPenjualan["NoRegister"] = formPenjualanObat.NoRegister;
        DataInputPenjualan["KodeDokterResep"] = formPenjualanObat.KodeDokter;
        DataInputPenjualan["PolyCode"] = formPenjualanObat.PolyCode;
        DataInputPenjualan["GrandTotal"] = formPenjualanObat.GrandTotal;

        DataInputPenjualan["Slip"] = {
            "MrNo": formPenjualanObat.MrNo,
            "TglMasukRawat": formPenjualanObat.TglMasukRawat,
            "NoRegister": formPenjualanObat.NoRegister,
            "Building": formPenjualanObat.BuildingId.toString(),
            "TransType": "RESEP",
        };

        // ... Lakukan Looping untuk me-match-kan antar Grid Input Penjualan Datasource
        let detail = this.gridPenjualan.gridDataSource;

        for (let index in detail) {
            detail[index]["QtyInClaim"] = 0;
            detail[index]["QtyOutClaim"] = detail[index]["Qty"];
            detail[index]["NoUrut"] = parseInt(index) + 1;
            detail[index]["HargaJualSatuan"] = detail[index]["Harga"];
            detail[index]["HargaTotal"] = detail[index]["Total"];
            detail[index]["HargaBeliSatuan"] = detail[index]["Hpp"];
            detail[index]["KodeGudang"] = "4080029";

            if (detail[index]["OnHandPerGudang"] == 0) {
                isStokKosong = true;
                idObatKosong = detail[index]["ItemCode"];
                break;
            };

            if (detail[index]["Qty"] > detail[index]["OnHandPerGudang"]) {
                isStokKosong = true;
                idObatKosong = detail[index]["ItemCode"];
                break;
            }
        }

        DataInputPenjualan["Details"] = detail;

        // .. Pasang filter, jika barang yg diinputkan ke Grid Input Penjualan Stok nya Kosong atau Kurang
        let filter: [] = detail.filter((filterized: any) => {
            return filterized["ItemCode"] != "" && filterized["Qty"] != 0;
        });

        if (filter.length > 0) {
            if (!isStokKosong) {
                // console.log("Data Input Penjualan :", DataInputPenjualan);
                console.log("Eksekusi Proses Simpan!");

                this.penjualanObatService.onSubmitForm(DataInputPenjualan)
                    .subscribe((_result) => {
                        Swal.fire({
                            icon: 'success',
                            title: 'Success',
                            text: 'Data Berhasil Disimpan!',
                        }).then(() => {
                            location.reload();
                        })
                    }, (error) => {
                        console.warn(error);
                    });
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Stok Barang ' + idObatKosong + ' Kosong!',
                });
            }
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Detail Tidak Boleh Kosong!',
            });
        }

    }

    get NoRegister() { return this.formPenjualanObat.get("NoRegister") }

    get MrNo() { return this.formPenjualanObat.get("MrNo") }

    get NamaPasien() { return this.formPenjualanObat.get("NamaPasien") }

    get TglLahir() { return this.formPenjualanObat.get("TglLahir") }

    get Sex() { return this.formPenjualanObat.get("Sex") }

    get Alamat() { return this.formPenjualanObat.get("Alamat") }

    get KodeDebitur() { return this.formPenjualanObat.get("KodeDebitur") }

    get NamaDebitur() { return this.formPenjualanObat.get("NamaDebitur") }

    get TglMasukRawat() { return this.formPenjualanObat.get("TglMasukRawat") }

    get PolyCode() { return this.formPenjualanObat.get("PolyCode") }

    get PolyName() { return this.formPenjualanObat.get("PolyName") }

    get KodeDokter() { return this.formPenjualanObat.get("KodeDokter") }

    get NamaDokter() { return this.formPenjualanObat.get("NamaDokter") }

    get GrandTotal() { return this.formPenjualanObat.get("GrandTotal") }
}
