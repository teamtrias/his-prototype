import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SidebarComponent } from '@syncfusion/ej2-angular-navigations';
import { AuthService } from 'src/app/service/authentication/auth.service';
import { StorageEncryptService } from 'src/app/service/authentication/storage-encrypt.service';
import { MenuService } from 'src/app/service/page/menu.service';
import { PushNotificationService } from 'src/app/service/page/push-notification.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css', './dock-style.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {

    isLoading = false;

    title: string = "SIADMIN";
    menuNavbar: {};
    menuSidebarActive: any[];
    arraySidebarMenu: any[];
    childMenuName: string;
    childMenuIcon: string;
    displayName: string;

    @ViewChild('dockBar')
    public dockBar: SidebarComponent;
    public enableDock: boolean = true;
    public width: string = '250px';
    public dockSize: string = '72px';

    constructor(
        private router: Router,
        private menuService: MenuService,
        private authService: AuthService,
        private pushNotifService: PushNotificationService,
        private storageEncryptService: StorageEncryptService
    ) { }

    ngOnInit(): void {
        this.onFetchMenuData();
        this.onSetUserDisplayName();
        this.title = localStorage.getItem('pageTitle');
    }

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.dockBar.hide();
        }, 500);
    }

    onClickMenuTanpaChild(menu: any) {
        console.log(menu);

        if (menu.text == "Billing") {
            this.router.navigateByUrl("dashboard/billing");
        } else {

        }
    }

    toggleClick() {
        this.dockBar.toggle();
    }

    onFetchMenuData() {
        let menuData = JSON.parse(this.storageEncryptService.getItem('currentUser'));

        this.menuNavbar = menuData.MenuBars;
    }

    onSetUserDisplayName() {
        let currentUser = JSON.parse(this.storageEncryptService.getItem('currentUser'));
        let currentUserName = currentUser.NamaKaryawan;

        this.displayName = currentUserName;
    }

    onOpenSidebar($event: any, grandChild: []) {
        setTimeout(() => {
            this.dockBar.show();
            // this.isOverlayActive = true;
        }, 300);

        // ... Setting Sidebar Menu Text
        this.childMenuName = grandChild['text'];
        this.childMenuIcon = grandChild['iconClass'];

        // ... Get Parameter Untuk Request Sidebar
        let currentUser = JSON.parse(this.storageEncryptService.getItem('currentUser'));
        let currentUserRoleId = currentUser.RoleId;
        let currentMenuId = grandChild['MenuId'];

        this.onFetchSidebarMenu(currentUserRoleId, currentMenuId);
    }

    onOpenSidebarNoChild($event: any, child: []) {
        setTimeout(() => {
            this.dockBar.show();
            // this.isOverlayActive = true;
        }, 300);

        // ... Setting Sidebar Menu Text
        this.childMenuName = child['text'];
        this.childMenuIcon = child['iconClass'];

        // ... Get Parameter Untuk Request Sidebar
        let currentUser = JSON.parse(this.storageEncryptService.getItem('currentUser'));
        let currentUserRoleId = currentUser.RoleId;
        let currentMenuId = child['MenuId'];

        this.onFetchSidebarMenu(currentUserRoleId, currentMenuId);
    }

    onFetchSidebarMenu(roleId: number, menuId: number) {
        this.menuService.onFetchSidebarMenu(roleId, menuId)
            .subscribe((_sidebar) => {
                let menuSidebar = _sidebar;

                let _sidebarActive = [];
                _sidebarActive.push(menuSidebar);

                this.arraySidebarMenu = _sidebarActive;
                this.menuSidebarActive = _sidebarActive;
            });
    }

    openingPage(args: any, sidebarMenu: any) {
        this.dockBar.hide();

        let url = sidebarMenu.url;

        this.router.navigateByUrl(url);

        setTimeout(() => {
            this.title = localStorage.getItem('pageTitle');
        }, 200);
    }

    onLogout() {
        this.authService.logout();
    }
}
