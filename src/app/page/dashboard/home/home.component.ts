import { AfterViewInit, Component, OnInit } from '@angular/core';
import { SharedGridComponent } from 'src/app/components/shared-grid/shared-grid.component';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {

    gridHistoryHeader: SharedGridComponent = null;
    gridHistoryHeaderDatasource: any = [];
    gridHistoryHeaderColumns: any = [];

    constructor() { }

    ngOnInit(): void {

    };

    ngAfterViewInit(): void {
        setTimeout(() => {
            this.gridHistoryHeaderColumns = [
                { headerText: "RUANG", width: 100, field: "Ruang", visible: false },
                { headerText: "NO BED", width: 100, field: "NoBed", visible: true },
                { headerText: "NO RM", width: 100, field: "NoRm", visible: true },
                { headerText: "REGISTER", width: 100, field: "Register", visible: true },
                { headerText: "NAMA PASIEN", width: 100, field: "NamaPasien", visible: true },
                { headerText: "DOKTER DPJP", width: 100, field: "Dpjp", visible: true },
            ];

            this.gridHistoryHeaderDatasource = [
                { Ruang: "Merak LT 2", NoBed: "D21.003", NoRm: "RM05506", Register: "A12.2016.05506", NamaPasien: "FATUR GAUTAMA S", Dpjp: "LESTARINGSIH, dr." },
                { Ruang: "Merak LT 2", NoBed: "D21.004", NoRm: "RM05507", Register: "A12.2016.05507", NamaPasien: "MICHELE THEA", Dpjp: "LESTARINGSIH, dr." },
                { Ruang: "Merak LT 2", NoBed: "D21.005", NoRm: "RM05508", Register: "A12.2016.05508", NamaPasien: "MEME AMELIA", Dpjp: "LESTARINGSIH, dr." },
            ];
        }, 500);
    }

    // ** Initialize Shared Grid Component
    onInitializedGridHistoryHeader(component: SharedGridComponent) {
        this.gridHistoryHeader = component;
    }
}
