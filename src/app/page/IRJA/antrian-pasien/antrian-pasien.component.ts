import { AfterViewInit, Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItemModel } from '@syncfusion/ej2-angular-navigations';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { SharedGridComponent } from 'src/app/components/shared-grid/shared-grid.component';
import { StorageEncryptService } from 'src/app/service/authentication/storage-encrypt.service';
import { AntrianPasienService } from 'src/app/service/page/antrian-pasien.service';
import { UtilityService } from 'src/app/service/utility/utility.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-antrian-pasien',
    templateUrl: './antrian-pasien.component.html',
    styleUrls: ['./antrian-pasien.component.css']
})
export class AntrianPasienComponent implements OnInit, AfterViewInit {

    // ... Modal Lookup Variable
    modalButtonLookup: string = "Cari";
    modalColumnsItemLookup: any = [];
    modalFiltersItemLookup: any = [];
    modalStaticFilterLookup: any = [];
    modalUrlLookup: string = "";

    // ... Grid Header Variable
    @ViewChild("gridAntrianPasien") public gridAntrianPasien: SharedGridComponent;

    gridId: string = "GridAntrianPasien";
    gridDataSource: Object[];
    gridLines: string = "Both";
    gridHeight: string = "350";
    gridColumns: any;
    isAllowPaging: boolean = true;
    gridPageSettings: any = [];
    gridToolbar: any = [];
    gridEditSettings: any = [];
    gridContextMenuItems: MenuItemModel[] = [
        {
            id: 'laboratorium',
            text: 'Order Laboratorium',
            iconCss: 'fas fa-clipboard-list'
        }, {
            id: 'radiologi',
            text: 'Order Radiologi',
            iconCss: 'fas fa-clipboard-list'
        }, {
            id: 'resep-digital',
            text: 'Order Resep',
            iconCss: 'fas fa-clipboard-list'
        }
    ];
    gridSelectedData: any;

    // ... Modal Konsul Variable
    modalRef: BsModalRef;
    modalId: string = "ModalKonsul";
    modalTitle: string = "Konsul Antar Poli";
    @ViewChild("template") template: TemplateRef<any>;

    // ... Grid Modal Konsul
    // @ViewChild("gridKonsul") public gridKonsul: SharedGridComponent;
    private gridKonsul: SharedGridComponent = null;

    gridKonsulId: string = "GridKonsul";
    gridKonsulDataSource: Object[];
    gridKonsulLines: string = "Both";
    gridKonsulHeight: string = "300";
    gridKonsulColumns: any;
    isAllowPagingKonsul: boolean = true;
    gridKonsulPageSettings: any = [];
    gridKonsulToolbar: any = [];
    gridKonsulEditSettings: any = [];
    gridKonsulSelectedData: any;
    gridKonsulSelectedRowIndex: any;

    array: any = [];

    // ... Input Form Variable
    @ViewChild("PolyCode") PolyCode: ElementRef;
    @ViewChild("PolyName") PolyName: ElementRef;

    constructor(private encryptStorageService: StorageEncryptService,
        private antrianPasienService: AntrianPasienService,
        private utilityService: UtilityService,
        private modalService: BsModalService,
        private router: Router) { }

    ngOnInit(): void {

    }

    ngAfterViewInit() {
        this.onSetGridAttribute();
    }

    onOpenLookup(id: string) {
        this.modalColumnsItemLookup = [];
        this.modalFiltersItemLookup = [];
        this.modalUrlLookup = "";

        if (id == "modalPoli") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetPoliByParams";
            this.onSetAttributeLookup("modalPoli");
        }
        else if (id == "modalPoliKonsul") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetPoliByParams";
            this.onSetAttributeLookup("modalPoli");
        }
        else if (id == "modalDokterKonsul") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetDokterByParams";
            this.onSetAttributeLookup("modalDokterKonsul");
        }
        else {
            //... do nothing!
        }
    }

    // ... Untuk set lookup attribute
    onSetAttributeLookup(id: string) {
        let urlColumns: string = "";
        let urlFilters: string = "";

        if (id == "modalPoli") {
            urlColumns = "AdmisiPasien/Lookup/LookupPoli/columns.json";
            urlFilters = "AdmisiPasien/Lookup/LookupPoli/filter.json";
        } else if (id == "modalPoliKonsul") {
            urlColumns = "AdmisiPasien/Lookup/LookupPoli/columns.json";
            urlFilters = "AdmisiPasien/Lookup/LookupPoli/filter.json";
        } else if (id == "modalDokterKonsul") {
            urlColumns = "AdmisiPasien/Lookup/LookupDokter/columns.json";
            urlFilters = "AdmisiPasien/Lookup/LookupDokter/filter.json";
        } else {
            urlColumns = "";
            urlFilters = "";
        }

        this.antrianPasienService.onGetLookupColumns(urlColumns)
            .subscribe((_result) => {
                this.modalColumnsItemLookup = _result;
            });

        this.antrianPasienService.onGetLookupFilters(urlFilters)
            .subscribe((_result) => {
                this.modalFiltersItemLookup = _result;
            });
    }

    // ... Untuk fill input form Poli
    onGetSelectedDataPoli(args: any) {
        this.PolyCode.nativeElement.value = args.PolyCode;
        this.PolyName.nativeElement.value = args.PolyName;

        this.onGetAllDataAntrian(args.PolyCode);
    }

    // ... Untuk get data antrian dan memasukkannya kedalam grid
    onGetAllDataAntrian(polyCode: string) {
        let search = [{
            "columnName": "irj.PolyCode",
            "filter": "like",
            "searchText": polyCode,
            "searchText2": ""
        }];

        this.antrianPasienService.onGetAllAntrianIRJA(search)
            .subscribe(
                (_result) => {
                    setTimeout(() => {
                        this.gridDataSource = _result;
                    }, 2100);
                });
    }

    // ... Untuk Set Attribute Grid
    onSetGridAttribute() {
        return this.antrianPasienService.onGetGridColumns()
            .subscribe((_result) => {
                this.gridColumns = _result.columns;

                this.gridToolbar = [
                    { text: "Periksa", tooltipText: "Periksa", prefixIcon: "fas fa-check", id: "periksa" },
                    { text: "Konsultasi", tooltipText: "Konsultasi Antar Poli", prefixIcon: "fas fa-tasks", id: "konsultasi" },
                    { text: "Refresh", tooltipText: "Refresh", prefixIcon: "fas fa-sync", id: "refresh" },
                    "Search"
                ];
            });
    }

    // ... Action ketika row di grid header di pilih
    onRowSelecting(args: any) {
        this.gridSelectedData = args.data;
    }

    // ... Action ketika toolbar grid di click
    onToolbarClick(args: any, grid: any) {
        let action = args.item.id;

        if (action == "refresh") {
            this.onToolbarClickActionRefresh();
        }
        else if (action == "periksa") {
            if (this.gridSelectedData)
                this.onToolbarClickActionPeriksa(this.gridSelectedData);
            else
                Swal.fire({
                    title: 'Perhatian',
                    icon: 'error',
                    html: 'Data Pasien Belum Dipilih'
                });
        }
        else if (action == "konsultasi") {
            if (this.gridSelectedData)
                this.onToolbarClickActionKonsul(this.gridSelectedData);
            else
                Swal.fire({
                    title: 'Perhatian',
                    icon: 'error',
                    html: 'Data Pasien Belum Dipilih'
                });
        }
        else {
            // ... do nothing!
        }
    }

    // ... Action ketika toolbar grid => REFRESH di click
    onToolbarClickActionRefresh(): any {
        let polyCode = this.PolyCode.nativeElement.value;

        if (polyCode)
            this.onGetAllDataAntrian(polyCode);
        else
            Swal.fire({
                title: 'Perhatian',
                icon: 'error',
                html: 'Poli Belum Dipilih'
            });
    }

    // ... Action ketika toolbar grid => PERIKSA di click
    onToolbarClickActionPeriksa(dataPasien: any) {
        if (dataPasien["KodeDokter"]) {
            if (dataPasien["TglPeriksa"] == "0001-01-01T00:00:00" || dataPasien["TglPeriksa"] == null) {

                // ... Deklarasikan Pesan Untuk Swal Confirmation
                let pesan = "Pemeriksaan Untuk No. Register : " + dataPasien["NoRegister"];

                // ... Dan ini action ketika Swal Confirmation = OK!
                this.utilityService.onShowConfirmationDialog(pesan,
                    this.onActionPeriksaConfirmation.bind(this)
                );
            }
            else {
                Swal.fire({
                    title: 'Perhatian',
                    icon: 'warning',
                    html: 'Pasien Sudah Pernah Diperiksa'
                });
            }
        }
        else {
            Swal.fire({
                title: 'Perhatian',
                icon: 'warning',
                html: 'Dokter Belum Ditentukan'
            });
        }
    }

    onActionPeriksaConfirmation() {
        this.antrianPasienService.onPostActionPeriksaPasien(this.gridSelectedData)
            .subscribe((_result) => {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    html: 'Data Berhasil Disimpan'
                }).then(() => {
                    this.onGetAllDataAntrian(this.PolyCode.nativeElement.value);
                })
            })
    }

    // ... Action ketika toolbar grid => KONSUL di click
    onToolbarClickActionKonsul(dataPasien: any) {
        console.log(dataPasien);

        if (dataPasien["TglPeriksa"] == "0001-01-01T00:00:00" || dataPasien["TglPeriksa"] == null) {
            Swal.fire({
                title: 'Perhatian',
                icon: 'warning',
                html: 'Pasien Harus Diperiksa Sebelum Melakukan Konsul'
            });
        }
        else {
            this.onOpenModalKonsul(this.template);
        }
    }

    // ... Action ketika Context Menu pada grid diclick / pilih
    onSelectGridContextMenu(args: any) {
        let id = args.item.id;

        if (id == "laboratorium") {
            if (this.gridSelectedData != null) {
                let data = this.encryptStorageService.encrypt(JSON.stringify(this.gridSelectedData));
                let secretRouteKey = `${environment.secretKey.routeKey}`;

                this.router.navigate(["/dashboard/order/laboratorium/", data, secretRouteKey]);
            }
            else {
                Swal.fire({
                    title: 'Oops...',
                    icon: 'error',
                    html: 'Data Antrian Pasien Belum Dipilih'
                })
            }
        }
        else if (id == "radiologi") {
            if (this.gridSelectedData != null) {
                let data = this.encryptStorageService.encrypt(JSON.stringify(this.gridSelectedData));
                let secretRouteKey = `${environment.secretKey.routeKey}`;

                this.router.navigate(["/dashboard/order/radiologi/", data, secretRouteKey]);
            }
            else {
                Swal.fire({
                    title: 'Oops...',
                    icon: 'error',
                    html: 'Data Antrian Pasien Belum Dipilih'
                })
            }
        }
        else {
            if (this.gridSelectedData != null) {
                let data = this.encryptStorageService.encrypt(JSON.stringify(this.gridSelectedData));
                let secretRouteKey = `${environment.secretKey.routeKey}`;

                this.router.navigate(["/dashboard/order/resep-digital/", data, secretRouteKey]);
            }
            else {
                Swal.fire({
                    title: 'Oops...',
                    icon: 'error',
                    html: 'Data Antrian Pasien Belum Dipilih'
                })
            }
        }
    }


    // ... Gunakan function ini apabila menggunakan SharedGrid di ng-template
    onInitializedGridKonsul(component: SharedGridComponent) {
        this.gridKonsul = component;
    }

    // ... Menampilkan Modal Konsul
    onOpenModalKonsul(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'modal-lg' })
        );

        this.gridKonsulDataSource = [];

        this.antrianPasienService.onGetGridKonsulColumns()
            .subscribe((_result) => {
                this.gridKonsulColumns = _result;

                this.gridKonsulToolbar = [
                    { text: 'Pilih Dokter', tooltipText: 'Pilih Dokter Untuk Poli Ini', prefixIcon: 'fas fa-tasks', id: 'pilihDokter' },
                    "Search",
                ];
            });
    }

    onCloseModalKonsul() {
        this.modalRef.hide();

        this.gridKonsulDataSource = [];

        this.gridKonsul.grid.refresh();

        console.log(this.gridKonsulDataSource);
    }

    // ... Untuk fill ke input form Poli Konsul
    onGetSelectedDataPoliKonsul(args: any) {
        (<HTMLInputElement>document.getElementById("PolyCodeKonsul")).value = args.PolyCode;
        (<HTMLInputElement>document.getElementById("PolyNameKonsul")).value = args.PolyName;

        this.array.push({
            PolyCode: args.PolyCode,
            PolyName: args.PolyName,
        });

        this.gridKonsulDataSource = this.array;

        this.gridKonsul.grid.refresh();
    }

    // ... Untuk fill ke input form Dokter Konsul
    onGetSelectedDataDokterKonsul(args: any) {
        (<HTMLInputElement>document.getElementById("KodeDokterKonsul")).value = args.KodeDokter;
        (<HTMLInputElement>document.getElementById("NamaDokterKonsul")).value = args.FirstName;

        this.gridKonsul.gridDataSource[this.gridKonsulSelectedRowIndex]["KodeDokterTujuan"] = args.KodeDokter;
        this.gridKonsul.gridDataSource[this.gridKonsulSelectedRowIndex]["DokterTujuan"] = args.FirstName;

        this.gridKonsul.grid.refresh();
    }

    // ... Action ketika row di grid konsul di pilih
    onRowKonsulSelecting(args: any, grid: any) {
        this.gridKonsul = grid;

        this.gridKonsulSelectedData = args.data;
        this.gridKonsulSelectedRowIndex = args.rowIndex;
    }

    // ... Action ketika toolbar grid konsul di click
    onToolbarKonsulClick(args: any, grid: any) {
        let action = args.item.id;

        if (action == "pilihDokter") {
            document.getElementById("btnLookupDokterKonsul").click();
        }
    }

    // ... Action ketika row di grid konsul di double click
    onRowKonsulDoubleClick(args: any) {
        document.getElementById("btnLookupDokterKonsul").click();
    }

    // ... Mapping Data Konsul
    onMappingDataKonsulPasien(gridKonsul: any) {
        let dataSourceGridKonsul = gridKonsul.gridDataSource;
        console.log(dataSourceGridKonsul);

        let dataKonsulPasien: any = [];

        for (let item in dataSourceGridKonsul) {
            const data: any = {};

            data['NoAntrian'] = this.gridSelectedData["NoAntrian"];
            data['IdAntrian'] = this.gridSelectedData["IdAntrian"];
            data['TglMasukRawat'] = this.gridSelectedData["TglMasukRawat"];
            data['NoRegister'] = this.gridSelectedData["NoRegister"];
            data['NamaPasien'] = this.gridSelectedData["NamaPasien"];
            data['MrNo'] = this.gridSelectedData["MrNo"];
            data['FromPolyCode'] = parseInt(item) == 0 ? this.gridSelectedData['PolyCode'] : dataSourceGridKonsul[parseInt(item) - 1]['PolyCode'];
            data['DokterAsal'] = this.gridSelectedData["KodeDokter"];
            data['PartyDebitur'] = this.gridSelectedData["PartyDebitur"];
            data['OrderMrId'] = this.gridSelectedData["OrderMrId"];
            data['PolyCode'] = dataSourceGridKonsul[item]["PolyCode"];
            data['DokterTujuan'] = dataSourceGridKonsul[item]["KodeDokterTujuan"];

            dataKonsulPasien.push(data);
        }

        // console.log(dataKonsulPasien);

        const pesan = "Apakah Anda Yakin?";

        this.utilityService.onShowConfirmationDialog(pesan,
            this.onSubmitKonsul.bind(this, dataKonsulPasien)
        );
    }

    // ... Untuk submit konsul
    onSubmitKonsul(dataKonsulPasien: any) {
        this.antrianPasienService.onPostActionKonsulPasien(dataKonsulPasien)
            .subscribe((_result) => {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    html: 'Data Berhasil Disimpan'
                }).then(() => {
                    this.onCloseModalKonsul();

                    setTimeout(() => {
                        this.onGetAllDataAntrian(this.PolyCode.nativeElement.value);
                    }, 300);
                })
            })
    }
}
