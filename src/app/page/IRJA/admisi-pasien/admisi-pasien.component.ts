import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StorageEncryptService } from 'src/app/service/authentication/storage-encrypt.service';
import { AdmisiPasienService } from 'src/app/service/page/admisi-pasien.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-admisi-pasien',
    templateUrl: './admisi-pasien.component.html',
    styleUrls: ['./admisi-pasien.component.css']
})
export class AdmisiPasienComponent implements OnInit {

    formAdmisiPasien: FormGroup;

    // ... Modal Lookup Variable
    modalButtonLookup: string = "Cari";
    modalColumnsItemLookup: any = [];
    modalFiltersItemLookup: any = [];
    modalStaticFilterLookup: any = [];
    modalUrlLookup: string = "";

    // ... Input Form Variable
    @ViewChild("PolyName") PolyName: ElementRef;
    @ViewChild("NamaUnit") NamaUnit: ElementRef;
    @ViewChild("FirstNameDokter") FirstNameDokter: ElementRef;
    @ViewChild("DebiturOrgCode") DebiturOrgCode: ElementRef;
    @ViewChild("DebiturName") DebiturName: ElementRef;
    @ViewChild("ScvPelayananCode") ScvPelayananCode: ElementRef;
    @ViewChild("ScvPelayananDescr") ScvPelayananDescr: ElementRef;
    @ViewChild("NamaRujukan") NamaRujukan: ElementRef;
    @ViewChild("KotaAsalRujukanName") KotaAsalRujukanName: ElementRef;
    @ViewChild("KeteranganDiagnosaMasuk") KeteranganDiagnosaMasuk: ElementRef;
    @ViewChild("CatatanTerakhir") CatatanTerakhir: ElementRef;
    @ViewChild("NamaUserEntry") NamaUserEntry: ElementRef;


    FormValidState: boolean = true;

    constructor(private formBuilder: FormBuilder,
        private admisiPasienService: AdmisiPasienService,
        private storageEncryptService: StorageEncryptService) { }

    ngOnInit(): void {
        this.onSetAttributeFormAdmisiPasien();

        setTimeout(() => {
            this.onGetUserData();
        }, 500);
    }

    onGetUserData() {
        let user = JSON.parse(this.storageEncryptService.getItem("currentUser"));

        this.NamaUnit.nativeElement.value = user.NamaUnit;

        this.OprEntry.setValue(user.UserId);

        this.NamaUserEntry.nativeElement.value = user.NamaKaryawan;
    }

    onOpenLookup(id: string) {
        this.modalColumnsItemLookup = [];
        this.modalFiltersItemLookup = [];
        this.modalUrlLookup = "";

        if (id == "modalPasien") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetAllPasienByParams";
            this.onSetAttributeLookup("modalPasien");
        } else if (id == "modalPoli") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetPoliByParams";
            this.onSetAttributeLookup("modalPoli");
        }
        else if (id == "modalDokter") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetDokterByParams";
            this.onSetAttributeLookup("modalDokter");
        }
        else if (id == "modalDebitur") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetDebiturByParams";
            this.onSetAttributeLookup("modalDebitur");
        }
        else if (id == "modalKelasPelayanan") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetKelasByParams";
            this.onSetAttributeLookup("modalKelasPelayanan");
        }
        else if (id == "modalAsalRujukan") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetAsalRujukanByParams";
            this.onSetAttributeLookup("modalAsalRujukan");
        }
        else if (id == "modalKotaAsalRujukan") {
            this.modalUrlLookup = "/Daerah/GetKotaByProvinsi";
            this.modalStaticFilterLookup = [
                {
                    "columnName": "IdProvinsi",
                    "filter": "like",
                    "searchText": "",
                    "searchText2": ""
                }
            ];
            this.onSetAttributeLookup("modalKotaAsalRujukan");
        }
        else if (id == "modalDiagnosaAwal") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetDiagnosaByParams";
            this.onSetAttributeLookup("modalDiagnosaAwal");
        }
        else {
            this.modalUrlLookup = "";
            this.onSetAttributeLookup("none");
        }
    }

    // ... Set Attribute Lookup Berdasarkan Id
    onSetAttributeLookup(id: string) {
        let urlColumns: string = "";
        let urlFilters: string = "";

        if (id == "modalPasien") {
            urlColumns = "AdmisiPasien/Lookup/LookupPasien/columns.json";
            urlFilters = "AdmisiPasien/Lookup/LookupPasien/filter.json";
        }
        else if (id == "modalPoli") {
            urlColumns = "AdmisiPasien/Lookup/LookupPoli/columns.json";
            urlFilters = "AdmisiPasien/Lookup/LookupPoli/filter.json";
        }
        else if (id == "modalDokter") {
            urlColumns = "AdmisiPasien/Lookup/LookupDokter/columns.json";
            urlFilters = "AdmisiPasien/Lookup/LookupDokter/filter.json";
        }
        else if (id == "modalDebitur") {
            urlColumns = "AdmisiPasien/Lookup/LookupDebitur/columns.json";
            urlFilters = "AdmisiPasien/Lookup/LookupDebitur/filter.json";
        }
        else if (id == "modalKelasPelayanan") {
            urlColumns = "AdmisiPasien/Lookup/LookupKelasPelayanan/columns.json";
            urlFilters = "AdmisiPasien/Lookup/LookupKelasPelayanan/filter.json";
        }
        else if (id == "modalAsalRujukan") {
            urlColumns = "AdmisiPasien/Lookup/LookupAsalRujukan/columns.json";
            urlFilters = "AdmisiPasien/Lookup/LookupAsalRujukan/filter.json";
        }
        else if (id == "modalKotaAsalRujukan") {
            urlColumns = "AdmisiPasien/Lookup/LookupKotaRujukan/columns.json";
            urlFilters = "AdmisiPasien/Lookup/LookupKotaRujukan/filter.json";
        }
        else if (id == "modalDiagnosaAwal") {
            urlColumns = "AdmisiPasien/Lookup/LookupDiagnosa/columns.json";
            urlFilters = "AdmisiPasien/Lookup/LookupDiagnosa/filter.json";
        }
        else {
            // ... do nothing
        }

        this.admisiPasienService.onGetLookupColumns(urlColumns)
            .subscribe((_result) => {
                this.modalColumnsItemLookup = _result;
            });

        this.admisiPasienService.onGetLookupFilters(urlFilters)
            .subscribe((_result) => {
                this.modalFiltersItemLookup = _result;
            });
    }

    // ... Untuk fill input form Mr.No
    onGetSelectedDataPasien(args: any) {
        // console.log(args);

        this.PartyId.setValue(args.PartyId);
        this.MrNo.setValue(args.MrNo);
        this.NamaPasien.setValue(args.Nama);

        let MrNoValue = (<HTMLInputElement>document.getElementById("MrNo")).value;

        this.MrNo.setValue(MrNoValue);
    }

    // ... Untuk fill input form Poli
    onGetSelectedDataPoli(args: any) {
        this.PolyRi.setValue(args.PolyCode);
        this.PolyName.nativeElement.value = args.PolyName;
    }

    // ... Untuk fill input form Dokter
    onGetSelectedDataDokter(args: any) {
        this.KodeDokterRj.setValue(args.KodeDokter);
        this.FirstNameDokter.nativeElement.value = args.FirstName;

    }

    // ... Untuk fill input form Debitur
    onGetSelectedDataDebitur(args: any) {
        this.PartyDebitur.setValue(args.PartyId);
        this.DebiturOrgCode.nativeElement.value = args.OrgCode;
        this.DebiturName.nativeElement.value = args.Name;
    }

    // .. Untuk fill input form Kelas Pelayanan
    onGetSelectedDataKelasPelayanan(args: any) {
        this.ScvPelayanan.setValue(args.ScvCode);
        this.ScvPelayananCode.nativeElement.value = args.ScvCode;
        this.ScvPelayananDescr.nativeElement.value = args.ScvDescr;
    }

    // .. Untuk fill input form Asal Rujukan
    onGetSelectedDataAsalRujukan(args: any) {
        this.KodeRujukan.setValue(args.KodeRujuk);
        this.NamaRujukan.nativeElement.value = args.NamaRujuk;
    }

    // .. Untuk fill input form Kota Asal Rujukan
    onGetSelectedDataKotaAsalRujukan(args: any) {
        this.KotaAsalRujukan.setValue(args.Id);
        this.KotaAsalRujukanName.nativeElement.value = args.Nama;
    }

    // .. Untuk fill input form Diagnosa Awal
    onGetSelectedDataDiagnosaAwal(args: any) {
        this.DiagnosaMasuk.setValue(args.IcdCode);
        this.KeteranganDiagnosaMasuk.nativeElement.value = args.IcdDescr;
    }

    onSubmitForm(formValue: FormGroup) {
        console.log(formValue);

        // this.admisiPasienService.onSubmitForm(formValue)
        //     .subscribe(
        //         (_result) => {
        //             Swal.fire({
        //                 title: 'Data Berhasil Disimpan',
        //                 icon: "success",
        //             }).then(() => {
        //                 this.formAdmisiPasien.reset();
        //                 this.onClearForm();
        //             });
        //         },
        //         (_error) => {
        //             Swal.fire({
        //                 title: 'Oops...',
        //                 icon: "error",
        //                 html: _error
        //             });
        //         })

        // if (formValue.valid) {
        //     this.FormValidState = true;
        // } else {
        //     this.FormValidState = false;
        // }
    }

    onClearForm() {
        this.PolyName.nativeElement.value = "";
        this.NamaUnit.nativeElement.value = "";
        this.FirstNameDokter.nativeElement.value = "";
        this.DebiturOrgCode.nativeElement.value = "";
        this.DebiturName.nativeElement.value = "";
        this.ScvPelayananCode.nativeElement.value = "";
        this.ScvPelayananDescr.nativeElement.value = "";
        this.NamaRujukan.nativeElement.value = "";
        this.KotaAsalRujukanName.nativeElement.value = "";
        this.KeteranganDiagnosaMasuk.nativeElement.value = "";
        this.CatatanTerakhir.nativeElement.value = "";
        this.NamaUserEntry.nativeElement.value = "";
    }

    // ... Set Form Admisi Pasien Attribute
    onSetAttributeFormAdmisiPasien() {
        this.formAdmisiPasien = this.formBuilder.group({
            "PartyId": [0, []],
            "MrNo": ["", [Validators.required]],
            "NamaPasien": ["", []],
            "TglMasukRawat": [null, []],
            "NoRegister": [0, []],
            "TipePasien": ["OUT", []],
            "KodeRujukan": ["", []],
            "KotaAsalRujukan": ["", []],
            "Perujuk": ["", []],
            "AsalMasuk": ["", []],
            "KodeDokterRj": ["", []],
            "TglRujukRi": [null, []],
            "FromNoRegister": [0, []],
            "TransferToNoreg": [0, []],
            "DiagnosaMasuk": ["", []],
            "KetDiagnosaMasuk": ["", []],
            "Keluhan": ["", []],
            "DiagnosaAkhir": ["", []],
            "KetDiagnosaAkhir": ["", []],
            "TglKeluar": [null, []],
            "KondisiKeluar": [0, []],
            "CaraKeluar": [0, []],
            "SebabRi": ["", []],
            "DokterPerujukRi": ["", []],
            "PolyAsalRi": ["", []],
            "PolyRi": ["", []],
            "SubPoly": ["", []],
            "PartyDebitur": [0, []],
            "InsNo": ["", []],
            "InsExpDate": [null, []],
            "ScvCode": ["", []],
            "ScvPelayanan": ["", []],
            "Balance": [0, []],
            "BalancePhar": [0, []],
            "Deposite": [0, []],
            "TotalCharge": [0, []],
            "TicketCode": ["", []],
            "PayTicketDate": [null, []],
            "OprEntry": [0, []],
            "IsVerifiedCoa": ["", []],
            "IsBirth": ["", []],
            "PayCompleteDate": [null, []],
            "CancelDate": [null, []],
            "SendDate": [null, []],
            "CancelBy": [0, []],
            "CancelReason": ["", []],
            "TransferFrom": ["", []],
            "JenisPelayananId": ["", []],
            "KeterbatasanFisik": [0, []],
            "PpjpEmpId": [0, []],
            "BillProcessDate": [null, []],
            "Catatan": [null, []]
        })
    }

    get PartyId() { return this.formAdmisiPasien.get("PartyId") }
    get NoRegister() { return this.formAdmisiPasien.get("NoRegister") }
    get MrNo() { return this.formAdmisiPasien.get("MrNo") }
    get NamaPasien() { return this.formAdmisiPasien.get("NamaPasien") }
    get TglMasukRawat() { return this.formAdmisiPasien.get("TglMasukRawat") }
    get PolyRi() { return this.formAdmisiPasien.get("PolyRi") }
    get KodeDokterRj() { return this.formAdmisiPasien.get("KodeDokterRj") }
    get PartyDebitur() { return this.formAdmisiPasien.get("PartyDebitur") }
    get ScvPelayanan() { return this.formAdmisiPasien.get("ScvPelayanan") }
    get FromNoRegister() { return this.formAdmisiPasien.get("FromNoRegister") }
    get ScvCode() { return this.formAdmisiPasien.get("ScvCode") }
    get TicketCode() { return this.formAdmisiPasien.get("TicketCode") }
    get KodeRujukan() { return this.formAdmisiPasien.get("KodeRujukan") }
    get KotaAsalRujukan() { return this.formAdmisiPasien.get("KotaAsalRujukan") }
    get Perujuk() { return this.formAdmisiPasien.get("Perujuk") }
    get DiagnosaMasuk() { return this.formAdmisiPasien.get("DiagnosaMasuk") }
    get Keluhan() { return this.formAdmisiPasien.get("Keluhan") }
    get Catatan() { return this.formAdmisiPasien.get("Catatan") }
    get OprEntry() { return this.formAdmisiPasien.get("OprEntry") }
}
