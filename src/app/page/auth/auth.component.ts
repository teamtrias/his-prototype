import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthResponseData, AuthService } from 'src/app/service/authentication/auth.service';
import { StorageEncryptService } from 'src/app/service/authentication/storage-encrypt.service';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
    authenticationMode: boolean = true;
    error: string = null;
    isLoading: boolean = false;

    constructor(private router: Router, private authService: AuthService, private storageEncryptService: StorageEncryptService) { }

    ngOnInit(): void {
    }

    onSwitchMode() {
        this.authenticationMode = !this.authenticationMode;
    }

    onSubmitForm(form: NgForm) {
        if (!form.valid) {
            return;
        }

        let username = form.value.email;
        let password = form.value.password;
        let encryptPassword = this.storageEncryptService.encryptSHA1(password);

        let dataAuth = {
            'UserName': username,
            'UserPassword': encryptPassword
        };

        this.isLoading = true;

        this.authService.login(dataAuth)
            .subscribe((_user) => {
                this.isLoading = false;
                this.router.navigateByUrl('/dashboard/home');

            }, (pesanError) => {
                console.log(pesanError);

                this.isLoading = false;
                this.error = pesanError;
            });

        form.reset();
    }

    onHideErrorMessage() {
        return this.error = null;
    }
}
