import { AfterViewInit, Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SharedGridComponent } from 'src/app/components/shared-grid/shared-grid.component';
import { StorageEncryptService } from 'src/app/service/authentication/storage-encrypt.service';
import { LaboratoriumService } from 'src/app/service/page/laboratorium.service';
import * as lodash from 'lodash';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-laboratorium',
    templateUrl: './laboratorium.component.html',
    styleUrls: ['./laboratorium.component.css']
})
export class LaboratoriumComponent implements OnInit, AfterViewInit {

    tabHeader: any = [];

    // ... Card Header Attribute
    dataPasien: any;
    namaPasien: string = "";
    noRegister: string = "";
    namaDokter: string = "";
    kodeDokter: string = "";
    mrNo: string = "";
    sex: string = "";
    tglLahir: string = "";
    ppjp: string = "";
    umur: string = "";
    tglMasukRawat: string = "";
    debiturName: string = "";

    //... Grid Daftar Order Attribute
    gridDaftarOrder: SharedGridComponent = null;
    gridIdDaftarOrder: string = "GridDaftarOrder";
    gridDataSourceDaftarOrder: any = [];
    gridHeightDaftarOrder: string = "120";
    gridLinesDaftarOrder: string = "Both";
    gridColumnsDaftarOrder: any = [];

    //... Grid Detail Order Attribute
    gridDetailOrder: SharedGridComponent = null;
    gridIdDetailOrder: string = "GridDetailOrder";
    gridDataSourceDetailOrder: any = [];
    gridHeightDetailOrder: string = "120";
    gridLinesDetailOrder: string = "Both";
    gridColumnsDetailOrder: any = [];

    // ... Tab Tarif Attribute
    tabTarifLab: any = [];
    tabTarifLabIndexNumber: number = 0;

    buttonTarifLab: any = [];
    buttonTarifLabIndexNumber: number = 0;

    tarifPerKelompok: any = [];

    // ... Modal BsModalRef
    modalRef: BsModalRef;

    // ... Modal Lookup Attribute
    modalButtonLookup: string = "Cari";
    modalColumnsItemLookup: any = [];
    modalFiltersItemLookup: any = [];
    modalUrlLookup: string = "";

    formOrderLaboratorium: FormGroup;

    formInputDiagnosa: FormGroup;

    //... Grid Diagnosa Attribute
    gridDiagnosa: SharedGridComponent = null;
    gridIdDiagnosa: string = "GridDiagnosa";
    gridDataSourceDiagnosa: any = [];
    gridHeightDiagnosa: string = "350";
    gridLinesDiagnosa: string = "Both";
    gridColumnsDiagnosa: any = [];
    gridDiagnosaRowSelected: any;

    //... Grid Pemeriksaan Attribute
    gridPemeriksaan: SharedGridComponent = null;
    gridIdPemeriksaan: string = "GridPemeriksaan";
    gridDataSourcePemeriksaan: any = [];
    gridHeightPemeriksaan: string = "250";
    gridLinesPemeriksaan: string = "Both";
    gridColumnsPemeriksaan: any = [];

    //... Grid Order Attribute
    gridOrderLab: SharedGridComponent = null;
    gridIdOrderLab: string = "GridOrderLab";
    gridDataSourceOrderLab: any = [];
    gridHeightOrderLab: string = "200";
    gridLinesOrderLab: string = "Both";
    gridColumnsOrderLab: any = [];

    constructor(private activatedRoute: ActivatedRoute,
        private storageEncryptService: StorageEncryptService,
        private laboratoriumService: LaboratoriumService,
        private bsModalService: BsModalService,
        private formBuilder: FormBuilder) { }

    ngOnInit(): void {
        // ... Panggil function set tab header
        this.onSetTabHeader();

        // ... Panggil function set form group properties
        this.onSetFormOrderLabProperties();

        // ... Panggil function set form group properties
        this.onSetFormInputDiagnosaProperties();
    }

    ngAfterViewInit(): void {
        // ... Panggil function get route data pasien
        setTimeout(() => {
            this.onGetRouteDataFromAntrianPasien();
        }, 1500);

        // ... Panggil functionn set attribute grid
        this.onSetGridAttribute();
    }

    //... Untuk mendapatkan route Data dari Antrian Pasien
    onGetRouteDataFromAntrianPasien() {
        let dataPasien = JSON.parse(
            this.storageEncryptService.decrypt(this.activatedRoute.snapshot.params["data"])
        );

        this.dataPasien = dataPasien;

        console.log(this.dataPasien);

        this.namaPasien = dataPasien.NamaPasien;
        this.noRegister = dataPasien.NoRegister;
        this.kodeDokter = dataPasien.KodeDokter;
        this.namaDokter = dataPasien.NamaDokter;
        this.mrNo = dataPasien.MrNo;
        this.sex = dataPasien.Sex;
        this.tglLahir = dataPasien.TanggalLahir;
        this.umur = dataPasien.Umur;
        this.tglMasukRawat = dataPasien.TglMasukRawat;
        this.debiturName = dataPasien.DebiturName;

        this.onFetchGridDaftarOrderData(dataPasien.PartyId);
    }

    // ... On Set Tab Header
    onSetTabHeader() {
        this.tabHeader = [
            {
                id: "dataOrder",
                iconCss: "fas fa-table fa-sm",
                text: "Data Order"
            },
            {
                id: "inputOrder",
                iconCss: "fas fa-plus fa-sm",
                text: "Order Laboratorium"
            },
        ]
    }

    // ... Gunakan function ini apabila menggunakan SharedGrid di ng-template
    onInitializedGridDaftarOrder(component: SharedGridComponent) {
        this.gridDaftarOrder = component;
    }

    // ... Gunakan function ini apabila menggunakan SharedGrid di ng-template
    onInitializedGridDetailOrder(component: SharedGridComponent) {
        this.gridDetailOrder = component;
    }

    // ... Untuk mendapatkan grid column
    onSetGridAttribute() {
        this.laboratoriumService.onGetGridColumn("Order/Laboratorium/DaftarOrder.json")
            .subscribe((_result) => {
                this.gridColumnsDaftarOrder = _result;
            });

        this.laboratoriumService.onGetGridColumn("Order/Laboratorium/DetailOrder.json")
            .subscribe((_result) => {
                this.gridColumnsDetailOrder = _result;
            });
    }

    // ... On Fetch Data Source Grid Daftar Order 
    onFetchGridDaftarOrderData(PartyId: any) {
        let data = {
            "PartyId": PartyId,
            "CekType": "TDLB"
        };

        this.laboratoriumService.onFetchGridDataSource(
            "/TransaksiOrderLaboratorium/GetHistoryOrderLab",
            data
        ).subscribe((_result) => {
            setTimeout(() => {
                this.gridDataSourceDaftarOrder = _result;
            }, 2100);
        })
    }

    onGridDaftarOrderDoubleClick(args: any) {

        args.CekType = "TDLB";

        this.laboratoriumService.onFetchGridDataSource(
            "/TransaksiOrderLaboratorium/GetHistoryOrderLabDetail",
            args
        ).subscribe((_result) => {
            this.gridDataSourceDetailOrder = _result;
        })
    }

    // ... On Fetch Data Tarif 
    onFetchDataTarifConfigurations(dataPasien: any) {
        let currentDate: Date = new Date();

        let data = {
            "PartyDebitur": dataPasien.PartyDebitur ? dataPasien.PartyDebitur : 3,
            "ScvCode": dataPasien.KelasPelayanan ? dataPasien.KelasPelayanan : 2,
            "TglMasukRawat": dataPasien.TglMasukRawat ? dataPasien.TglMasukRawat : currentDate,
            "GroupPenunjang": "LAB",
        };

        this.laboratoriumService.onFetchTarifConfiguration(JSON.stringify(data))
            .subscribe((_result) => {
                if (this.buttonTarifLab.length == 0 && this.tabTarifLab.length == 0) {
                    // ... Jika tabTarifLab tidak memiliki value maka 
                    this.tabTarifLab = _result.TabTarifLab;
                }
                else {
                    //... Jika tabTarifLab memiliki value maka tidak perlu diisi lagi
                }
            });
    }

    onGetTabTarifLabIndex(index: any) {
        this.tabTarifLabIndexNumber = index;
    }

    onCreatedTabTarif() {
        this.onFetchDataTarifConfigurations(this.dataPasien);

        this.laboratoriumService.onGetGridColumn("Order/Laboratorium/GridOrder.json")
            .subscribe((_result) => {
                this.gridColumnsOrderLab = _result;
            });
    }

    // ... On Fetch Data Tarif per Kelompok 
    onShowTarifByButtonPerKelompok(tarif: any, index: any) {
        this.buttonTarifLabIndexNumber = index;
        this.tarifPerKelompok = [];
        this.tarifPerKelompok = tarif;
    }

    // ... On Change Checkbox Tarif
    onChangeTarif(args: any, tarif: any) {
        const IdCheckbox = args.target.id;
        const IsChecked = args.target.checked;

        let DataTarifCheckbox = tarif;
        let DataUntukRenderCheckbox: any;

        if (IsChecked) {
            DataTarifCheckbox["ClinicalDetail"] = "";
            DataTarifCheckbox["Qty"] = 1;
            DataTarifCheckbox["HospitalFee"] = DataTarifCheckbox["HosFee"];

            this.gridOrderLab.gridDataSource.push(DataTarifCheckbox);
            this.gridOrderLab.grid.refresh();

            DataUntukRenderCheckbox = this.gridOrderLab.gridDataSource;
        } else {
            let dataTarifFilterized = this.gridDataSourceOrderLab.filter((dataFilterized: any) => {
                return dataFilterized["KodeTarif"] != IdCheckbox;
            });

            this.gridDataSourceOrderLab = dataTarifFilterized;
            this.gridOrderLab.grid.refresh();

            DataUntukRenderCheckbox = this.gridDataSourceOrderLab;
        }

        this.onRenderCheckbox(DataUntukRenderCheckbox);
    }

    // ... Untuk merender Checkbox, karena checkbox akan ter-uncek apabila mengganti tab
    onRenderCheckbox(data: any) {
        if (data.length > 0) {
            for (let index in this.tarifPerKelompok) {

                this.tarifPerKelompok[index].checked = false;

                data.filter((filterized: any) => {
                    if (this.tarifPerKelompok[index].KodeTarif == filterized.KodeTarif) {
                        this.tarifPerKelompok[index].checked = true;
                    }
                });
            }
        } else {
            for (let index in this.tarifPerKelompok) {

                this.tarifPerKelompok[index].checked = false;

                data.filter((filterized: any) => {
                    if (this.tarifPerKelompok[index].KodeTarif == filterized.KodeTarif) {
                        this.tarifPerKelompok[index].checked = true;
                    }
                });
            }
        }
    }

    // ... On Open Dialog Input Diagnosa and Pilih Diagnosa
    onOpenModal(template: TemplateRef<any>) {
        this.modalRef = this.bsModalService.show(
            template,
            Object.assign({}, {
                class: 'modal-lg'
            })
        );

        let id = template["_declarationTContainer"]["localNames"][0];

        if (id == "templateDialogPemeriksaanLainLain") {
            this.laboratoriumService.onGetGridColumn("Order/Laboratorium/Pemeriksaan/GridHeader.json")
                .subscribe((_result) => {
                    this.gridColumnsPemeriksaan = _result;
                });

            this.laboratoriumService.onFetchTarifPemeriksaanLain("KodeTarifPemeriksaan");
        }
        else if (id == "templateDialogPilihDiagnosa") {
            this.laboratoriumService.onGetGridColumn("Order/Laboratorium/Diagnosa/GridHeader.json")
                .subscribe((_result) => {
                    this.gridColumnsDiagnosa = _result;
                });

            this.onGetGridDiagnosaDatasource();
        }
    }

    onCloseModal(form?: FormGroup) {
        this.modalRef.hide();

        form.reset();
    }

    // ... On Open Lookup Diagnosa
    onOpenLookup(id: string) {
        this.modalColumnsItemLookup = [];
        this.modalFiltersItemLookup = [];
        this.modalUrlLookup = "";

        if (id == "modalDiagnosa") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetDiagnosaByParams";
            this.onSetAttributeLookup("modalDiagnosa");
        }
        else if (id == "modalDokter") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetDokterByParams";
            this.onSetAttributeLookup("modalDokter");
        }
    }

    // ... On Set Form Input Diagnosa Properties
    onSetFormInputDiagnosaProperties() {
        this.formInputDiagnosa = this.formBuilder.group({
            "Mrno": ["", []],
            "NoRegister": [0, []],
            "TglMasukRawat": ["", []],
            "PatName": ["", []],
            "PatType": ["OUT", []],
            "ReqPoly": ["", []],
            "KodeDokter": ["", []],
            "DiagnosaIcdAwal": ["", []],
            "SoapSubjective": ["", []],
            "SoapObjective": ["", []],
            "SoapAssesment": ["", []],
            "Catatan": ["", []],
            "SoapPlan": ["", []],
        });
    }

    // ... On Set Attribute Lookup 
    onSetAttributeLookup(id: string) {
        let urlColumns: string = "";
        let urlFilters: string = "";

        if (id == "modalDiagnosa") {
            urlColumns = "Order/Laboratorium/Lookup/LookupDiagnosaAwal/columns.json";
            urlFilters = "Order/Laboratorium/Lookup/LookupDiagnosaAwal/filters.json";
        } else if (id == "modalDokter") {
            urlColumns = "AdmisiPasien/Lookup/LookupDokter/columns.json";
            urlFilters = "AdmisiPasien/Lookup/LookupDokter/filter.json";
        } else {

        }

        this.laboratoriumService.onGetLookupColumns(urlColumns)
            .subscribe((_result) => {
                this.modalColumnsItemLookup = _result;
            });

        this.laboratoriumService.onGetLookupFilters(urlFilters)
            .subscribe((_result) => {
                this.modalFiltersItemLookup = _result;
            });
    }

    // ... On Get Selected Diagnosa
    onGetSelectedDiagnosa(args: any) {
        this.DiagnosaIcdAwal.setValue(args.IcdCode);
        (<HTMLInputElement>document.getElementById("NamaDiagnosaAwal")).value = args.IcdDescr;
    }

    // ... On Submit Form Input Diagnosa
    onSubmitFormInputDiagnosa(formValue: any) {
        formValue.Mrno = this.dataPasien.MrNo;
        formValue.NoRegister = this.dataPasien.NoRegister;
        formValue.TglMasukRawat = this.dataPasien.TglMasukRawat;
        formValue.PatName = this.dataPasien.NamaPasien;
        formValue.PatType = "OUT";
        formValue.ReqPoly = this.dataPasien.PolyCode;
        formValue.KodeDokter = this.kodeDokter;
        formValue.Catatan = this.SoapAssesment.value;

        this.laboratoriumService.onSubmitForm(
            "/TransaksiOrderLaboratorium/InsertDiagnosa",
            formValue,
        ).subscribe((_result) => {
            let NamaDiagnosaAwal = (<HTMLInputElement>document.getElementById("NamaDiagnosaAwal")).value;
            let SoapAssesment = formValue.SoapAssesment;

            Swal.fire({
                title: 'Data Berhasil Disimpan',
                icon: "success",
            }).then(() => {
                this.Note.setValue("(" + NamaDiagnosaAwal + "). " + SoapAssesment);
            }).then(() => {
                this.onCloseModal(this.formInputDiagnosa);
            });
        })
    }

    // ... On Get Selected Dokter Peminta
    onGetSelectedDokterPeminta(args: any) {
        (<HTMLInputElement>document.getElementById("NamaDokter")).value = args.FirstName;
        this.ReqDocCode.setValue(args.KodeDokter);
    }

    // ... On Set FormOrderLaboratorium Property 
    onSetFormOrderLabProperties() {
        this.formOrderLaboratorium = this.formBuilder.group({
            "Mrno": ["", []],
            "NoRegister": [0, []],
            "TglMasukRawat": ["", []],
            "PatName": ["", []],
            "PatType": ["OUT", []],
            "ReqPoly": ["", []],
            "Note": ["", []],
            "NoteSample": ["", []],
            "ReqDocCode": ["", []],
            "IsOrderDarah": ["N", []],
            "CekType": ["TDLB", []],
            "OmLabs": ["", []],
        })
    }



    // ... Gunakan function ini apabila menggunakan SharedGrid di ng-template
    onInitializedGridDiagnosa(component: SharedGridComponent) {
        this.gridDiagnosa = component;
    }

    // ... Untuk mendapatkan Grid Diagnosa Datasource
    onGetGridDiagnosaDatasource() {
        let data = [{
            "columnName": "NoRegister",
            "filter": "equal",
            "searchText": this.noRegister,
            "searchText2": ""
        }];

        this.laboratoriumService.onFetchGridDataSource(
            "/TransaksiOrderLaboratorium/GetDiagnosaPenyakit",
            data
        ).subscribe((_result) => {
            setTimeout(() => {
                this.gridDataSourceDiagnosa = _result;
            }, 2100);
        })
    }

    // ... Ketika memilih salah satu baris Grid Diagnosa
    onSelectingGridDiagnosa(args: any) {
        console.log(args);
        this.gridDiagnosaRowSelected = args.rowIndexes;
    }

    // ... Ketika menyimpan Grid Diagnosa
    onSubmitGridDiagnosa(gridDiagnosa: SharedGridComponent) {
        let data = gridDiagnosa.gridDataSource;

        let rowIndexes = this.gridDiagnosaRowSelected;

        let keteranganKlinik: string = "";

        if (rowIndexes.length > 0) {
            for (let i = 0; i < rowIndexes.length; i++) {
                keteranganKlinik += "(" + data[i]["DiagnosaIcdAwalDescr"] + "). " + data[i]["Catatan"] + "\n";
            }
        };

        Swal.fire({
            title: 'Data Berhasil Disimpan',
            icon: "success",
        }).then(() => {
            this.Note.setValue(keteranganKlinik);
        }).then(() => {
            this.modalRef.hide();
        });
    }



    // ... Gunakan function ini apabila menggunakan SharedGrid di ng-template
    onInitializedGridPemeriksaan(component: SharedGridComponent) {
        this.gridPemeriksaan = component;
    }

    onCloseDialogPemeriksaanLain() {
        this.modalRef.hide();

        this.gridDataSourcePemeriksaan = [];
        this.gridPemeriksaan.grid.refresh();

        this.onResetFormDialogPemeriksaan();
    }

    // ... Untuk menambahkan baris ke dalam grid Pemeriksaan
    onAddPemeriksaanLainLain() {
        let KodeTarifPemeriksaan = (<HTMLInputElement>document.getElementById("KodeTarifPemeriksaan")).value;
        let NamaPemeriksaanLain = (<HTMLInputElement>document.getElementById("NamaPemeriksaanLain")).value;
        let CatatanPemeriksaanLain = (<HTMLInputElement>document.getElementById("CatatanPemeriksaanLain")).value;

        const DataPemeriksaanLain: Object = {
            "KodeTarif": KodeTarifPemeriksaan,
            "NamaTarif": NamaPemeriksaanLain,
            "ClinicalDetail": CatatanPemeriksaanLain,
            "DeptCode": "LAB",
            "DoctFee": 0,
            "HospitalFee": 0,
            "AnasFee": 0,
            "MedicalFee": 0,
            "CompFee": 0,
            "Idpaket": 0,
            "IurBiaya": 0,
            "Qty": 1
        };

        if (NamaPemeriksaanLain != "") {
            this.gridPemeriksaan.gridDataSource.push(DataPemeriksaanLain);
            this.gridPemeriksaan.grid.refresh();

            setTimeout(() => {
                this.onResetFormDialogPemeriksaan();
            }, 100);
        }
        else {
            Swal.fire(
                'Oops...',
                'Nama Pemeriksaan Tidak Boleh Kosong',
                'error'
            );
        }
    }

    // ... Action ketika berhasil menambahkan baris grid Pemeriksaan
    onResetFormDialogPemeriksaan() {
        this.laboratoriumService.onFetchTarifPemeriksaanLain("KodeTarifPemeriksaan");

        (<HTMLInputElement>document.getElementById("NamaPemeriksaanLain")).value = "";
        (<HTMLInputElement>document.getElementById("CatatanPemeriksaanLain")).value = "";
    }

    // ... Untuk menyimpan dialog Pemeriksaan Lain - Lain
    onSubmitDialogPemeriksaanLainLain() {
        for (let index in this.gridPemeriksaan.gridDataSource) {
            this.gridOrderLab.gridDataSource.push(this.gridPemeriksaan.gridDataSource[index]);
        }

        this.gridOrderLab.grid.refresh();

        setTimeout(() => {
            this.onCloseDialogPemeriksaanLain();
        }, 200);
    }


    // ... Gunakan function ini apabila menggunakan SharedGrid di ng-template
    onInitializedGridOrderLab(component: SharedGridComponent) {
        this.gridOrderLab = component;
    }

    // ... On Submit Form 
    onSubmitFormOrderLab(value: any) {
        value.Mrno = this.dataPasien.MrNo;
        value.NoRegister = parseInt(this.dataPasien.NoRegister);
        value.TglMasukRawat = this.dataPasien.TglMasukRawat;
        value.PatName = this.dataPasien.NamaPasien;
        value.PatType = "OUT";
        value.ReqPoly = this.dataPasien.PolyCode;
        value.OmLabs = this.gridOrderLab.gridDataSource;

        this.laboratoriumService.onSubmitForm(
            "/TransaksiOrderLaboratorium/InsertUpdate",
            value,
        ).subscribe((_result) => {

            console.log(_result);

            Swal.fire({
                title: 'Data Berhasil Disimpan',
                icon: "success",
            }).then(() => {
                location.reload();
            })
        });
    }

    get DiagnosaIcdAwal() { return this.formInputDiagnosa.get("DiagnosaIcdAwal") }
    get SoapSubjective() { return this.formInputDiagnosa.get("SoapSubjective") }
    get SoapObjective() { return this.formInputDiagnosa.get("SoapObjective") }
    get SoapAssesment() { return this.formInputDiagnosa.get("SoapAssesment") }
    get Catatan() { return this.formInputDiagnosa.get("Catatan") }
    get SoapPlan() { return this.formInputDiagnosa.get("SoapPlan") }

    get Note() { return this.formOrderLaboratorium.get("Note") }
    get NoteSample() { return this.formOrderLaboratorium.get("NoteSample") }
    get ReqDocCode() { return this.formOrderLaboratorium.get("ReqDocCode") }
}
