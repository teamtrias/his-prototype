import { AfterViewInit, Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { SharedGridComponent } from 'src/app/components/shared-grid/shared-grid.component';
import { StorageEncryptService } from 'src/app/service/authentication/storage-encrypt.service';
import { LaboratoriumService } from 'src/app/service/page/laboratorium.service';
import { RadiologiService } from 'src/app/service/page/radiologi.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-radiologi',
    templateUrl: './radiologi.component.html',
    styleUrls: ['./radiologi.component.css']
})
export class RadiologiComponent implements OnInit, AfterViewInit {

    tabHeader: any = [];

    //... Grid Daftar Order Attribute
    gridDaftarOrder: SharedGridComponent = null;
    gridIdDaftarOrder: string = "GridDaftarOrder";
    gridDataSourceDaftarOrder: any = [];
    gridHeightDaftarOrder: string = "120";
    gridLinesDaftarOrder: string = "Both";
    gridColumnsDaftarOrder: any = [];

    //... Grid Detail Order Attribute
    gridDetailOrder: SharedGridComponent = null;
    gridIdDetailOrder: string = "GridDetailOrder";
    gridDataSourceDetailOrder: any = [];
    gridHeightDetailOrder: string = "120";
    gridLinesDetailOrder: string = "Both";
    gridColumnsDetailOrder: any = [];

    // ... Card Header Attribute
    dataPasien: any;
    namaPasien: string = "";
    noRegister: string = "";
    namaDokter: string = "";
    kodeDokter: string = "";
    mrNo: string = "";
    sex: string = "";
    tglLahir: string = "";
    ppjp: string = "";
    umur: string = "";
    tglMasukRawat: string = "";
    debiturName: string = "";

    // ... Tab Tarif Dynamic
    tabTarifRadiologi: any = [];
    tabTarifRadiologiIndexNumber: any;
    buttonTarifRadiologi: any = [];
    tarifPerKelompok: any = [];
    currentTarifSelectedIndex: any;

    // ... Modal BsModalRef
    modalRef: BsModalRef;

    // ... Form Group
    formInputDiagnosa: FormGroup;
    formOrderRadiologi: FormGroup;

    // ... Modal Lookup Attribute
    modalButtonLookup: string = "Cari";
    modalColumnsItemLookup: any = [];
    modalFiltersItemLookup: any = [];
    modalUrlLookup: string = "";

    //... Grid Diagnosa Attribute
    gridDiagnosa: SharedGridComponent = null;
    gridIdDiagnosa: string = "GridDiagnosa";
    gridDataSourceDiagnosa: any = [];
    gridHeightDiagnosa: string = "350";
    gridLinesDiagnosa: string = "Both";
    gridColumnsDiagnosa: any = [];
    gridSelectionDiagnosa: any = { type: "Multiple", mode: "Row" };
    gridDiagnosaRowSelected: any;

    //... Grid Order Attribute
    gridOrderRad: SharedGridComponent = null;
    gridIdOrderRad: string = "GridOrderLab";
    gridDataSourceOrderRad: any = [];
    gridHeightOrderRad: string = "200";
    gridLinesOrderRad: string = "Both";
    gridColumnsOrderRad: any = [];

    //... Grid Pemeriksaan Attribute
    gridPemeriksaan: SharedGridComponent = null;
    gridIdPemeriksaan: string = "GridPemeriksaan";
    gridDataSourcePemeriksaan: any = [];
    gridHeightPemeriksaan: string = "250";
    gridLinesPemeriksaan: string = "Both";
    gridColumnsPemeriksaan: any = [];

    constructor(
        private storageEncryptService: StorageEncryptService,
        private laboratoriumService: LaboratoriumService,
        private radiologiService: RadiologiService,
        private activatedRoute: ActivatedRoute,
        private bsModalService: BsModalService,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit(): void {
        // ... Panggil function set tab header
        this.onSetTabHeader();

        // ... Panggil function fetch data pasien from route
        this.onFetchDataPasienFromRoute();

        // ... Panggil function set form group properties
        this.onSetFormInputDiagnosaProperties();

        // ... Panggil function set form group properties
        this.onSetFormOrderRadiologiProperties();
    }

    ngAfterViewInit() {
        this.onSetGridAttribute();
    }

    // ... Tab Header Setting ... 
    // ..........................
    onSetTabHeader() {
        this.tabHeader = [
            {
                id: "dataOrder",
                iconCss: "fas fa-table fa-sm",
                text: "Data Order"
            },
            {
                id: "inputOrder",
                iconCss: "fas fa-plus fa-sm",
                text: "Order Radiologi"
            },
        ]
    }

    // ... Grid Attribute Setting ... 
    // ..........................
    onSetGridAttribute() {
        this.radiologiService.onGetGridColumn("Order/Radiologi/DaftarOrder.json")
            .subscribe((_result) => {
                this.gridColumnsDaftarOrder = _result;
            });

        this.radiologiService.onGetGridColumn("Order/Radiologi/DetailOrder.json")
            .subscribe((_result) => {
                this.gridColumnsDetailOrder = _result;
            });
    }

    // ... Grid Daftar Order Setting ...
    // .................................
    onInitializedGridDaftarOrder(component: SharedGridComponent) {
        this.gridDaftarOrder = component;
    }

    onGridDaftarOrderDoubleClick(args: any) {

        let data = {
            "NoRegister": args.NoRegister,
            "ExamOfferingId": args.ExamOfferingId
        }

        this.radiologiService.onFetchHistoryOrderLabDetail(data)
            .subscribe((_result) => {
                this.gridDataSourceDetailOrder = _result;
            })
    }

    // ... Grid Detail Order Setting ...
    // .................................
    onInitializedGridDetailOrder(component: SharedGridComponent) {
        this.gridDetailOrder = component;
    }

    // ... Dapatkan Data Pasien from Route ...
    // .......................................
    onFetchDataPasienFromRoute() {
        const dataPasienFromRoute = JSON.parse(
            this.storageEncryptService.decrypt(
                this.activatedRoute.snapshot.params["data"]
            ));

        this.dataPasien = dataPasienFromRoute;
        this.namaPasien = dataPasienFromRoute.NamaPasien;
        this.noRegister = dataPasienFromRoute.NoRegister;
        this.kodeDokter = dataPasienFromRoute.KodeDokter;
        this.namaDokter = dataPasienFromRoute.NamaDokter;
        this.mrNo = dataPasienFromRoute.MrNo;
        this.sex = dataPasienFromRoute.Sex;
        this.tglLahir = dataPasienFromRoute.TanggalLahir;
        this.umur = dataPasienFromRoute.Umur;
        this.tglMasukRawat = dataPasienFromRoute.TglMasukRawat;
        this.debiturName = dataPasienFromRoute.DebiturName;

        this.onFetchGridDaftarOrderData(dataPasienFromRoute.PartyId);
    }

    // ... Dapatkan History Order Radiologi setelah Get Data Pasien ...
    // ................................................................
    onFetchGridDaftarOrderData(PartyId: any) {
        let data = { "PartyId": PartyId };

        this.radiologiService.onFetchHistoryOrderLab(data)
            .subscribe((_result) => {
                this.gridDataSourceDaftarOrder = _result;
            });
    }

    // ... On Click Tab Order ...
    // ..........................
    onClickTabOrderRadiologi() {
        this.onFetchDataTarifConfiguration(this.dataPasien);

        this.radiologiService.onGetGridColumn("Order/Radiologi/Order.json")
            .subscribe((_result) => {
                this.gridColumnsOrderRad = _result;
            });
    }

    // ... On Fetch Tab Dynamic Order Radiologi ...
    // ............................................
    onFetchDataTarifConfiguration(dataPasien: any) {
        let currentDate: Date = new Date();

        let data = {
            "PartyDebitur": dataPasien.PartyDebitur ? dataPasien.PartyDebitur : 3,
            "ScvCode": dataPasien.KelasPelayanan ? dataPasien.KelasPelayanan : 2,
            "TglMasukRawat": dataPasien.TglMasukRawat ? dataPasien.TglMasukRawat : currentDate,
            "GroupPenunjang": "RAD",
        };

        this.radiologiService.onFetchTarifConfiguration(data)
            .subscribe((_result) => {
                if (this.buttonTarifRadiologi.length == 0 && this.tabTarifRadiologi.length == 0) {
                    // ... Jika tabTarifRadiologi tidak memiliki value maka 
                    this.tabTarifRadiologi = _result.TabTarifRadiologi;
                }
                else {
                    //... Jika tabTarifRadiologi memiliki value maka tidak perlu diisi lagi
                }
            })
    }

    // ... On Get Tab Index Number when Click Tab Dynamic ...
    // ......................................................
    onGetTabTarifRadiologiIndex(index: any) {
        this.tabTarifRadiologiIndexNumber = index;
    }

    // ... Menampilkan Button per Kelompok / Tab Dynamic ...
    // .....................................................
    onShowTarifByButtonPerKelompok(tarif: any, index: any) {
        this.buttonTarifRadiologi = index;
        this.tarifPerKelompok = tarif;

        this.onRenderCheckbox(this.gridDataSourceOrderRad);
    }

    // ... On Change Checkbox Dynamic ...
    // ..................................
    onChangeTarif(args: any, tarif: any, indexTarif: any) {
        const IdCheckbox = args.target.id;
        const IsChecked = args.target.checked;

        this.currentTarifSelectedIndex = indexTarif;

        let DataTarifCheckbox = {
            "ExamCode": tarif.KodeTarif,
            "ExamName": tarif.NamaTarif,
            "Position": "",
            "PkFlag": "",
            "ReqNo": "",
            "SeqNo": 0
        };

        let DataUntukRenderCheckbox: any;

        if (IsChecked) {
            this.gridOrderRad.gridDataSource.push(DataTarifCheckbox);
            this.gridOrderRad.grid.refresh();

            DataUntukRenderCheckbox = this.gridOrderRad.gridDataSource;
        } else {
            let dataFilterized = this.gridDataSourceOrderRad.filter((filter: any) => {
                return filter["ExamCode"] != IdCheckbox;
            });

            this.gridDataSourceOrderRad = dataFilterized;
            this.gridOrderRad.grid.refresh();

            DataUntukRenderCheckbox = this.gridDataSourceOrderRad;
        }

        this.onRenderCheckbox(DataUntukRenderCheckbox);
    }

    // ... On Render Checkbox Dynamic ...
    // ..................................
    onRenderCheckbox(data: any) {
        if (data.length > 0) {
            for (let index in this.tarifPerKelompok) {
                this.tarifPerKelompok[index].checked = false;
                this.tarifPerKelompok[index].position = "";
                this.tarifPerKelompok[index].warna = "";

                data.filter((filterized: any) => {
                    if (this.tarifPerKelompok[index].KodeTarif == filterized.ExamCode) {
                        this.tarifPerKelompok[index].checked = true;
                        this.tarifPerKelompok[index].position = filterized.Position;
                        this.tarifPerKelompok[index].warna = filterized.PkFlag;
                    }
                });
            }
        } else {
            for (let index in this.tarifPerKelompok) {

                this.tarifPerKelompok[index].checked = false;

                data.filter((filterized: any) => {
                    if (this.tarifPerKelompok[index].KodeTarif == filterized.KodeTarif) {
                        this.tarifPerKelompok[index].checked = true;
                        this.tarifPerKelompok[index].position = "";
                        this.tarifPerKelompok[index].warna = "";
                    }
                });
            }
        }

    }

    // ... On Set Position and Warna for Checkbox Dynamic ... 
    // ......................................................
    onSetPositionAndWarna(kodeTarif: any) {
        let Left = (<HTMLInputElement>document.getElementById(kodeTarif + "L")).checked ? "L" : "";
        let Right = (<HTMLInputElement>document.getElementById(kodeTarif + "R")).checked ? "R" : "";
        let Polos = (<HTMLInputElement>document.getElementById(kodeTarif + "P")).checked ? "P" : "";
        let Kontras = (<HTMLInputElement>document.getElementById(kodeTarif + "K")).checked ? "K" : "";

        const Position = Left + Right;
        const Warna = Polos + Kontras;

        let grid = this.gridDataSourceOrderRad;

        if (grid.length > 0) {
            for (let index in grid) {
                if (grid[index]["ExamCode"] == kodeTarif) {
                    grid[index]["Position"] = Position;
                    grid[index]["PkFlag"] = Warna;
                };
            }

            this.gridOrderRad.grid.refresh();
        }
    }

    // ... On Open / Close Modal ...
    // .............................
    onOpenModal(template: TemplateRef<any>) {
        this.modalRef = this.bsModalService.show(
            template,
            Object.assign({}, {
                class: 'modal-lg'
            })
        );

        let id = template["_declarationTContainer"]["localNames"][0];

        if (id == "templateDialogPemeriksaanLainLain") {
            this.radiologiService.onGetGridColumn("Order/Radiologi/Pemeriksaan.json")
                .subscribe((_result) => {
                    this.gridColumnsPemeriksaan = _result;
                });

            this.laboratoriumService.onFetchTarifPemeriksaanLain("KodeTarifPemeriksaan");
        }
        else if (id == "templateDialogPilihDiagnosa") {
            this.radiologiService.onGetGridColumn("Order/Radiologi/Diagnosa.json")
                .subscribe((_result) => {
                    this.gridColumnsDiagnosa = _result;
                });

            this.onFetchGridDiagnosaDatasource();
        }
    }

    onCloseModal(form?: FormGroup) {
        this.modalRef.hide();

        form.reset();
    }

    // ... On Open Lookup ...
    // ......................
    onOpenLookup(id: string) {
        this.modalColumnsItemLookup = [];
        this.modalFiltersItemLookup = [];
        this.modalUrlLookup = "";

        if (id == "modalDiagnosa") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetDiagnosaByParams";
            this.onSetAttributeLookup("modalDiagnosa");
        } else if (id == "modalDokter") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetDokterByParams";
            this.onSetAttributeLookup("modalDokter");
        }
    }

    // ... On Set Attribute Lookup ...
    // ...............................
    onSetAttributeLookup(id: string) {
        let urlColumns: string = "";
        let urlFilters: string = "";

        if (id == "modalDiagnosa") {
            urlColumns = "Order/Laboratorium/Lookup/LookupDiagnosaAwal/columns.json";
            urlFilters = "Order/Laboratorium/Lookup/LookupDiagnosaAwal/filters.json";
        } else if (id == "modalDokter") {
            urlColumns = "AdmisiPasien/Lookup/LookupDokter/columns.json";
            urlFilters = "AdmisiPasien/Lookup/LookupDokter/filter.json";
        } else {

        }

        this.laboratoriumService.onGetLookupColumns(urlColumns)
            .subscribe((_result) => {
                this.modalColumnsItemLookup = _result;
            });

        this.laboratoriumService.onGetLookupFilters(urlFilters)
            .subscribe((_result) => {
                this.modalFiltersItemLookup = _result;
            });
    }

    // ... On Get Selected Data Diagnosa from Lookup ...
    // .................................................
    onGetSelectedDiagnosa(args: any) {
        this.DiagnosaIcdAwal.setValue(args.IcdCode);
        (<HTMLInputElement>document.getElementById("NamaDiagnosaAwal")).value = args.IcdDescr;
    }

    // ... On Set Form Input Diagnosa Properties ...
    // .............................................
    onSetFormInputDiagnosaProperties() {
        this.formInputDiagnosa = this.formBuilder.group({
            "Mrno": ["", []],
            "NoRegister": [0, []],
            "TglMasukRawat": ["", []],
            "PatName": ["", []],
            "PatType": ["OUT", []],
            "ReqPoly": ["", []],
            "KodeDokter": ["", []],
            "DiagnosaIcdAwal": ["", []],
            "SoapSubjective": ["", []],
            "SoapObjective": ["", []],
            "SoapAssesment": ["", []],
            "Catatan": ["", []],
            "SoapPlan": ["", []],
        });
    }

    // ... On Submit Form Input Diagnosa  ...
    // ......................................
    onSubmitFormInputDiagnosa(formValue: any) {
        formValue.Mrno = this.dataPasien.MrNo;
        formValue.NoRegister = this.dataPasien.NoRegister;
        formValue.TglMasukRawat = this.dataPasien.TglMasukRawat;
        formValue.PatName = this.dataPasien.NamaPasien;
        formValue.PatType = "OUT";
        formValue.ReqPoly = this.dataPasien.PolyCode;
        formValue.KodeDokter = this.kodeDokter;
        formValue.Catatan = this.SoapAssesment.value;

        this.radiologiService.onSubmitForm(
            "/TransaksiOrderRadiologi/InsertDiagnosa",
            formValue,
        ).subscribe((_result) => {
            let NamaDiagnosaAwal = (<HTMLInputElement>document.getElementById("NamaDiagnosaAwal")).value;
            let SoapAssesment = formValue.SoapAssesment;

            Swal.fire({
                title: 'Data Berhasil Disimpan',
                icon: "success",
            }).then(() => {
                this.Note.setValue("(" + NamaDiagnosaAwal + "). " + SoapAssesment);
            }).then(() => {
                this.onCloseModal(this.formInputDiagnosa);
            });
        })
    }

    // ... Grid Pilih Diagnosa Setting ...
    // ...................................
    onInitializedGridDiagnosa(component: SharedGridComponent) {
        this.gridDiagnosa = component;
    }

    // ... On Fetch Grid Pilih Diagnosa Data source ...
    // ................................................
    onFetchGridDiagnosaDatasource() {
        let data = [{
            "columnName": "NoRegister",
            "filter": "equal",
            "searchText": this.noRegister,
            "searchText2": ""
        }];

        this.laboratoriumService.onFetchGridDataSource(
            "/TransaksiOrderRadiologi/GetDiagnosaPenyakit",
            data
        ).subscribe((_result) => {
            setTimeout(() => {
                this.gridDataSourceDiagnosa = _result;
            }, 2100);
        })
    }

    // ... On Select Data Grid Pilih Diagnosa ...
    // ..........................................
    onSelectingGridDiagnosa(args: any) {
        console.log(args);
        this.gridDiagnosaRowSelected = args.rowIndexes;
    }

    // ... Ketika menyimpan Grid Pilih Diagnosa ...
    // ............................................
    onSubmitGridDiagnosa(gridDiagnosa: SharedGridComponent) {
        let data = gridDiagnosa.gridDataSource;

        let rowIndexes = this.gridDiagnosaRowSelected;

        let keteranganKlinik: string = "";

        if (rowIndexes.length > 0) {
            for (let index in rowIndexes) {
                keteranganKlinik += "(" + data[index]["DiagnosaIcdAwalDescr"] + "). " + data[index]["Catatan"] + "\n";
            }
        };

        Swal.fire({
            title: 'Data Berhasil Disimpan',
            icon: "success",
        }).then(() => {
            this.Note.setValue(keteranganKlinik);
        }).then(() => {
            this.modalRef.hide();
        });
    }


    // ... On Set Form Order Radiologi Property ...
    // ............................................
    onSetFormOrderRadiologiProperties() {
        this.formOrderRadiologi = this.formBuilder.group({
            "Mrno": ["", []],
            "NoRegister": [0, []],
            "TglMasukRawat": ["", []],
            "NamaPasien": ["", []],
            "ReqPoly": ["", []],
            "Note": ["", []],
            "NoteSample": ["", []],
            "KodeDokter": ["", []],
        })
    }

    // ... On Get Data Dokter after Selected in Lookup ...
    // ...................................................
    onGetSelectedDokterPeminta(args: any) {
        (<HTMLInputElement>document.getElementById("NamaDokter")).value = args.FirstName;
        this.KodeDokter.setValue(args.KodeDokter);
    }


    // ... Grid Pemeriksaan Action ...
    // ...............................
    onInitializedGridPemeriksaan(component: SharedGridComponent) {
        this.gridPemeriksaan = component;
    }

    onAddPemeriksaanLainLain() {
        let KodeTarifPemeriksaan = (<HTMLInputElement>document.getElementById("KodeTarifPemeriksaan")).value;
        let NamaPemeriksaanLain = (<HTMLInputElement>document.getElementById("NamaPemeriksaanLain")).value;
        let CatatanPemeriksaanLain = (<HTMLInputElement>document.getElementById("CatatanPemeriksaanLain")).value;

        const DataPemeriksaanLain: Object = {
            "ExamCode": KodeTarifPemeriksaan,
            "ExamName": NamaPemeriksaanLain,
            "ClinicalDetail": CatatanPemeriksaanLain,
            "Position": "",
            "PkFlag": "",
            "ReqNo": "",
            "OmSeqNo": "",
        };

        if (NamaPemeriksaanLain != "") {
            this.gridPemeriksaan.gridDataSource.push(DataPemeriksaanLain);
            this.gridPemeriksaan.grid.refresh();

            setTimeout(() => {
                this.onResetFormDialogPemeriksaan();
            }, 100);
        }
        else {
            Swal.fire(
                'Oops...',
                'Nama Pemeriksaan Tidak Boleh Kosong',
                'error'
            );
        }
    }

    onResetFormDialogPemeriksaan() {
        this.laboratoriumService.onFetchTarifPemeriksaanLain("KodeTarifPemeriksaan");

        (<HTMLInputElement>document.getElementById("NamaPemeriksaanLain")).value = "";
        (<HTMLInputElement>document.getElementById("CatatanPemeriksaanLain")).value = "";
    }

    onSubmitDialogPemeriksaanLainLain() {
        for (let index in this.gridPemeriksaan.gridDataSource) {
            this.gridOrderRad.gridDataSource.push(this.gridPemeriksaan.gridDataSource[index]);
        }

        this.gridOrderRad.grid.refresh();

        setTimeout(() => {
            this.onCloseDialogPemeriksaanLain();
        }, 200);
    }

    onCloseDialogPemeriksaanLain() {
        this.modalRef.hide();

        this.gridDataSourcePemeriksaan = [];
        this.gridPemeriksaan.grid.refresh();

        this.onResetFormDialogPemeriksaan();
    }


    // ... Grid Order Radiologi Setting ...
    // ....................................
    onInitializedGridOrderRad(component: SharedGridComponent) {
        this.gridOrderRad = component;
    }

    onSubmitFormOrderRadiologi(formValue: any) {
        formValue.Mrno = this.dataPasien.MrNo;
        formValue.NoRegister = parseInt(this.dataPasien.NoRegister);
        formValue.TglMasukRawat = this.dataPasien.TglMasukRawat;
        formValue.NamaPasien = this.dataPasien.NamaPasien;
        formValue.ReqPoly = this.dataPasien.PolyCode;
        formValue.Details = this.gridOrderRad.gridDataSource;

        this.radiologiService.onSubmitForm(
            "/TransaksiOrderRadiologi/InsertUpdate",
            formValue,
        ).subscribe((_result) => {
            console.log(_result);

            Swal.fire({
                title: 'Data Berhasil Disimpan',
                icon: "success",
            }).then(() => {
                location.reload();
            });
        })
    }

    get DiagnosaIcdAwal() { return this.formInputDiagnosa.get("DiagnosaIcdAwal") }
    get SoapSubjective() { return this.formInputDiagnosa.get("SoapSubjective") }
    get SoapObjective() { return this.formInputDiagnosa.get("SoapObjective") }
    get SoapAssesment() { return this.formInputDiagnosa.get("SoapAssesment") }
    get Catatan() { return this.formInputDiagnosa.get("Catatan") }
    get SoapPlan() { return this.formInputDiagnosa.get("SoapPlan") }

    get Note() { return this.formOrderRadiologi.get("Note") }
    get NoteSample() { return this.formOrderRadiologi.get("NoteSample") }
    get KodeDokter() { return this.formOrderRadiologi.get("KodeDokter") }
}
