import { AfterViewInit, Component, HostListener, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ComboBoxComponent } from '@syncfusion/ej2-angular-dropdowns';
import { MenuItemModel } from '@syncfusion/ej2-angular-navigations';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { SharedGridComponent } from 'src/app/components/shared-grid/shared-grid.component';
import { StorageEncryptService } from 'src/app/service/authentication/storage-encrypt.service';
import { LaboratoriumService } from 'src/app/service/page/laboratorium.service';
import { ResepDigitalService } from 'src/app/service/page/resep-digital.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-resep-digital',
    templateUrl: './resep-digital.component.html',
    styleUrls: ['./resep-digital.component.css']
})
export class ResepDigitalComponent implements OnInit, AfterViewInit {

    @ViewChild("staticTabs", { static: false }) public staticTabs: TabsetComponent;
    tabHeader: any = [];

    // ... Card Header Attribute
    dataPasien: any;
    namaPasien: string = "";
    noRegister: string = "";
    namaDokter: string = "";
    kodeDokter: string = "";
    mrNo: string = "";
    sex: string = "";
    tglLahir: string = "";
    ppjp: string = "";
    umur: string = "";
    tglMasukRawat: string = "";
    debiturName: string = "";

    // ... Form Group Input Resep
    formInputResep: FormGroup;

    // ... Drug Name Autocomplete Dropdown Properties
    drugNameFields: any = { value: 'NamaObatLengkap' };
    drugNameDatasource: any = [];
    drugNameSelectedData: any = [];

    // ... Drug Delivery Dropdown Properties drugDeliveryFields
    @ViewChild("DrugDeliveryCombobox") public DrugDeliveryCombobox: ComboBoxComponent;
    drugDeliveryFields: any = { text: 'text', value: 'value' };
    public drugDeliveryDatasource: { [key: string]: Object }[] = [
        { text: 'Oral', value: 'Oral' },
        { text: 'Rectal', value: 'Rectal' },
        { text: 'Intravena', value: 'Intravena' },
        { text: 'Intramoskuler', value: 'Intramoskuler' },
        { text: 'etc', value: 'etc' }
    ];

    // ... Period Dropdown Properties periodFields
    @ViewChild("PeriodCombobox") public PeriodCombobox: ComboBoxComponent;
    periodFields: any = { text: 'text', value: 'value' };
    public periodDatasource: { [key: string]: Object }[] = [
        { text: 'Every 6 hour', value: 'Every 6 hour' },
        { text: 'Every 8 hour', value: 'Every 8 hour' },
        { text: 'Every 12 hour', value: 'Every 12 hour' },
        { text: 'Every 24 hour', value: 'Every 24 hour' }
    ];

    // ... Period Moment Dropdown Properties periodFields
    @ViewChild("PeriodMomentCombobox") public PeriodMomentCombobox: ComboBoxComponent;
    periodMomentFields: any = { text: 'text', value: 'value' };
    public periodMomentDatasource: { [key: string]: Object }[] = [
        { text: 'Free', value: 'Free' },
        { text: 'Empty Stomatch', value: 'Empty Stomatch' },
        { text: 'Before Food', value: 'Before Food' },
        { text: 'After Food', value: 'After Food' }
    ];

    // ... Modal Ref 
    modalRef: BsModalRef;

    // ... Grid Input Resep
    gridInputResep: SharedGridComponent = null;
    gridInputResepDataSource: any = [];
    gridInputResepColumns: any = [];
    gridContextMenuItems: MenuItemModel[] = [
        {
            id: 'hapus',
            text: 'Hapus Obat',
            iconCss: 'fas fa-trash'
        }
    ];

    gridInputResepSelectedRowIndex: any;

    // ... Form Input Diagnosa
    formInputDiagnosa: FormGroup;

    @ViewChild("templateDialogInputDiagnosa") public templateDialogInputDiagnosa: TemplateRef<any>

    // ... Modal Lookup Diagnosa Attribute
    modalButtonLookup: string = "Cari";
    modalColumnsItemLookup: any = [];
    modalFiltersItemLookup: any = [];
    modalUrlLookup: string = "";

    // ... Grid Daftar Resep Attribute
    gridDaftarResep: SharedGridComponent = null;
    gridDataSourceDaftarResep: any = [];
    gridColumnsDaftarResep: any = [];

    // ... Grid Detail Resep Attribute
    gridDetailResep: SharedGridComponent = null;
    gridDataSourceDetailResep: any = [];
    gridColumnsDetailResep: any = [];

    constructor(
        private storageEncryptService: StorageEncryptService,
        private laboratoriumService: LaboratoriumService,
        private resepDigitalService: ResepDigitalService,
        private activatedRoute: ActivatedRoute,
        private bsModalService: BsModalService,
        private formBuilder: FormBuilder,
    ) {
        // ... Panggil function untuk set form Input Resep Attribute
        this.onSetFormInputResep();

        // ... Panggil function untuk set form Input Diagnosa Attribute
        this.onSetFormInputDiagnosa();
    }

    ngOnInit(): void {
        // ... Panggil function set tab header
        this.onSetTabHeader();

        // ... Panggil function fetch data pasien from route
        this.onFetchDataPasienFromRoute();
    }

    ngAfterViewInit(): void {
        // ... Panggil function untuk set Drug Name Autocomplete Datasource
        this.onFetchDrugNameDatasource();

        // ... Panggil function untuk set Grid Input Resep Properties
        this.onSetGridInputResepProperties();

        // ... Function untuk mendeteksi apakah fullscreen atau tidak
        window.onresize = () => {
            if (window.matchMedia('(display-mode: fullscreen)').matches ||
                window.document.fullscreenElement) {
            } else {
                this.gridInputResep.grid.height = "150px";
            }
        }
    }

    // ... Tab Header Setting ... 
    // ..........................
    onSetTabHeader() {
        this.tabHeader = [
            {
                id: "inputResep",
                iconCss: "fas fa-plus fa-sm",
                text: "Input Resep"
            },
            {
                id: "prescriptionHistory",
                iconCss: "fas fa-table fa-sm",
                text: "Prescription History"
            },
        ]
    }

    // ... Dapatkan Data Pasien from Route ...
    // .......................................
    onFetchDataPasienFromRoute() {
        const dataPasienFromRoute = JSON.parse(
            this.storageEncryptService.decrypt(
                this.activatedRoute.snapshot.params["data"]
            ));

        this.dataPasien = dataPasienFromRoute;
        this.namaPasien = dataPasienFromRoute.NamaPasien;
        this.noRegister = dataPasienFromRoute.NoRegister;
        this.kodeDokter = dataPasienFromRoute.KodeDokter;
        this.namaDokter = dataPasienFromRoute.NamaDokter;
        this.mrNo = dataPasienFromRoute.MrNo;
        this.sex = dataPasienFromRoute.Sex;
        this.tglLahir = dataPasienFromRoute.TanggalLahir;
        this.umur = dataPasienFromRoute.Umur;
        this.tglMasukRawat = dataPasienFromRoute.TglMasukRawat;
        this.debiturName = dataPasienFromRoute.DebiturName;
    }

    // ... Form Input Resep Setting ...
    // ................................
    onSetFormInputResep() {
        this.formInputResep = this.formBuilder.group({
            "FormulariumId": [0, []],
            "SediaanId": ["", []],
            "KategoriFormulariumId": [0, []],
            "DrugName": ["", []],
            "DrugDelivery": ["", []],
            "Period": ["", []],
            "PeriodMoment": ["", []],
            "Dispense": ["", []],
        });
    }

    // ... Fetch Drug Name Autocomplete DataSource ...
    // ...............................................
    onFetchDrugNameDatasource() {
        this.resepDigitalService.onFetchDrugNameAutocompleteDatasource()
            .subscribe((_result) => {
                this.drugNameDatasource = _result;
            });
    }

    // ... On Open Dialog Modal Detail Obat ...
    // ........................................
    onOpenDetailDialogObat(template: TemplateRef<any>) {
        console.log(this.drugNameSelectedData);

        if (this.drugNameSelectedData != null) {
            this.modalRef = this.bsModalService.show(template,
                Object.assign({}, {
                    class: 'modal-lg'
                })
            );

            (<HTMLInputElement>document.getElementById("NamaObat")).value = this.drugNameSelectedData.NamaObatLengkap;
            (<HTMLInputElement>document.getElementById("Catatan")).value = this.drugNameSelectedData.Catatan;
            (<HTMLInputElement>document.getElementById("Therapy")).value = this.drugNameSelectedData.TherapyDesc;
        } else {
            Swal.fire({
                title: 'Obat Belum Dipilih',
                icon: "error",
            });
        }
    }

    // ... On Change Drug Name Autocomplete Dropdown ...
    // ........................................
    onChangeDropdownDrugName(args: any) {
        let data = args.itemData;

        if (data != null) {
            this.drugNameSelectedData = data;

            this.FormulariumId.setValue(data["FormulariumId"]);
            this.SediaanId.setValue(data["SediaanId"]);
            this.KategoriFormulariumId.setValue(data["KategoriFormulariumId"]);
        }
    }

    // ... On Change Drug Delivery Dropdown ...
    // ........................................
    onChangeDropdownDrugDelivery(args: any) {
        let value = args.itemData.value;

        if (value != null) {
            this.DrugDelivery.setValue(value);
        }
    }

    // ... On Change Period Dropdown ...
    // .................................
    onChangeDropdownPeriod(args: any) {
        let value = args.itemData.value;

        if (value != null) {
            this.Period.setValue(value);
        }
    }

    // ... On Change Period Moment Dropdown ...
    // ........................................
    onChangeDropdownPeriodMoment(args: any) {
        let value = args.itemData.value;

        if (value != null) {
            this.PeriodMoment.setValue(value);
        }
    }

    // ... On Set Grid Input Resep Columns Properties ...
    // ..................................................
    onSetGridInputResepProperties() {
        this.resepDigitalService.onFetchGridInputResepProperties()
            .subscribe((_result) => {
                this.gridInputResepColumns = _result;
            });

        this.resepDigitalService.onFetchGridDaftarResepProperties()
            .subscribe((_result) => {
                this.gridColumnsDaftarResep = _result;
            });

        this.resepDigitalService.onFetchGridDetailResepProperties()
            .subscribe((_result) => {
                this.gridColumnsDetailResep = _result;
            });
    }

    // ... On Set Grid Input Resep Columns Properties ...
    // ..................................................
    onAddDataToGridInputResep(value: any) {
        value.NoRegister = this.noRegister;
        value.DrugDelivery = value.DrugDelivery + " : " + value.Period + " - " + value.PeriodMoment;

        this.resepDigitalService.onAddDataToGridInputResep(value)
            .subscribe((_result) => {
                if (_result.length > 0) {
                    this.gridInputResep.gridDataSource.push(value);
                    this.gridInputResep.grid.refresh();

                    //... Panggil function untuk Clear Input Resep
                    this.onClearDataInputResep();
                } else {
                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: 'btn btn-success ml-2',
                            cancelButton: 'btn btn-danger'
                        },
                        buttonsStyling: false
                    });

                    swalWithBootstrapButtons.fire({
                        title: 'Diagnosa Pasien Masih Kosong',
                        text: "Apakah Anda Ingin Menginputkan Diagnosa?",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonText: 'Ya, Input Diagnosa',
                        cancelButtonText: 'Tidak, Batalkan',
                        reverseButtons: true,
                        focusConfirm: true,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            this.onOpenDialogInputDiagnosa(this.templateDialogInputDiagnosa)
                        } else if (
                            /* Read more about handling dismissals below */
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                            swalWithBootstrapButtons.fire(
                                'Dibatalkan',
                                'Anda Membatalkan Input Resep',
                                'error'
                            )
                        }
                    })
                }
            });
    }

    // ... On Clear Grid Input Resep ...
    // .................................
    onClearDataInputResep() {
        this.DrugDeliveryCombobox.value = "";
        this.PeriodCombobox.value = "";
        this.PeriodMomentCombobox.value = "";

        this.formInputResep.reset();
    }

    // ... On Init Grid Input Resep ...
    // ................................
    onInitializedGridInputResep(component: SharedGridComponent) {
        this.gridInputResep = component;
    }

    onToolbarGridInputResepClick(args: any) {
        args.cancel = true;

        let id = args.item.id;

        if (id == "GridInputResep_delete") {
            let grid: [] = this.gridInputResep.gridDataSource;
            let gridIndex = this.gridInputResepSelectedRowIndex;

            grid.splice(gridIndex, 1);

            this.gridInputResep.grid.refresh();
        }
    }

    // ... On Select Row Grid Input Resep ...
    // ......................................
    onSelectingGridInputResep(args: any) {
        this.gridInputResepSelectedRowIndex = args.rowIndex;
    }

    // ... On Select Context Menu Grid Input Resep ...
    // ...............................................
    onSelectGridContextMenu(args: any) {
        let id = args.item.id;

        if (id == "hapus") {
            let grid: [] = this.gridInputResep.gridDataSource;
            let gridIndex = this.gridInputResepSelectedRowIndex;

            grid.splice(gridIndex, 1);

            this.gridInputResep.grid.refresh();
        }
    }

    // ... On Open Dialog Modal Detail Obat ...
    // ........................................
    onOpenDialogInputDiagnosa(template: TemplateRef<any>) {
        this.modalRef = this.bsModalService.show(template,
            Object.assign({}, {
                class: 'modal-lg'
            })
        );
    }

    // ... Form Input Diagnosa Setting ...
    // ...................................
    onSetFormInputDiagnosa() {
        this.formInputDiagnosa = this.formBuilder.group({
            "Mrno": ["", []],
            "NoRegister": [0, []],
            "TglMasukRawat": ["", []],
            "PatName": ["", []],
            "PatType": ["OUT", []],
            "ReqPoly": ["", []],
            "KodeDokter": ["", []],
            "DiagnosaIcdAwal": ["", []],
            "SoapSubjective": ["", []],
            "SoapObjective": ["", []],
            "SoapAssesment": ["", []],
            "Catatan": ["", []],
            "SoapPlan": ["", []],
        });
    }

    // ... On Open Lookup Diagnosa ...
    // ..............................
    onOpenLookup(id: string) {
        this.modalColumnsItemLookup = [];
        this.modalFiltersItemLookup = [];
        this.modalUrlLookup = "";

        if (id == "modalDiagnosa") {
            this.modalUrlLookup = "/SetupAdmisiPasienIRJA/GetDiagnosaByParams";

            let urlColumns = "Order/Laboratorium/Lookup/LookupDiagnosaAwal/columns.json";
            let urlFilters = "Order/Laboratorium/Lookup/LookupDiagnosaAwal/filters.json";

            this.laboratoriumService.onGetLookupColumns(urlColumns)
                .subscribe((_result) => {
                    this.modalColumnsItemLookup = _result;
                });

            this.laboratoriumService.onGetLookupFilters(urlFilters)
                .subscribe((_result) => {
                    this.modalFiltersItemLookup = _result;
                });
        }
    }

    // ... On Get Selected Diagnosa ...
    // ................................
    onGetSelectedDiagnosa(args: any) {
        this.DiagnosaIcdAwal.setValue(args.IcdCode);
        (<HTMLInputElement>document.getElementById("NamaDiagnosaAwal")).value = args.IcdDescr;
    }

    // ... On Submit Form Input Diagnosa ...
    // .....................................
    onSubmitFormInputDiagnosa(formValue: any) {
        formValue.Mrno = this.dataPasien.MrNo;
        formValue.NoRegister = this.dataPasien.NoRegister;
        formValue.TglMasukRawat = this.dataPasien.TglMasukRawat;
        formValue.PatName = this.dataPasien.NamaPasien;
        formValue.PatType = "OUT";
        formValue.ReqPoly = this.dataPasien.PolyCode;
        formValue.KodeDokter = this.kodeDokter;
        formValue.Catatan = this.SoapAssesment.value;

        this.laboratoriumService.onSubmitForm(
            "/TransaksiOrderLaboratorium/InsertDiagnosa",
            formValue,
        ).subscribe((_result) => {
            Swal.fire({
                title: 'Data Berhasil Disimpan',
                icon: "success",
            }).then(() => {
                this.modalRef.hide();
            });
        })
    }

    // ... On Converting Grid Input Resep Data ...
    // ...........................................
    onConvertingGridInputResepData() {
        let Detail = this.gridInputResep.gridDataSource;

        for (let index in Detail) {
            Detail[index]["NamaObat"] = Detail[index]["DrugName"];
            Detail[index]["LabelDesc"] = Detail[index]["DrugDelivery"];
            Detail[index]["Qty"] = Detail[index]["Dispense"];
            Detail[index]["Keterangan"] = "-";
            Detail[index]["FlagFormularium"] = true;
            Detail[index]["BerapaKaliPerHari"] = 0;
            Detail[index]["BerapaHari"] = 0;
        }

        this.onSubmitFormInputResep(Detail)
    }

    // ... On Submit Form Input Resep ...
    // ..................................
    onSubmitFormInputResep(dataDetail: any) {
        let Resep = {
            "JenisPasien": "OUT",
            "MrNo": this.mrNo,
            "NoRegister": this.noRegister,
            "KodeDokterEntry": this.kodeDokter,
            "StatusResep": "PENDING",
            "UserEntry": 0,
            "Keterangan": "-",
            "Details": dataDetail
        };

        this.resepDigitalService.onSubmitFormInputResep(Resep)
            .subscribe((_result) => {
                Swal.fire({
                    title: 'Data Berhasil Disimpan',
                    icon: "success",
                }).then(() => {
                    this.staticTabs.tabs[1].active = true;

                    setTimeout(() => {
                        this.onGetHistoryResepDigital();
                    }, 500);
                });
            })
    }

    // ... On Get History Resep ...
    // ............................
    onGetHistoryResepDigital() {
        this.resepDigitalService.onFetchHistoryResepDigital(this.dataPasien)
            .subscribe((_result) => {
                setTimeout(() => {
                    this.gridDataSourceDaftarResep = _result;
                }, 2100);
            })
    }

    // ... On Get History Resep ...
    // ............................
    onGetDetailResep(args: any) {
        let data = args.data;

        this.resepDigitalService.onFetchDetailHistoryResepDigital(data)
            .subscribe((_result) => {
                this.gridDataSourceDetailResep = _result;
            })
    }

    // ... On Init Grid Daftar Resep ...
    // .................................
    onInitializedGridDaftarResep(component: SharedGridComponent) {
        this.gridDaftarResep = component;
    }

    // ... On Init Grid Detail Resep ...
    // .................................
    onInitializedGridDetailResep(component: SharedGridComponent) {
        this.gridDetailResep = component;
    }

    onSetGridToFullscreen() {
        let elem = document.getElementById("gridInputResep");
        elem.requestFullscreen();

        this.gridInputResep.grid.height = "100%";
        this.gridInputResep.grid.width = "100%";
    }

    get FormulariumId() { return this.formInputResep.get("FormulariumId") }
    get SediaanId() { return this.formInputResep.get("SediaanId") }
    get KategoriFormulariumId() { return this.formInputResep.get("KategoriFormulariumId") }
    get DrugName() { return this.formInputResep.get("DrugName") }
    get DrugDelivery() { return this.formInputResep.get("DrugDelivery") }
    get Period() { return this.formInputResep.get("Period") }
    get PeriodMoment() { return this.formInputResep.get("PeriodMoment") }
    get Dispense() { return this.formInputResep.get("Dispense") }

    get DiagnosaIcdAwal() { return this.formInputDiagnosa.get("DiagnosaIcdAwal") }
    get SoapSubjective() { return this.formInputDiagnosa.get("SoapSubjective") }
    get SoapObjective() { return this.formInputDiagnosa.get("SoapObjective") }
    get SoapAssesment() { return this.formInputDiagnosa.get("SoapAssesment") }
    get Catatan() { return this.formInputDiagnosa.get("Catatan") }
    get SoapPlan() { return this.formInputDiagnosa.get("SoapPlan") }
}
