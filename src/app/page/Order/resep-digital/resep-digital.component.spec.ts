import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResepDigitalComponent } from './resep-digital.component';

describe('ResepDigitalComponent', () => {
  let component: ResepDigitalComponent;
  let fixture: ComponentFixture<ResepDigitalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResepDigitalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResepDigitalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
