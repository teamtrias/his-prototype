import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { CommandClickEventArgs, CommandColumnService, ContextMenuItem, EditSettingsModel, GridComponent, ToolbarItems } from '@syncfusion/ej2-angular-grids';
import { FirebaseCrudService } from 'src/app/service/firebase/firebase-crud.service';

@Component({
    selector: 'shared-grid',
    templateUrl: './shared-grid.component.html',
    styleUrls: ['./shared-grid.component.css'],
    providers: [CommandColumnService]
})
export class SharedGridComponent implements OnInit, AfterViewInit {

    @ViewChild("grid") public grid: GridComponent;

    @Input('grid-id') gridId: string;
    @Input('grid-width') gridWidth: any;
    @Input('grid-height') gridHeight: any;
    @Input('grid-lines') gridLines: string;
    @Input('grid-paging') gridPaging: any;
    @Input('grid-initial-page') gridInitialPage: any;
    @Input('grid-toolbar') toolbar: ToolbarItems[];
    @Input('grid-editTemplate') gridEditTemplate: Object;
    @Input('grid-editSettings') gridEditingSettings: EditSettingsModel = {};
    @Input('grid-DataSource') gridDataSource: any = [];
    @Input('grid-ContextMenuItems') gridContextMenuItems: ContextMenuItem[];
    @Input('grid-selectionSettings') gridSelectionSettings: any = { type: "Single", mode: "Row" };

    @Input('columns') items: any = [];
    @Input('urlGetColumns') urlGetColumns: string;

    @Output('row-selected') onRowSelected = new EventEmitter<any>();
    @Output('enter-pressed') onKeyPressed = new EventEmitter<any>();
    @Output('double-click') onDoubleClicked = new EventEmitter<any>();
    @Output('action-begin') onActionBegined = new EventEmitter<any>();
    @Output('action-complete') onActionCompleted = new EventEmitter<any>();
    @Output('toolbar-click') onToolbarClicked = new EventEmitter<any>();
    @Output('command-click') onCommandClicked = new EventEmitter<any>();
    @Output('initialized') initialized: EventEmitter<SharedGridComponent> = new EventEmitter<SharedGridComponent>();

    constructor(private firebaseCrudService: FirebaseCrudService) { }

    ngOnInit(): void {

        // .... Wajib di inisialisasi, jika tidak grid tidak bisa dynamic widthnya
        this.items = [
            { headerText: "NO ANTRIAN", width: 300, field: "NoAntrian", visible: false },
            { headerText: "NO ANTRIAN", width: 200, field: "JamAdmisi", visible: false },
            { headerText: "TGL ADMISI", width: 200, field: "TglAdmisi", visible: false },
            { headerText: "JAM MASUK", width: 200, field: "JamMasukRawat", visible: false },
            { headerText: "JAM MASUK", width: 200, field: "JamMasukRawat", visible: false },
            { headerText: "JAM MASUK", width: 200, field: "JamMasukRawat", visible: false },
            { headerText: "JAM MASUK", width: 200, field: "JamMasukRawat", visible: false },
            { headerText: "JAM MASUK", width: 200, field: "JamMasukRawat", visible: false },
            { headerText: "JAM MASUK", width: 200, field: "JamMasukRawat", visible: false },
        ];
    }

    ngAfterViewInit(): void {
        //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
        //Add 'implements AfterViewInit' to the class.

        // console.log(this.grid);

        this.initialized.emit(this);
    }

    onRowSelecting(args: any) {
        this.onRowSelected.emit(args);
    }

    onKeyPressing(args: any) {
        this.onKeyPressed.emit(args);
    }

    onDoubleClick(args: any) {
        this.onDoubleClicked.emit(args.rowData);
    }

    onActionBegin(args: any) {
        this.onActionBegined.emit(args);
    }

    onActionComplete(args: any) {
        this.onActionCompleted.emit(args);
    }

    onToolbarClick(args: any) {
        this.onToolbarClicked.emit(args);
    }

    onCommandClick(args: CommandClickEventArgs) {
        this.onCommandClicked.emit(args);
    }
}
