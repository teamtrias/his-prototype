import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'input-text-lookup',
    templateUrl: './input-field.component.html',
    styleUrls: ['./input-field.component.css']
})
export class InputFieldComponent implements OnInit {

    @Input('input-form-id') inputId: string;
    @Input('lookup-untuk-apa') inputName: string;

    @Output('on-open-lookup') onOpenLookup = new EventEmitter<any>();

    constructor() { }

    ngOnInit() { }

    onOpenRolesLookup(args: any) {
        this.onOpenLookup.emit(args)
    }
}
