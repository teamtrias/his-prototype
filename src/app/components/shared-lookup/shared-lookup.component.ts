import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { map } from 'rxjs/operators';
import { HttpRequestService } from 'src/app/service/http-request/http-request.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-shared-lookup',
    templateUrl: './shared-lookup.component.html',
    styleUrls: ['./shared-lookup.component.css'],
    providers: [BsModalService]
})
export class SharedLookupComponent implements OnInit {

    modalRef: BsModalRef;

    @Input('modal-id') modalId: string;
    @Input('modal-title') modalTitle: string;
    @Input('modal-button') modalButton: string;
    @Input('column-items') columnItems: any;
    @Input('filter-lookup') filters: any;
    @Input('lookup-url') lookupUrl: string;
    @Input('static-filter') staticFilters: any = [];
    @Input('hide-button') isButtonHidden: boolean = false;
    @Input('button-id') buttonLookup: string;

    items: any;

    gridId: string = 'GridModal';
    gridWidth: any = 'auto';
    gridHeight: any = 300;
    gridDataSource: any;
    gridLines: string = 'Both';
    gridPaging: boolean = true;
    gridPageSettings: Object;
    searchValueId: string;
    currentFilters: any;

    currentData: any;
    @Output('onGetSelectedData') onGetSelectedData = new EventEmitter<any>();
    @Output('onOpenModal') openModal = new EventEmitter<any>();

    @Input("SelectedInputId") SelectedInputId: string;

    constructor(private modalService: BsModalService, private HttpRequestService: HttpRequestService) { }

    ngOnInit(): void {
        this.gridPageSettings = { pageSizes: true, pageCount: 4, pageSize: 11 };
    }

    onOpenModal(template: TemplateRef<any>, id: any) {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'modal-lg' })
        );

        this.openModal.emit(id);
    }

    onCloseModal() {
        this.modalRef.hide();

        setTimeout(() => {
            this.gridDataSource = [];
        }, 200);
    }

    onFetchDataSource(params: any) {
        this.HttpRequestService.defaultPostRequest(this.lookupUrl, params, true)
            .subscribe(
                (_result) => {
                    // console.log(_result);
                    setTimeout(() => {
                        this.gridDataSource = _result;
                    }, 2100);
                }, (pesanError) => {
                    console.log(pesanError);
                }
            )
    }

    onChangeFilters(args: any) {
        this.currentFilters = args;
    }

    onSearchLookup(value: string) {
        let columnName: string;
        let filter: string;

        if (this.currentFilters) {
            columnName = this.currentFilters.field;
            filter = this.currentFilters.filter;
        } else {
            columnName = "";
            filter = "";
        }

        let search = [{
            "columnName": columnName,
            "filter": filter,
            "searchText": value,
            "searchText2": ""
        }];

        if (this.staticFilters.length > 0) {
            for (let i = 0; i < this.staticFilters.length; i++) {
                search.push(this.staticFilters[i]);
            }
        } else {
            //... do nothing
        }

        this.onFetchDataSource(search);
    }

    onRowSelected(args: any) {
        this.currentData = args.data;
    }

    onKeyPressed(args: any) {
        let keycode = args.keyCode;

        if (keycode == 13) {
            args.cancel = true;
            this.onKeyPressedUtility(this.currentData);
        }
    }

    onKeyPressedUtility(data: any) {
        (<HTMLInputElement>document.getElementById(this.SelectedInputId)).value = data[this.SelectedInputId];

        this.onGetSelectedData.emit(data);
        this.onCloseModal();
    }

    onDoubleClicked(args: any) {
        this.onKeyPressedUtility(args);
    }

    ngOnDestroy() {
        this.gridDataSource = [];
    }
}
