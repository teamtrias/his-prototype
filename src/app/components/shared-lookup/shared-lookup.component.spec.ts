import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedLookupComponent } from './shared-lookup.component';

describe('SharedLookupComponent', () => {
  let component: SharedLookupComponent;
  let fixture: ComponentFixture<SharedLookupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SharedLookupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
