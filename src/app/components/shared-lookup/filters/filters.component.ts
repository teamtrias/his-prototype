import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'shared-filters',
    templateUrl: './filters.component.html',
    styleUrls: ['./filters.component.css']
})
export class FiltersComponent implements OnInit {

    @Input('filters') filters: any;

    @Output('onChangeFilters') onChangeFilters = new EventEmitter<any>();

    constructor() { }

    ngOnInit(): void {
        // console.log(this.filters);
    }

    onFilterChanged(checkbox: any) {
        this.onChangeFilters.emit(checkbox);
    }
}
