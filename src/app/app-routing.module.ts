import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AuthGuard } from './helper/guard/auth.guard';
import { AuthComponent } from './page/auth/auth.component';
import { BillingComponent } from './page/billing/billing.component';
import { DashboardComponent } from './page/dashboard/dashboard.component';
import { HomeComponent } from './page/dashboard/home/home.component';
import { BrowsePenjualanObatComponent } from './page/Farmasi/browse-penjualan-obat/browse-penjualan-obat.component';
import { PenjualanObatComponent } from './page/Farmasi/penjualan-obat/penjualan-obat.component';
import { AdmisiPasienComponent } from './page/IRJA/admisi-pasien/admisi-pasien.component';
import { AntrianPasienComponent } from './page/IRJA/antrian-pasien/antrian-pasien.component';
import { DataPasienComponent } from './page/IRNA/data-pasien/data-pasien.component';
import { SetupPasienComponent } from './page/IRNA/setup-pasien/setup-pasien.component';
import { LaboratoriumComponent } from './page/Order/laboratorium/laboratorium.component';
import { RadiologiComponent } from './page/Order/radiologi/radiologi.component';
import { ResepDigitalComponent } from './page/Order/resep-digital/resep-digital.component';
import { SetupHargaTarifComponent } from './page/setup-data/setup-harga-tarif/setup-harga-tarif.component';
import { SetupTarifComponent } from './page/setup-data/setup-tarif/setup-tarif.component';

const routes: Routes = [
    { path: '', component: AuthComponent, data: { title: 'Login' } },
    {
        path: 'dashboard', component: DashboardComponent, data: { title: 'Home' },
        canActivate: [AuthGuard],
        children: [
            { path: 'home', component: HomeComponent, data: { title: 'Welcome' } },
            { path: 'setup-tarif', component: SetupTarifComponent, data: { title: 'Setup Tarif' } },
            { path: 'setup-harga-tarif', component: SetupHargaTarifComponent, data: { title: 'Setup Harga Tarif' } },
            { path: 'irna/data-pasien', component: DataPasienComponent, data: { title: 'Data Pasien' } },
            { path: 'irna/pasien', component: SetupPasienComponent, data: { title: 'Setup Pasien' } },
            { path: 'irja/admisi-pasien', component: AdmisiPasienComponent, data: { title: 'Admisi Pasien' } },
            { path: 'irja/antrian', component: AntrianPasienComponent, data: { title: 'Antrian Pasien' } },
            { path: 'order/laboratorium/', component: LaboratoriumComponent, data: { title: 'Order Laboratorium' } },
            { path: 'order/laboratorium/:data/:key', component: LaboratoriumComponent, data: { title: 'Order Laboratorium' } },
            { path: 'order/radiologi/', component: RadiologiComponent, data: { title: 'Order Radiologi' } },
            { path: 'order/radiologi/:data/:key', component: RadiologiComponent, data: { title: 'Order Radiologi' } },
            { path: 'order/resep-digital/', component: ResepDigitalComponent, data: { title: 'Order Resep' } },
            { path: 'order/resep-digital/:data/:key', component: ResepDigitalComponent, data: { title: 'Order Resep' } },
            { path: 'pharmacy/penjualan-obat-register', component: PenjualanObatComponent, data: { title: 'Penjualan Obat' } },
            { path: 'pharmacy/browse-penjualan-obat', component: BrowsePenjualanObatComponent, data: { title: 'Browse Penjualan Obat' } },
            { path: 'billing', component: BillingComponent, data: { title: 'Billing' } },
        ]
    },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }

