import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { SessionCheckService } from './service/authentication/session-check.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'siadmin';

    constructor(private router: Router,
        private activatedRoute: ActivatedRoute,
        private titleService: Title,
        private sessionCheckService: SessionCheckService) { }

    ngOnInit() {
        this.router.events.pipe(
            filter(event => event instanceof NavigationEnd),
        ).subscribe(() => {
            const rt = this.getChild(this.activatedRoute);
            rt.data.subscribe((data: any) => {
                localStorage.setItem('pageTitle', data.title);
                this.titleService.setTitle(data.title);
            })
        });

        this.onCekSession();
    }

    onCekSession() {
        let prefix: string = "UserData";

        this.sessionCheckService.onCekSession(prefix)
            .subscribe((session) => {
                console.log('Cek Session :', session);
            })
    }

    getChild(activatedRoute: ActivatedRoute) {
        if (activatedRoute.firstChild) {
            return this.getChild(activatedRoute.firstChild);
        } else {
            return activatedRoute;
        }
    }
}
