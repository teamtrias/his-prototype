import { HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import { exhaustMap, take } from "rxjs/operators";
import { AuthService } from "src/app/service/authentication/auth.service";
import { StorageEncryptService } from "src/app/service/authentication/storage-encrypt.service";
import { environment } from "src/environments/environment";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private storageEncryptService: StorageEncryptService,
        private cookieService: CookieService,
        private authService: AuthService) { }

    intercept(httpRequest: HttpRequest<any>, next: HttpHandler) {
        // ... Kemudian decrypt hasil get
        const token = this.storageEncryptService.decrypt(this.cookieService.get(this.storageEncryptService.encryptSHA1('AUTH-TOKEN')));

        // ... Gunakan jwtToken untuk setHeaders tiap HTTP Request
        const jwtToken = this.storageEncryptService.encrypt(token, `${environment.secretKey.jwtKey}`);

        const isApiUrl = httpRequest.url.startsWith(environment.webApiUrl);

        return this.authService.currentUser
            .pipe(
                take(1),
                exhaustMap(user => {
                    if (isApiUrl) {
                        const modifiedRequest = httpRequest.clone({
                            setHeaders: {
                                AuthorizationToken: `${jwtToken}`
                            },
                        });

                        return next.handle(modifiedRequest);
                    }
                    else {
                        return next.handle(httpRequest);
                    }
                })
            )
    }
}