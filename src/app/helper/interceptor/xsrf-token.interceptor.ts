import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { CookieService } from 'ngx-cookie-service';
import { StorageEncryptService } from 'src/app/service/authentication/storage-encrypt.service';
import { environment } from 'src/environments/environment';

@Injectable()

export class XsrfTokenInterceptor implements HttpInterceptor {
    constructor(private cookieService: CookieService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler) {

        const xsrfToken = this.cookieService.get(".xS");
        const isApiUrl = request.url.startsWith(environment.webApiUrl);
        const headerName = 'X-XSRF-TOKEN';

        if (isApiUrl && xsrfToken !== null && !request.headers.has("X-XSRF-TOKEN")) {
            // ...
            const modifiedRequest = request.clone({
                withCredentials: true,
                headers: request.headers.set(headerName, xsrfToken),
            });

            return next.handle(modifiedRequest);
        } else {
            // ...
            return next.handle(request);
        }
    }
}