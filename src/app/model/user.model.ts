export class User {
    constructor(
        public userid: number,
        public username: string,
        public name: string,
        public roleid: number,
        public _token: string,
        public _tokenExpirationDate: Date
    ) { }
}