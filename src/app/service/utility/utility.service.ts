import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as moment from 'moment';
import Swal from 'sweetalert2';
import { HttpRequestService } from '../http-request/http-request.service';

@Injectable({
    providedIn: 'root'
})
export class UtilityService {


    constructor(private httpRequestService: HttpRequestService) { }

    getAgeUsingMoment(dateString: any) {
        dateString = moment(dateString).format("DD/MM/YYYY");

        // console.log(dateString);

        let now = new Date();
        let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

        let yearNow = now.getFullYear();
        let monthNow = now.getMonth();
        let dateNow = now.getDate();

        let dob = new Date(dateString.substring(6, 10),
            dateString.substring(3, 5) - 1,
            dateString.substring(0, 2)
        );

        let yearDob = dob.getFullYear();
        let monthDob = dob.getMonth();
        let dateDob = dob.getDate();
        let age: any = {};
        let ageString = "";
        let yearString = "";
        let monthString = "";
        let dayString = "";

        let monthAge = 0;
        let dateAge = 0;


        let yearAge = yearNow - yearDob;

        if (monthNow >= monthDob)
            monthAge = monthNow - monthDob;
        else {
            yearAge--;
            monthAge = 12 + monthNow - monthDob;
        }

        if (dateNow >= dateDob)
            dateAge = dateNow - dateDob;
        else {
            monthAge--;
            dateAge = 31 + dateNow - dateDob;

            if (monthAge < 0) {
                monthAge = 11;
                yearAge--;
            }
        }

        age = {
            years: yearAge,
            months: monthAge,
            days: dateAge
        };

        // console.log(age)

        if (age.years > 1) yearString = " years";
        else yearString = " year";
        if (age.months > 1) monthString = " months";
        else monthString = " month";
        if (age.days > 1) dayString = " days";
        else dayString = " day";


        if ((age.years > 0) && (age.months > 0) && (age.days > 0))
            ageString = age.years + yearString + ", " + age.months + monthString + " " + age.days + dayString + " ";
        else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
            ageString = " " + age.days + dayString + " ";
        else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
            ageString = age.years + yearString + " ";
        else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
            ageString = age.years + yearString + "  " + age.months + monthString + " ";
        else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
            ageString = age.months + monthString + " " + age.days + dayString + " ";
        else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
            ageString = age.years + yearString + " " + age.days + dayString + " ";
        else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
            ageString = age.months + monthString + " ";
        else ageString = "Oops! Could not calculate age!";

        return age;
    }

    // ... Untuk mengisi form control value
    setFormControlValue(formGroup: FormGroup, formControlName: string, value: any) {
        formGroup.get(formControlName)?.setValue(value);
    }


    // ... Untuk menampilkan dialog konfirmasi
    onShowConfirmationDialog(pesan: string, isConfirmed: () => void) {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: pesan,
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Iya, Saya Yakin!',
            cancelButtonText: 'Cancel',
            focusCancel: true
        }).then((result) => {
            if (result.isConfirmed) {
                isConfirmed();
            } else {
                Swal.fire(
                    'Cancelled!',
                    'Periksa Kembali Data Anda',
                    'error'
                )
            }
        })
    }

    // !! Update SweetAlert 
    onShowConfirmationDialogTrias(pesan: string, isConfirmed: () => void) {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: pesan,
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Iya, Saya Yakin!',
            cancelButtonText: 'Cancel',
            focusCancel: true
        }).then((result) => {
            if (result.isConfirmed) {
                isConfirmed();
            } else {
                Swal.fire(
                    'Cancelled!',
                    'Periksa Kembali Data Anda',
                    'error'
                )
            }
        })
    }

    // ... Untuk print report
    onPrintReport(url: string, data: any) {
        this.httpRequestService.defaultPostRequestWithoutMap(url, JSON.stringify(data), true)
            .subscribe((_result) => {

                let Type = "data:application/pdf;base64, ";
                let Url = Type + _result;

                let Setting = [
                    "height=" + screen.height,
                    "width=" + screen.width,
                    "fullscreen=yes"
                ].join(',');

                let Content = "<embed width=100% height=100% id='viewer'" +
                    " type='application/pdf'" +
                    " src='" + Url + "'></embed>";

                let PrintWindow = window.open("", "pdf", Setting);
                PrintWindow.moveTo(0, 0);
                PrintWindow.document.write(Content);

            }, (error) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: error,
                });
            })
    }

    onConvertBase64toBlob(data: any, contentType: any, sliceSize?: any) {
        contentType = contentType || "";
        sliceSize = sliceSize || 512;

        let byteCharacters = atob(data);
        let byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            let slice = byteCharacters.slice(offset, offset + sliceSize);

            let byteNumbers = new Array(slice.length);

            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            let byte = new Uint8Array(byteNumbers);

            byteArrays.push(byte);
        }

        let blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    onConvertBase64toFile(data: any, format: string) {
        let Content = "application/" + format;
        let Blob = this.onConvertBase64toBlob(data, Content);

        let BlobUrl = URL.createObjectURL(Blob);

        window.open(BlobUrl);
    }

    onExportReport(url: string, data: any, format: string) {
        return this.httpRequestService.defaultPostRequestForExportPrint(url, data, true, format)
            .subscribe((_result) => {
                this.onConvertBase64toFile(_result, format);
            }, (error) => {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: error,
                });
            })
    }
}
