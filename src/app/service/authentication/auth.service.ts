import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../../model/user.model';
import { StorageEncryptService } from './storage-encrypt.service';

export interface AuthResponseData {
    kind: string;
    idToken: string;
    email: string;
    refreshToken: string;
    expiresIn: string;
    localId: string;
    registered?: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    httpHeaders: any = new HttpHeaders();

    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    private tokenExpirationTimer: any;

    constructor(private http: HttpClient, private router: Router, private storageEncryptService: StorageEncryptService, private cookieService: CookieService) {
        this.httpHeaders = this.httpHeaders.set('Content-Type', 'Application/json');

        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(this.storageEncryptService.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    signUp(email: string, password: string, data: any) {

    }

    requestToken(userId: number) {
        return this.http.post<any>(
            `${environment.webApiUrl}/Authentication/RequestToken`, userId,
            {
                headers: this.httpHeaders
            }
        ).pipe(
            map(token => {
                let _token = token.result;
                return _token;
            })
        );
    };

    login(data: any) {
        return this.http.post<any>(
            `${environment.webApiUrl}/Authentication/SignIn`, JSON.stringify(data),
            {
                headers: this.httpHeaders
            }
        ).pipe(
            catchError(this.handlingError),
            map((user) => {
                let result = JSON.parse(user.result);

                // ... Request Token
                let _userId = result.UserId;
                this.requestToken(_userId)
                    .subscribe((_token) => {
                        // localStorage.setItem('token', _token);
                        const authToken = this.storageEncryptService.encryptSHA1('AUTH-TOKEN');
                        const jwtToken = this.storageEncryptService.encrypt(_token);

                        console.log('Setting up cookie service....');
                        this.cookieService.set(authToken, jwtToken);
                    });

                // ... Handling Authentication
                this.handlingAuth(result);

                return result;
            })
        )
    }

    logout() {
        // ... Kosongkan Current User Subject
        this.currentUserSubject.next(null);

        // ... Kemudian Arahkan Ke Login Url
        this.router.navigateByUrl('');

        // ... RemoveItem 
        this.storageEncryptService.removeItem('currentUser');
    }

    autoLogin() {
        const userData: {
            userid: number;
            username: string;
            name: string;
            roleid: number;
            _token: string;
            _tokenExpirationDate: string;
        } = JSON.parse(this.storageEncryptService.getItem('currentUser'));

        if (!userData) { return; }

        const loadedUser = new User(
            userData.userid,
            userData.username,
            userData.name,
            userData.roleid,
            userData._token,
            new Date(userData._tokenExpirationDate));

        if (loadedUser._token) {
            this.currentUserSubject.next(loadedUser);
            const expirationDuration = new Date(userData._tokenExpirationDate).getTime() - new Date().getTime();

            console.log(expirationDuration);
        };

        this.storageEncryptService.getItem('menuData');
    }

    autoLogout(expirationDuration: number) {
        this.tokenExpirationTimer = setTimeout(() => {
            this.logout();
        }, expirationDuration)
    }

    private handlingAuth(userData: any) {
        // ... Dapatkan token
        const token = localStorage.getItem('token');

        // ... Atur Token Expired Date
        let expiresIn: number;
        if (userData.RoleId > 0) { expiresIn = 3600 };
        const expiredDate = new Date(new Date().getTime() + expiresIn * 1000);

        // ... Set Auto Logout Timer
        this.autoLogout(expiresIn * 1000);

        // ... Set User
        const user = new User(userData.UserId, userData.UserName, userData.NamaKaryawan, userData.RoleId, token, expiredDate);
        this.storageEncryptService.setItem('currentUser', JSON.stringify(userData));
        this.currentUserSubject.next(user);

        return user;
    }

    private handlingError(errorResponse: HttpErrorResponse) {
        let error = errorResponse.error.result;
        let pesanError = "An Error Occured";

        if (!error) {
            return throwError(pesanError);
        }
        switch (error) {
            case "Periksa Username dan Password Anda!":
                pesanError = "Username dan Password Anda Salah";
                break;
        }
        return throwError(pesanError);
    }
}
