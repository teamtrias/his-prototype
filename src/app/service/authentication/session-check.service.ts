import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { StorageEncryptService } from './storage-encrypt.service';

@Injectable({
    providedIn: 'root'
})
export class SessionCheckService {

    httpHeaders: HttpHeaders = new HttpHeaders();

    constructor(private httpClient: HttpClient, private storageEncryptService: StorageEncryptService) {
        this.httpHeaders = this.httpHeaders.set('Content-Type', 'Application/json');
    }

    onCekSession(prefix: string) {
        let encryptedPrefix = this.storageEncryptService.encrypt(prefix, `${environment.secretKey.sessionKey}`);

        return this.httpClient.post<any>(
            `${environment.webApiUrl}/Authentication/CheckingSessionActive`,
            {
                "Prefix": encryptedPrefix
            },
            {
                headers: this.httpHeaders
            }).pipe(
                map((_result) => {
                    // console.log(_result);

                    return _result;
                })
            )
    }
}
