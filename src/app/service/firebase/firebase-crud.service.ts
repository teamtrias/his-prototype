import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { HttpRequestService } from '../http-request/http-request.service';

@Injectable({
    providedIn: 'root'
})
export class FirebaseCrudService {

    private httpHeaders: any;

    constructor(private httpRequestService: HttpRequestService, private httpClient: HttpClient) {
        this.httpHeaders = new HttpHeaders();
        this.httpHeaders = this.httpHeaders.set('Content-Type', 'application/json');
    }

    onSubmitForm(url: string, data: any) {
        return this.httpRequestService.defaultPostRequest(
            `${environment.firebaseDatabaseUrl}` + url,
            data,
            false
        ).subscribe((result) => {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Data Berhasil Disimpan',
                showConfirmButton: false,
                timer: 1500
            });

            console.log(result);
        }, (pesanError) => {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
                footer: pesanError
            });
            console.log(pesanError);
        })
    }

    onFetchData(url: string) {
        let searchParams = new HttpParams();
        searchParams = searchParams.append('print', 'pretty');
        searchParams = searchParams.append('custom', 'json');

        return this.httpClient.get<any>(
            `${environment.firebaseDatabaseUrl}` + url,
            {
                headers: this.httpHeaders,
                params: searchParams
            })
            .pipe(
                catchError(this.handleError),
            )
    }

    onDeleteData(url: string, data: any) {
        return this.httpRequestService.defaultDeleteRequest(
            `${environment.firebaseDatabaseUrl}` + '/' + url, data,
        ).subscribe((result) => {

            console.log(result);
        }, (pesanError) => {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
                footer: pesanError
            });
            console.log(pesanError);
        })
    }

    private handleError(httpErrorResponse: HttpErrorResponse) {
        let pesanError = 'An Error Occured!';

        if (httpErrorResponse.error)
            return throwError(pesanError);
    }
}
