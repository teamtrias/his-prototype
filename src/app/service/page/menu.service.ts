import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class MenuService {

    private httpHeaders: HttpHeaders = new HttpHeaders();

    constructor(private http: HttpClient) {
        this.httpHeaders = this.httpHeaders.set('Content-Type', 'Application/json');
    }

    onFetchData() {

    }

    onFetchSidebarMenu(roleId: number, menuId: number) {
        let sidebarParams = {
            RoleId: roleId,
            MenuId: menuId
        };

        return this.http.post<any>(
            `${environment.webApiUrl}` + "/Menu/SideBar", JSON.stringify(sidebarParams),
            {
                headers: this.httpHeaders
            }
        ).pipe(
            map((_menuSidebar) => {
                return _menuSidebar;
            })
        );
    }
}
