import { Injectable } from '@angular/core';
import { FirebaseCrudService } from '../firebase/firebase-crud.service';
import { HttpRequestService } from '../http-request/http-request.service';

@Injectable({
    providedIn: 'root'
})
export class ResepDigitalService {

    constructor(
        private httpRequestService: HttpRequestService,
        private firebaseRequestService: FirebaseCrudService) { }

    onFetchDrugNameAutocompleteDatasource() {
        let url = "/TransaksiOrderResep/GetAllFormulariumByParams";
        let columnName = "fl.KategoriFormulariumId";
        let filter = "equal";
        let searchText = "2";

        return this.httpRequestService.defaultPostRequestWithParameterSearchModel(url, columnName, filter, searchText);
    }

    onFetchGridInputResepProperties() {
        let url = "Order/Resep/GridObat.json";

        return this.firebaseRequestService.onFetchData(url);
    }

    onAddDataToGridInputResep(data: any) {
        let url = "/TransaksiOrderLaboratorium/GetDiagnosaPenyakit";
        let columnName = "NoRegister";
        let filter = "equal";
        let searchText = data.NoRegister;

        return this.httpRequestService.defaultPostRequestWithParameterSearchModel(url, columnName, filter, searchText);
    }

    onSubmitFormInputResep(data: any) {
        let url = "/TransaksiOrderResep/InsertUpdate";

        return this.httpRequestService.defaultPostRequest(url, data, true);
    }

    onFetchHistoryResepDigital(dataPasien: any) {
        let url = "/TransaksiOrderResep/GetAllResepByParams";
        let data = [
            {
                columnName: "MrNo",
                filter: "equal",
                searchText: "",
                searchText2: dataPasien.MrNo,
            }, {
                columnName: "NoRegister",
                filter: "equal",
                searchText: "",
                searchText2: dataPasien.NoRegister,
            }
        ];

        return this.httpRequestService.defaultPostRequest(url, data, true);
    }

    onFetchDetailHistoryResepDigital(dataHeader: any) {
        let url = "/TransaksiOrderResep/GetAllResepDetailByParams";
        let data = [{
            columnName: "ResepId",
            filter: "equal",
            searchText: "",
            searchText2: (dataHeader.ResepId).toString(),
        }];

        return this.httpRequestService.defaultPostRequest(url, data, false);
    }

    onFetchGridDaftarResepProperties() {
        let url = "Order/Resep/HistoryHeader/columns.json";

        return this.firebaseRequestService.onFetchData(url);
    }

    onFetchGridDetailResepProperties() {
        let url = "Order/Resep/HistoryDetail/columns.json";

        return this.firebaseRequestService.onFetchData(url);
    }
}
