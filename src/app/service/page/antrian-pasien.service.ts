import { Injectable } from '@angular/core';
import { FirebaseCrudService } from '../firebase/firebase-crud.service';
import { HttpRequestService } from '../http-request/http-request.service';

@Injectable({
    providedIn: 'root'
})
export class AntrianPasienService {

    constructor(private firebaseCrudService: FirebaseCrudService,
        private httpRequestService: HttpRequestService) { }

    // ... Lookup Utility
    onGetLookupColumns(url: string) {
        return this.firebaseCrudService.onFetchData(url);
    }

    onGetLookupFilters(url: string) {
        return this.firebaseCrudService.onFetchData(url);
    }

    // ... Grid Utility
    onGetGridColumns() {
        let url = "AntrianPasien/GridHeader.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    // ... On Get Antrian IRJA
    onGetAllAntrianIRJA(params: any) {
        return this.httpRequestService.defaultPostRequest(
            "/SetupAdmisiPasienIRJA/GetAllDataAntrianIrjaByParams",
            params,
            true
        );
    }

    // ... On Post Action Periksa
    onPostActionPeriksaPasien(params: any) {
        let url = "/SetupAdmisiPasienIRJA/Periksa";

        let data = {
            "Id": params["IdAntrian"],
            "MrNo": params["MrNo"],
        };

        return this.httpRequestService.defaultPostRequest(url, data, true);
    }

    // ... Grid Konsul Utility
    onGetGridKonsulColumns() {
        let url = "AntrianPasien/GridHeaderKonsul.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    // ... On Post Action Konsul 
    onPostActionKonsulPasien(params: any) {
        let url = "/SetupAdmisiPasienIRJA/Konsultasi";

        return this.httpRequestService.defaultPostRequest(url, params, true);
    }
}
