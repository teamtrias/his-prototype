import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { FirebaseCrudService } from '../firebase/firebase-crud.service';
import { HttpRequestService } from '../http-request/http-request.service';
import * as lodash from 'lodash';
import { element } from 'protractor';

@Injectable({
    providedIn: 'root'
})
export class LaboratoriumService {

    constructor(private firebaseCrudService: FirebaseCrudService,
        private httpRequestService: HttpRequestService) { }

    // ... On Get Grid Column
    onGetGridColumn(url: string) {
        return this.firebaseCrudService.onFetchData(url)
            .pipe(
                map((_result) => {
                    return _result.columns;
                })
            )
    }

    // ... On Fetch Grid Data source
    onFetchGridDataSource(url: string, params: any) {
        return this.httpRequestService.defaultPostRequest(url, params, false);
    }

    // ... On Fetch Tarif Configuration
    onFetchTarifConfiguration(data: any) {
        let url = "/TransaksiOrderLaboratorium/GetTarifMapConfigurations";

        return this.httpRequestService.defaultPostRequest(url, data, false)
            .pipe(
                map((_result) => {

                    // ... Grouping by Dept Description
                    let group = lodash.groupBy(_result, "DeptDescr");

                    let tabTarifLab = [];

                    // ... Gunakan for untuk mendapatkan kelompok
                    for (let item in group) {

                        // ... Grouping by Kode Kelompok
                        let kelompok = lodash.groupBy(group[item], "KodeKelompok");

                        let buttonPerKelompok = [];
                        // ... Gunakan for untuk mendapatkan Button per kelompok
                        for (let itemPerKelompok in kelompok) {

                            // ... Push ke Array Button per kelompok
                            buttonPerKelompok.push({
                                "KodeKelompok": itemPerKelompok,
                                "NamaKelompok": group[item].filter((data) => { return data["KodeKelompok"] == itemPerKelompok })[0]["NamaKelompok"],
                                "Tarif": kelompok[itemPerKelompok]
                            });
                        }

                        // ... Kemudian set tabTarifLab menggunakan buttonPerKelompok dan item
                        tabTarifLab.push({
                            "Text": item,
                            "Kelompok": buttonPerKelompok
                        });
                    }

                    return _result = { "TabTarifLab": tabTarifLab };
                })
            )
    }

    onGetLookupColumns(url: string) {
        return this.firebaseCrudService.onFetchData(url);
    }

    onGetLookupFilters(url: string) {
        return this.firebaseCrudService.onFetchData(url);
    }

    onSubmitForm(url: string, data: any) {
        return this.httpRequestService.defaultPostRequest(url, data, true);
    }

    onSubmitFormOrderLab(url: string, data: any) {
        return this.httpRequestService.defaultPostRequest(url, data, true);
    }

    onFetchTarifPemeriksaanLain(elementId: string) {
        return this.httpRequestService.defaultPostRequestWithoutMap(
            "/TransaksiOrderLaboratorium/GetKodeTarifPemeriksaanLain",
            [],
            false
        ).subscribe((_result) => {
            (<HTMLInputElement>document.getElementById(elementId)).value = _result;
        })
    }
}
