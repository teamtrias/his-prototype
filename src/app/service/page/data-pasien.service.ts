declare var toProperCase;

import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { FirebaseCrudService } from '../firebase/firebase-crud.service';
import { HttpRequestService } from '../http-request/http-request.service';


@Injectable({
    providedIn: 'root'
})
export class DataPasienService {

    constructor(private HttpRequestService: HttpRequestService,
        private firebaseCrudService: FirebaseCrudService) { }

    getKriteriaPasien() {
        let url = "DataPasien/kriteriaPencarianPasien.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    getGridColumnHeaders() {
        let url = "DataPasien/gridDataPasienHeader.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    getDataPasien(columnName: string, filter: string, searchText: string) {
        let url = "/SetupPasien/GetAllPasienByParamsForGrid";

        if (columnName)
            return this.HttpRequestService.defaultPostRequestWithParameterSearchModel(url, columnName, filter, searchText);
        else
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Kategori Tidak Boleh Kosong!',
            });
    }
}
