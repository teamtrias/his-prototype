import { Injectable } from '@angular/core';
import { FirebaseCrudService } from '../firebase/firebase-crud.service';
import { HttpRequestService } from '../http-request/http-request.service';

@Injectable({
    providedIn: 'root'
})
export class AdmisiPasienService {

    constructor(private firebaseCrudService: FirebaseCrudService,
        private httpRequestService: HttpRequestService) { }

    onGetLookupColumns(url: string) {
        return this.firebaseCrudService.onFetchData(url);
    }

    onGetLookupFilters(url: string) {
        return this.firebaseCrudService.onFetchData(url);
    }

    onSubmitForm(dataAdmisiPasien: any) {
        let url = "/SetupAdmisiPasienIRJA/InsertUpdate";

        return this.httpRequestService.defaultPostRequest(url, dataAdmisiPasien, true);
    }
}
