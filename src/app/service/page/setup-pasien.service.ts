import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { map, tap } from 'rxjs/operators';
import { FirebaseCrudService } from '../firebase/firebase-crud.service';
import { HttpRequestService } from '../http-request/http-request.service';
import { UtilityService } from '../utility/utility.service';

@Injectable({
    providedIn: 'root'
})
export class SetupPasienService {

    constructor(private HttpRequestService: HttpRequestService,
        private firebaseCrudService: FirebaseCrudService,
        private utilityService: UtilityService) { }

    getTabHeader() {
        let url = "SetupPasien/TabHeader.json";

        return this.firebaseCrudService.onFetchData(url)
    }

    getDataFromSetupPasien(url: string) {
        return this.HttpRequestService.defaultPostRequest(url, [], false)
            .pipe(
                map((_result) => {
                    return _result;
                })
            )
    }

    // ... Lookup Agama Attribute
    getLookupAgamaColumns() {
        let url = "SetupPasien/GridHeader/LookupAgama/columns.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    getLookupAgamaFilters() {
        let url = "SetupPasien/GridHeader/LookupAgama/filter.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    // ... Lookup Marital Attribute
    getLookupMaritalColumns() {
        let url = "SetupPasien/GridHeader/LookupMaritalStatus/columns.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    getLookupMaritalFilters() {
        let url = "SetupPasien/GridHeader/LookupMaritalStatus/filter.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    // ... Lookup Pendidikan Attribute
    getLookupPendidikanColumns() {
        let url = "SetupPasien/GridHeader/LookupPendidikan/columns.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    getLookupPendidikanFilters() {
        let url = "SetupPasien/GridHeader/LookupPendidikan/filter.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    // ... Lookup Pekerjaan Attribute
    getLookupPekerjaanColumns() {
        let url = "SetupPasien/GridHeader/LookupPekerjaan/columns.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    getLookupPekerjaanFilters() {
        let url = "SetupPasien/GridHeader/LookupPekerjaan/filter.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    // ... Lookup Provinsi Attribute
    getLookupProvinsiColumns() {
        let url = "SetupPasien/GridHeader/LookupProvinsi/columns.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    getLookupProvinsiFilters() {
        let url = "SetupPasien/GridHeader/LookupProvinsi/filter.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    // ... Lookup Kota Attribute
    getLookupKotaColumns() {
        let url = "SetupPasien/GridHeader/LookupKota/columns.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    getLookupKotaFilters() {
        let url = "SetupPasien/GridHeader/LookupKota/filter.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    // ... Lookup Kecamatan Attribute
    getLookupKecamatanColumns() {
        let url = "SetupPasien/GridHeader/LookupKecamatan/columns.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    getLookupKecamatanFilters() {
        let url = "SetupPasien/GridHeader/LookupKecamatan/filter.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    // ... Lookup Kelurahan Attribute
    getLookupKelurahanColumns() {
        let url = "SetupPasien/GridHeader/LookupKelurahan/columns.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    getLookupKelurahanFilters() {
        let url = "SetupPasien/GridHeader/LookupKelurahan/filter.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    // ... Lookup Jenis Kartu Attribute
    getLookupJenisKartuColumns() {
        let url = "SetupPasien/GridHeader/LookupKartuIdentitas/columns.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    getLookupJenisKartuFilters() {
        let url = "SetupPasien/GridHeader/LookupKartuIdentitas/filter.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    // ... Lookup Suku Attribute
    getLookupSukuColumns() {
        let url = "SetupPasien/GridHeader/LookupSuku/columns.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    getLookupSukuFilters() {
        let url = "SetupPasien/GridHeader/LookupSuku/filter.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    // ... Lookup Kebangsaan Attribute
    getLookupKebangsaanColumns() {
        let url = "SetupPasien/GridHeader/LookupKebangsaan/columns.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    getLookupKebangsaanFilters() {
        let url = "SetupPasien/GridHeader/LookupKebangsaan/filter.json";

        return this.firebaseCrudService.onFetchData(url);
    }

    // ... On Submit Form
    onSubmitForm(dataPasien: any) {
        let url = "/SetupPasien/InsertUpdate";

        return this.HttpRequestService.defaultPostRequest(url, dataPasien, true);
    }

    //... On Get Data after click Edit Button in Data Pasien
    onGetDataPasien(url: string, params: any, formGroup: FormGroup, formControlData: any) {
        return this.HttpRequestService.defaultPostRequestWithParameterSearchModel(
            url,
            params.columnName,
            params.filter,
            params.searchText
        ).pipe(
            tap((_result) => {
                console.log(_result);

                if (formControlData.length > 0) {
                    for (let i = 0; i < formControlData.length; i++) {
                        if (formControlData[i]["isInt"])
                            this.utilityService.setFormControlValue(formGroup, formControlData[i]["formControlName"], parseInt(_result[0][formControlData[i]["value"]]));
                        else
                            this.utilityService.setFormControlValue(formGroup, formControlData[i]["formControlName"], _result[0][formControlData[i]["value"]]);
                    }
                }
            })
        )
    }

    onGetDataKeluargaPasien(url: string, params: any, formGroup: FormGroup, formControlData: any) {
        return this.HttpRequestService.defaultPostRequestWithParameterSearchModel(
            url,
            params.columnName,
            params.filter,
            params.searchText
        ).pipe(
            tap((_result) => {
                console.log(_result);

                if (formControlData.length > 0) {
                    for (let i = 0; i < formControlData.length; i++) {
                        if (formControlData[i]["isInt"])
                            this.utilityService.setFormControlValue(formGroup, formControlData[i]["formControlName"], parseInt(_result[formControlData[i]["value"]]));
                        else
                            this.utilityService.setFormControlValue(formGroup, formControlData[i]["formControlName"], _result[formControlData[i]["value"]]);
                    }
                }
            })
        )
    }


}