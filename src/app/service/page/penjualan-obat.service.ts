import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { FirebaseCrudService } from '../firebase/firebase-crud.service';
import { HttpRequestService } from '../http-request/http-request.service';

@Injectable({
    providedIn: 'root'
})
export class PenjualanObatService {

    constructor(
        private httpRequestService: HttpRequestService,
        private firebaseCrudService: FirebaseCrudService
    ) { }

    onGetLookupColumns(url: string) {
        return this.firebaseCrudService.onFetchData(url);
    }

    onGetLookupFilters(url: string) {
        return this.firebaseCrudService.onFetchData(url);
    }

    // ... On Fetch Grid Column
    onFetchGridColumn(url: string) {
        return this.firebaseCrudService.onFetchData(url)
            .pipe(
                map((_result) => {
                    return _result.columns;
                })
            )
    }

    // ... Function untuk GET DIAGNOSA PASIEN ...
    onGetDiagnosaPasienByNoRegister(noRegister: any) {
        let url = "/TransaksiOrderLaboratorium/GetDiagnosaPenyakit";
        let columnName = "NoRegister";
        let filter = "equal";
        let searchText = noRegister.toString();

        return this.httpRequestService.defaultPostRequestWithParameterSearchModel(
            url, columnName, filter, searchText
        );
    }

    // ... Function untuk GET RESEP HEADER ...
    onGetResepHeaderByNoRegisterAndNoMr(MrNo: any, NoRegister: any) {
        let url = "/TransaksiOrderResep/GetAllResepByParams";
        let data = [
            {
                columnName: "MrNo",
                filter: "equal",
                searchText: "",
                searchText2: MrNo.toString(),
            }, {
                columnName: "NoRegister",
                filter: "equal",
                searchText: "",
                searchText2: NoRegister.toString(),
            }
        ];

        return this.httpRequestService.defaultPostRequest(url, data, true);
    }

    // ... Function untuk GET RESEP DETAIL ...
    onGetResepDetailByResepId(ResepId: any) {
        let url = "/TransaksiOrderResep/GetAllResepDetailByParams";
        let data = [{
            columnName: "ResepId",
            filter: "equal",
            searchText: "",
            searchText2: ResepId.toString(),
        }];

        return this.httpRequestService.defaultPostRequest(url, data, false);
    }

    onMapBetweenGridDatasourceAndSomeData(gridDataPerRow: any, someData: any) {
        for (let item in someData) {
            gridDataPerRow[item] = someData[item];
        }
    }

    // .. Function untuk GET HARGA OBAT TERAKHIR ...
    onGetHargaObatTerakhirByItemCode(ItemCode: any) {
        let url = "/TransaksiPenjualanObat/GetHargaObatTerakhir";

        return this.httpRequestService.defaultPostRequest(url, JSON.stringify(ItemCode), false);
    }

    // ... Function untuk GET HISTORY HEADER ...
    onGetHistoryHeaderByNoRegister(NoRegister: any) {
        let url = "/TransaksiPenjualanObat/GetHistoryHeader";
        let columnName = "NoRegister";
        let filter = "equal";
        let searchText = NoRegister.toString();

        return this.httpRequestService.defaultPostRequestWithParameterSearchModel(url, columnName, filter, searchText);
    }

    // ... Function untuk GET HISTORY DETAIL ...
    onGetHistoryDetailByPenjualanResepId(PenjualanResepId: any) {
        let url = "/TransaksiPenjualanObat/GetHistoryDetail";
        let columnName = "PenjualanResepID";
        let filter = "equal";
        let searchText = PenjualanResepId.toString();

        return this.httpRequestService.defaultPostRequestWithParameterSearchModel(url, columnName, filter, searchText);
    }

    // .. Function untuk Submit Form Input Penjualan ..
    onSubmitForm(data: any) {
        let url = "/TransaksiPenjualanObat/Insert";

        return this.httpRequestService.defaultPostRequest(url, data, true);
    }

}
