import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { HttpRequestService } from '../http-request/http-request.service';

@Injectable({
    providedIn: 'root'
})
export class SetupTarifService {

    url: string = "setupTarif.json";

    constructor(private httpRequestService: HttpRequestService) { }

    onSubmitSetupTarif(data: any, callbackData: any) {
        return this.httpRequestService.defaultPostRequest(this.url, data, false)
            .subscribe(
                (_result) => {
                    console.log(_result);
                    this.onFetchSetupTarif(callbackData);
                }, (_error) => {
                    console.log(_error);
                }
            )
    }

    onFetchSetupTarif(callbackData: any) {
        let params: [];

        return this.httpRequestService.defaultGetRequest(this.url, params)
            .subscribe((_result) => {
                callbackData = _result;
            });
    }

    onEditSetupTarif(previousData: any, updatedData: any) {
        this.httpRequestService.defaultUpdateRequest("setupTarif", updatedData, previousData)
            .subscribe(
                (_result) => {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Data Berhasil Disimpan',
                        showConfirmButton: false,
                        timer: 1500
                    })
                },
                (pesanError) => {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                        footer: pesanError
                    });
                }
            )
    }
}
