import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { FirebaseCrudService } from '../firebase/firebase-crud.service';
import { HttpRequestService } from '../http-request/http-request.service';

@Injectable({
    providedIn: 'root'
})
export class BillingService {

    constructor(
        private httpRequestService: HttpRequestService,
        private firebaseCrudService: FirebaseCrudService
    ) { }

    onGetLookupColumns(url: string) {
        return this.firebaseCrudService.onFetchData(url);
    }

    onGetLookupFilters(url: string) {
        return this.firebaseCrudService.onFetchData(url);
    }

    // ... On Fetch Grid Column
    onFetchGridColumn(url: string) {
        return this.firebaseCrudService.onFetchData(url)
            .pipe(
                map((_result) => {
                    return _result.columns;
                })
            )
    }

    onFetchGridSlipTagihanDatasource(NoRegister: string, MrNo: string) {

        let url = "/TransaksiBillingPasien/GetSlipTagihan";
        let data = [
            {
                columnName: "MrNo",
                filter: "equal",
                searchText: "",
                searchText2: MrNo.toString(),
            }, {
                columnName: "NoRegister",
                filter: "equal",
                searchText: "",
                searchText2: NoRegister.toString(),
            }
        ];

        return this.httpRequestService.defaultPostRequest(url, data, true);
    }
}
