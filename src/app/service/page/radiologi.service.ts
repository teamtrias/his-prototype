import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpRequestService } from '../http-request/http-request.service';
import * as lodash from 'lodash';
import { FirebaseCrudService } from '../firebase/firebase-crud.service';
import { GridModule } from '@syncfusion/ej2-angular-grids';

@Injectable({
    providedIn: 'root'
})
export class RadiologiService {

    constructor(
        private httpRequestService: HttpRequestService,
        private firebaseCrudService: FirebaseCrudService,
    ) { }

    // ... On Fetch Tarif Configuration
    onFetchTarifConfiguration(data: any) {
        let url = "/TransaksiOrderLaboratorium/GetTarifMapConfigurations";

        return this.httpRequestService.defaultPostRequest(url, data, false)
            .pipe(
                map((_result) => {

                    // ... Grouping by Dept Description
                    let group = lodash.groupBy(_result, "DeptDescr");

                    let tabTarifRadiologi = [];

                    // ... Gunakan for untuk mendapatkan kelompok
                    for (let item in group) {

                        // ... Grouping by Kode Kelompok
                        let kelompok = lodash.groupBy(group[item], "KodeKelompok");

                        let buttonPerKelompok = [];
                        // ... Gunakan for untuk mendapatkan Button per kelompok
                        for (let itemPerKelompok in kelompok) {

                            // ... Push ke Array Button per kelompok
                            buttonPerKelompok.push({
                                "KodeKelompok": itemPerKelompok,
                                "NamaKelompok": group[item].filter((data) => { return data["KodeKelompok"] == itemPerKelompok })[0]["NamaKelompok"],
                                "Tarif": kelompok[itemPerKelompok]
                            });
                        }

                        // ... Kemudian set tabTarifLab menggunakan buttonPerKelompok dan item
                        tabTarifRadiologi.push({
                            "Text": item,
                            "Kelompok": buttonPerKelompok
                        });
                    }

                    return _result = { "TabTarifRadiologi": tabTarifRadiologi };
                })
            )
    }

    // ... On Get Grid Column
    onGetGridColumn(url: string) {
        return this.firebaseCrudService.onFetchData(url)
            .pipe(
                map((_result) => {
                    return _result.columns;
                })
            )
    }

    // ... On Submit Form
    onSubmitForm(url: string, data: any) {
        return this.httpRequestService.defaultPostRequest(url, data, true);
    }

    onFetchHistoryOrderLab(data: any) {
        let url = "/TransaksiOrderRadiologi/GetHistoryOrderRad";

        return this.httpRequestService.defaultPostRequest(url, data, false);
    }

    onFetchHistoryOrderLabDetail(data: any) {
        let url = "/TransaksiOrderRadiologi/GetHistoryOrderRadDetail";

        return this.httpRequestService.defaultPostRequest(url, data, false);
    }
}
