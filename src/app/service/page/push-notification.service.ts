import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpRequestService } from '../http-request/http-request.service';

@Injectable({
    providedIn: 'root'
})
export class PushNotificationService {

    constructor(private httpRequestService: HttpRequestService) { }

    onGetAllNotification(params: any) {
        let url: string = "/NotificationManagement/GetAllNotificationJson";

        return this.httpRequestService.defaultPostRequest(url, params, false)
            .pipe(
                map((_notif) => {
                    console.log(_notif);

                    return _notif;
                })
            )
    }
}
