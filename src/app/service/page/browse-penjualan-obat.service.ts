import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { FirebaseCrudService } from '../firebase/firebase-crud.service';
import { HttpRequestService } from '../http-request/http-request.service';

@Injectable({
    providedIn: 'root'
})
export class BrowsePenjualanObatService {

    constructor(
        private httpRequestService: HttpRequestService,
        private firebaseCrudService: FirebaseCrudService
    ) { }

    // ... On Fetch Grid Column
    onFetchGridColumn(url: string) {
        return this.firebaseCrudService.onFetchData(url)
            .pipe(
                map((_result) => {
                    return _result.columns;
                })
            )
    }

    onFetchHistoryHeaderByBillDate(BillDate: any) {
        let data = [
            {
                "columnName": "BillDate",
                "filter": "equal",
                "searchText": "",
                "searchText2": BillDate
            }
        ];

        return this.httpRequestService.defaultPostRequest(
            "/TransaksiPenjualanObat/GetHistoryHeaderForBrowse",
            data,
            true
        );
    }

    onFetchHistoryDetailByPenjualanResepId(PenjualanResepId: any) {
        let data = [
            {
                "columnName": "PenjualanResepID",
                "filter": "equal",
                "searchText": "",
                "searchText2": PenjualanResepId.toString()
            }
        ];

        return this.httpRequestService.defaultPostRequest(
            "/TransaksiPenjualanObat/GetHistoryDetail",
            data,
            true
        );
    }

}
