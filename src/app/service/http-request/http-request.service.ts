import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

declare var toProperCase;

@Injectable({
    providedIn: 'root'
})
export class HttpRequestService {

    private httpHeaders: any;

    constructor(private httpClient: HttpClient) {
        this.httpHeaders = new HttpHeaders();
        this.httpHeaders = this.httpHeaders.set('Content-Type', 'application/json');
    }

    defaultPostRequest(url: string, params: any, isShowLoading: boolean) {
        return this.httpClient.post<any>(
            `${environment.webApiUrl}` + url,
            params,
            {
                headers: this.httpHeaders
            }
        ).pipe(
            catchError(this.handleError),
            map((_result) => {
                return toProperCase(_result.result);
            }),
            tap((_result) => {
                if (isShowLoading) {
                    this.onShowLoading();
                }
            })
        )
    }

    defaultPostRequestWithoutMap(url: string, params: any, isShowLoading: boolean, headersOptional?: any) {
        return this.httpClient.post<any>(
            `${environment.webApiUrl}` + url,
            params,
            {
                headers: this.httpHeaders,
            }
        ).pipe(
            catchError(this.handleError),
            map((_result) => {
                return _result.result;
            }),
            tap((_result) => {
                if (isShowLoading) {
                    this.onShowLoading();
                }
            })
        )
    }

    defaultPostRequestWithParameterSearchModel(url: string, columnName: any, filter: any, searchText: any) {
        return this.httpClient.post<any>(
            `${environment.webApiUrl}` + url,
            [
                {
                    "columnName": columnName,
                    "filter": filter,
                    "searchText": searchText,
                    "searchText2": ""
                }
            ],
            {
                headers: this.httpHeaders
            }
        ).pipe(
            map((_result) => {
                return toProperCase(_result.result);
            })
        )
    }

    defaultPostRequestForExportPrint(url: string, params: any, isShowLoading: boolean, reportFormat: string) {
        switch (reportFormat) {
            case "pdf":
                reportFormat = "pdf";
                break;
            case "doc":
                reportFormat = "msword";
                break;
            case "word":
                reportFormat = "msword";
                break;
            case "xls":
                reportFormat = "vnd.ms-excel";
                break;
            case "excel":
                reportFormat = "vnd.ms-excel";
                break;
            default:
                reportFormat = "pdf";
                break;
        };

        let httpHeadersReport = new HttpHeaders();
        httpHeadersReport.append("ReportFormat", reportFormat);

        return this.httpClient.post<any>(
            `${environment.webApiUrl}` + url,
            params,
            {
                headers: httpHeadersReport
            }
        ).pipe(
            catchError(this.handleError),
            map((_result) => {
                return _result.result;
            }),
            tap((_result) => {
                if (isShowLoading) {
                    this.onShowLoading();
                }
            })
        )
    }

    defaultGetRequest(url: string, params: any) {
        return this.httpClient.get<any>(
            `${environment.webApiUrl}` + url,
            {
                headers: this.httpHeaders
            })
            .pipe(
                catchError(this.handleError),
                map((result: { [key: string]: any }) => {
                    // console.log(result);

                    let data = [];

                    for (const key in result) {
                        if (result.hasOwnProperty(key)) {
                            data.push({ ...result[key], id: key })
                        }
                    }

                    return data;
                })
            )
    }

    defaultGetRequestWithoutMap(url: string) {
        return this.httpClient.get<any>(
            `${environment.webApiUrl}` + url,
            {
                headers: this.httpHeaders
            })
            .pipe(
                catchError(this.handleError),
                map((_result) => {
                    return toProperCase(_result);
                })
            )
    }

    defaultDeleteRequest(url: string, params: any) {
        return this.httpClient.delete<any>(
            `${environment.webApiUrl}` + url + params,
            {
                headers: this.httpHeaders
            })
            .pipe(
                map((result) => {
                    console.log(result);
                })
            );
    }

    defaultUpdateRequest(url: string, updatedData: any, previousData: any) {
        let key = previousData['id'] + ".json";

        return this.httpClient.patch<any>(
            `${environment.webApiUrl}` + url + "/" + key,
            updatedData,
            {
                headers: this.httpHeaders,
            }
        ).pipe(
            catchError(this.handleError),
            map((_result) => {
                return _result;
            })
        )
    }

    onShowLoading() {
        let timerInterval: any;

        Swal.fire({
            title: 'Loading...',
            timer: 2000,
            timerProgressBar: true,
            showConfirmButton: false,
            showCancelButton: false,
            willOpen: () => {
                Swal.showLoading();
                timerInterval = setInterval(() => {
                    const content = Swal.getContent();
                    if (content) {
                        const b: any = content.querySelector('b');

                        if (b) {
                            b.textContent = Swal.getTimerLeft()
                        }
                    }
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval);
            }
        }).then((alertResponse) => {
            if (alertResponse.dismiss === Swal.DismissReason.timer) {

            };
        });
    }

    private handleError(httpErrorResponse: HttpErrorResponse) {
        let pesanError = 'An Error Occured!';

        if (httpErrorResponse.status == 400 && httpErrorResponse.error.errors) {
            // let errorField: string;

            // for (let index in httpErrorResponse.error.errors) {
            //     let field = index.replace('$.', '').split(".")[index.replace('$.', '').split(".").length - 1];
            //     errorField = field.replace(/([a-z0-9])([A-Z])/g, '$1 $2');
            // }

            // return throwError(errorField + " Tidak Boleh Kosong");
            return throwError(pesanError);
        }
        else if (httpErrorResponse.status === 500) {
            return throwError("Incorrect syntax near the keyword filters")
        }
        else {
            return throwError(pesanError);
        }
    }
}
